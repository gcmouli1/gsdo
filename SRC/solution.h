/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _SOLUTION_H 
#define _SOLUTION_H 
#include "inputParameters.h"
#include "rbfCompute.h"
#include "computeObjConstrValue.h"
#include <string.h>

typedef struct {
	int totalPts;
	int numHiddenConstrPts;
	int numFeasiblePts;
	int numInfeasiblePts;
	int numUnrelaxablePts;
	int numNQInfeasiblePts;
	int numAprioriHiddenConstrPts;
	int pollCenterIndex;
	int pollBestPtIndex;
	int leastInfPointIndex;
	int inputPtIndex;
	int bestPtIndex;
	int totalNonHiddenPts;
	int nonHiddenPtIndex;
	bool IsAprioriNonQRAKInfeasible;
	bool repeatPoint;
	bool feasiblePtFound;
	bool optimizationSuccessFul;
	bool allPointsRepeat;
	bool bestPtOnly;
	bool globalOptimaReached;
	bool IsPointStatusHidden;
	bool IsPointStatusUnrelaxable;
	bool stagnation;
	double minMaxDistance;
	double bestFeasibleObjectiveValue;
	double inputPtObjValue;
	double infeasMinConstrValue;

	char searchType;
	char *stepType;
	char *pointType;
	char *maxConstrViolReached;
	vectorD *lbScaled;
	vectorD *ubScaled;
	vectorD *objectiveValues;
	vectorD *leastInfPoint;
	vectorD *optimSolutionPt;
	vectorD *exploitLevel;
	vectorD *bestPoint;
	vectorD *sumConstraints;

	matrixD *lhMatrixPts;
	matrixD *allPtsMatrix;
	matrixD *aprioriHiddenConstrPts;
	matrixD *allConstraintValues;
	matrixD *diffMatrix;
	matrixD *pollDirections;
}solutionStruct;

void initiateSolution (inputData *inputParam, solutionStruct *solution);
void computeLeastInfeasiblePoint (inputData *inputParam, solutionStruct *solution);
bool checkDuplicity (inputData *inputParam, solutionStruct *solution, vectorD *inputPt, bool updateDiff);
void updateSolution (inputData *inputParam, solutionStruct *solution, vectorD *inputPt);
void coordinateStagnate (solutionStruct *solution, inputData *inputParam);
void pushInfeasIntoFeasRegion (inputData *inputParam, solutionStruct *solution, vectorD *infPt);
void updateDistMatrix (inputData *inputParam, solutionStruct *solution, vectorD *distVec);
void createNewBounds (inputData *inputParam, solutionStruct *solution, double delta, vectorD *pt);
void freeSolution (solutionStruct *solution);

#endif
