/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _CUSTOMFUNCTIONS_H
#define _CUSTOMFUNCTIONS_H
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include <stdbool.h>
#include "vecLapack.h"

/* Solver assumes that constraints are of the form g(x) >= 0. 
 * User must make sure that constraint output adheres to this form.  */


/* User defined connector between solver and original optimization problem.
 * x (input): 			Input point
 * dimension (input): 		dimension of the problem
 * funcValue (output): 		objective function value at x. 
 * constraintValues (output):	constraint values at x.
 * numConstraints (input): 	number of constraints.
 * pointType (output): 		If constraint violates some hidden constraint, set it to 'H'.  */

void userDefinedObjConstrFunc (double *x, int dimension, double *funcValue, double *constraintValues, int numConstraints, char *pointType);

/* User defined connector between solver and apriori constraints from original optimization problem.
 * x (input): 			Input point
 * dimension (input): 		dimension of the problem
 * constraintValues (output): 	constraint values at x.
 * pointType (output): 		If constraint violates some hidden constraint, set it to 'H'.  */

void userDefinedAprioriConstrEval (double *x, int dimension, double *constrVal, int numAprioriConstr, char *pointType);
#endif
