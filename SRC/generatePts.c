/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "generatePts.h"

void generatePts (inputData *inputParam, int numPts, vectorD *lb, vectorD *ub, matrixD *genPtMatrix)
{
	if (inputParam->randomGeneration == true){
		latinHypercubeUniform (inputParam, inputParam->dimension, numPts, lb, ub, genPtMatrix);
		inputParam->randomSeed++;
	}
	else if (inputParam->staticGeneration == true){
		latinHypercubeUniform (inputParam, inputParam->dimension, numPts, lb, ub, genPtMatrix);
		inputParam->iterHalton += numPts * inputParam->dimension;
	}
	else{
		printf("No point generation scheme chosen. Using Uniform Latin Hypercube as default\n");
		latinHypercubeUniform (inputParam, inputParam->dimension, numPts, lb, ub, genPtMatrix);
	}
}
