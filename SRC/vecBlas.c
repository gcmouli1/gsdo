/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "vecBlas.h"
/* Level-1 BLAS operations */

void swapVectors (vectorD *vectorX, vectorD *vectorY)
{
	int dimension=vectorX->dimension;
	int incx=1;
	int incy=1;
	dswap_(&dimension, vectorX->vec, &incx, vectorY->vec, &incy);
}

/* copies X into Y */
void copyVectors (vectorD *vectorX, vectorD *vectorY)
{
	int dimension=vectorX->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, vectorX->vec, &incx, vectorY->vec, &incy);
}

/* copies X into Y */
void copyMatrix (matrixD *matrixX, matrixD *matrixY)
{
	int dimension= (matrixX->rows) *(matrixY->cols);
	int incx=1;
	int incy=1;
	dcopy_(&dimension, matrixX->mat, &incx, matrixY->mat, &incy);
}

void scaleVector (vectorD *vector, double alpha)
{
	int dimension=vector->dimension;
	int incx=1;
	dscal_(&dimension, &alpha, vector->vec, &incx);
}

/* only for general matrix */
void scaleMatrix (matrixD *inputMat, double alpha)
{
	int dimension=(inputMat->rows)*(inputMat->cols);
	int incx=1;
	dscal_(&dimension, &alpha, inputMat->mat, &incx);
}

void vectorAddOrig (double alpha, vectorD *vectorX, double beta, vectorD *vectorY)
{
	if (beta != 1)
	{
		scaleVector (vectorY, beta);
	}
	int dimension=vectorX->dimension;
	int incx=1;
	int incy=1;
	daxpy_(&dimension, &alpha, vectorX->vec, &incx, vectorY->vec, &incy);
}

void vectorAdd ( double alpha, vectorD *vectorX, double beta, vectorD *vectorY, vectorD *outputVec)
{
	int dimension=vectorX->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, vectorY->vec, &incx, outputVec->vec, &incy);
	if (beta != 1.0)
	{
		dscal_(&dimension, &beta, outputVec->vec, &incx);
	}
	daxpy_(&dimension, &alpha, vectorX->vec, &incx, outputVec->vec, &incy);
}

double vectorDotProd (vectorD *vectorX, vectorD *vectorY)
{
	int incx=1;
	int incy=1;
	int dimension=vectorX->dimension;  //casting to int from int
	return ddot_(&dimension, vectorX->vec, &incx, vectorY->vec, &incy);
}

double vectorNorm2 (vectorD *vector)
{
	int dimension=vector->dimension;
	int incx=1;
	return dnrm2_(&dimension, vector->vec, &incx);
}

double vectorNorm1 (vectorD *vector)
{
	int dimension=vector->dimension;
	int incx=1;
	return dasum_(&dimension, vector->vec, &incx);
}

int vectorNormInfIndex (vectorD *vector)
{
	int dimension=vector->dimension;
	int incx=1;
	return idamax_(&dimension, vector->vec, &incx);
}

void vectorNormalize2 (vectorD *vector)
{
	int dimension=vector->dimension;
	int incx=1;
	double alpha=1.0/(dnrm2_(&dimension, vector->vec, &incx));
	dscal_(&dimension, &alpha, vector->vec, &incx);
}

void matrixVectorNorm2 (matrixD *inputMat, vectorD *normValue)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	int incx=1;
	double *tmp=malloc (sizeof (double)*rows);
	int i;
	for (i=0; i<cols; i++)
	{
		memcpy (tmp, inputMat->mat+rows*i, sizeof(double)*rows);
		normValue->vec[i]=dnrm2_(&rows, tmp, &incx);
	}
	free (tmp);
}

void matrixNormalize2 (matrixD *inputMat)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	int incx=1;
	double *tmp=malloc (sizeof (double)*rows);
	int i;
	for (i=0; i<cols; i++)
	{
		memcpy (tmp, inputMat->mat+rows*i, sizeof(double)*rows);
		double alpha=1.0/dnrm2_(&rows, tmp, &incx);
		dscal_(&rows, &alpha, tmp, &incx);
		memcpy (inputMat->mat+rows*i, tmp, sizeof(double)*rows);
	}
	free (tmp);
}
	
/* Level-2 BLAS operations */

void vectorMatrixMultOrig (char Trans, double alpha, matrixD *matrix, vectorD *vectorX, double beta, vectorD *vectorY)
{
	if (beta != 1.0)
	{
		scaleVector (vectorY, beta);
	}
	int incx=1;
	int incy=1;
	int rows=matrix->rows;
	int cols=matrix->cols;
	int LDA=rows;
	dgemv_(&Trans, &rows, &cols, &alpha, matrix->mat, &LDA, vectorX->vec, &incx, &beta, vectorY->vec, &incy);
}

void vectorMatrixMult (char Trans, double alpha, matrixD *matrix, vectorD *vectorX, double beta, vectorD *vectorY, vectorD *outputVec)
{
	int incx=1;
	int incy=1;
	int dimension=vectorX->dimension;
	dcopy_(&dimension, vectorY->vec, &incx, outputVec->vec, &incy);
	int rows=matrix->rows;
	int cols=matrix->cols;
	int LDA=rows;
	dgemv_(&Trans, &rows, &cols, &alpha, matrix->mat, &LDA, vectorX->vec, &incx, &beta, outputVec->vec, &incy);
}

void vectorTriagMatrixMult ( char UPLO, char Trans, char Diag, matrixD *inputMat, vectorD *vector, vectorD *outputVec)
{
	int incx=1;
	int incy=1;
	int dimension=vector->dimension;
	dcopy_(&dimension, vector->vec, &incx, outputVec->vec, &incy);
	int rows=inputMat->rows;
	int LDA=rows;
	dtrmv_(&UPLO, &Trans, &Diag, &rows, inputMat->mat, &LDA, outputVec->vec, &incx);
}

void matrixRankOne ( double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(inputMat->rows)*(inputMat->cols);
	dcopy_(&dimension, inputMat->mat, &incx, outputMat->mat, &incy);
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	int LDA=rows;
	dger_(&rows, &cols, &alpha, vectorX->vec, &incx, vectorY->vec, &incy, outputMat->mat, &LDA);
}

void matrixRankOneOrig ( double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	int incx=1;
	int incy=1;
	int LDA=rows;
	dger_(&rows, &cols, &alpha, vectorX->vec, &incx, vectorY->vec, &incy, inputMat->mat, &LDA);
}

void matrixSymRankOne ( char UPLO, double alpha, vectorD *inputVec, matrixD *inputMat, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(inputMat->rows)*(inputMat->cols);
	dcopy_(&dimension, inputMat->mat, &incx, outputMat->mat, &incy);
	int LDA=inputMat->rows;
	dsyr_(&UPLO, &dimension, &alpha, inputVec->vec, &incx, outputMat->mat, &LDA);
}

void matrixSymRankTwo ( char UPLO, double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(inputMat->rows)*(inputMat->cols);
	dcopy_(&dimension, inputMat->mat, &incx, outputMat->mat, &incy);
	dimension=vectorX->dimension;
	int LDA=inputMat->rows;
	dsyr2_(&UPLO, &dimension, &alpha, vectorX->vec, &incx, vectorY->vec, &incy, outputMat->mat, &LDA);
}

/* Level-3 BLAS functions */

void matrixMultiply (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, matrixD *outputMat)
{
	int rowsA, colsA, colsB;
	if (TransA=='N') {
		rowsA=matrixA->rows;
		colsA=matrixA->cols;
	}
	if (TransA=='T'){
		rowsA=matrixA->cols;
		colsA=matrixA->rows;
	}
	if (TransB=='N') {
		colsB=matrixB->cols;
	}
	if (TransB=='T'){
		colsB=matrixB->rows;
	}
	int LDA=matrixA->rows;
	int LDB=matrixB->rows;
	int LDC=outputMat->rows;
	double beta=0.0;
	dgemm_(&TransA, &TransB, &rowsA, &colsB, &colsA, &alpha, matrixA->mat, &LDA, matrixB->mat, &LDB, &beta, outputMat->mat, &LDC);
}

void matrixMultiplyABPCOrig (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC)
{
	int M,N,K,LDA,LDB,LDC;
	if (TransA=='N') {
		M=matrixA->rows;
		K=matrixA->cols;
		LDA=matrixA->rows;
	}
	else {
		M=matrixA->cols;
		K=matrixA->rows;
		LDA=matrixA->cols;
	}
	if (TransB=='N') {
		K=matrixB->rows;
		N=matrixB->cols;
		LDB=matrixB->cols;
	}
	else {
		K=matrixB->cols;
		N=matrixB->rows;
		LDB=matrixB->rows;
	}
	LDC=matrixC->rows;
	dgemm_(&TransA, &TransB, &M, &N, &K, &alpha, matrixA->mat, &LDA, matrixB->mat, &LDB, &beta, matrixC->mat, &LDC);
}

void matrixMultiplyABPC (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixC->rows)*(matrixC->cols);
	dcopy_(&dimension, matrixC->mat, &incx, outputMat->mat, &incy);
	int M,N,K,LDA,LDB,LDC;
	if (TransA=='N')
	{
		M=matrixA->rows;
		K=matrixA->cols;
		LDA=matrixA->rows;
	}
	else
	{
		M=matrixA->cols;
		K=matrixA->rows;
		LDA=matrixA->cols;
	}
	if (TransB=='N')
	{
		K=matrixB->rows;
		N=matrixB->cols;
		LDB=matrixB->rows;
	}
	else
	{
		K=matrixB->cols;
		N=matrixB->rows;
		LDB=matrixB->cols;
	}
	LDC=matrixC->rows;

	dgemm_(&TransA, &TransB, &M, &N, &K, &alpha, matrixA->mat, &LDA, matrixB->mat, &LDB, &beta, outputMat->mat, &LDC);
}

void matrixSymmRankKOrig (char UPLO, char Trans, double alpha, matrixD *matrixA, double beta, matrixD *matrixC)
{
	int N,K,LDA,LDC;
	if (Trans=='N')
	{
		N=matrixA->rows;
		K=matrixA->cols;
		LDA=matrixA->rows;
	}
	else
	{
		K=matrixA->rows;
		N=matrixA->cols;
		LDA=matrixA->cols;
	}
	LDC=matrixC->rows;
	dsyrk_(&UPLO, &Trans, &N, &K, &alpha, matrixA->mat, &LDA, &beta, matrixC->mat, &LDC);
}
void matrixSymmRankK (char UPLO, char Trans, double alpha, matrixD *matrixA, double beta, matrixD *matrixC, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixC->rows)*(matrixC->cols);
	dcopy_(&dimension, matrixC->mat, &incx, outputMat->mat, &incy);
	int N,K,LDA,LDC;
	if (Trans=='N')
	{
		N=matrixA->rows;
		K=matrixA->cols;
		LDA=matrixA->rows;
	}
	else
	{
		K=matrixA->rows;
		N=matrixA->cols;
		LDA=matrixA->cols;
	}
	LDC=matrixC->rows;
	dsyrk_(&UPLO, &Trans, &N, &K, &alpha, matrixA->mat, &LDA, &beta, outputMat->mat, &LDC);
}

void matrixSymmRank2K (char UPLO, char Trans, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixC->rows)*(matrixC->cols);
	dcopy_(&dimension, matrixC->mat, &incx, outputMat->mat, &incy);
	int N,K,LDA,LDB,LDC;
	if (Trans=='N')
	{
		N=matrixA->rows;
		K=matrixA->cols;
		LDA=matrixA->rows;
		LDB=matrixA->rows;
	}
	else
	{
		N=matrixA->cols;
		K=matrixA->rows;
		LDB=matrixA->cols;
	}
	LDC=matrixC->rows;
	dsyr2k_(&UPLO, &Trans, &N, &K, &alpha, matrixA->mat, &LDA, matrixB->mat, &LDB, &beta, outputMat->mat, &LDC);
}

/*Extra Functions added */

void matrixHadamardProd (matrixD *matrixA, matrixD *matrixB, matrixD *outputMat)
{
	int i;
	int numElem=(matrixA->rows)*(matrixA->cols);
	for (i=0;i<numElem; i++)
	{
		outputMat->mat[i]=matrixA->mat[i]*matrixB->mat[i];
	}
}

void matrixAddition (double alpha, matrixD *matrixA, double beta, matrixD *matrixB, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixB->rows)*(matrixB->cols);
	dcopy_(&dimension, matrixB->mat, &incx, outputMat->mat, &incy);
	dscal_(&dimension, &beta, outputMat->mat, &incx);
	daxpy_(&dimension, &alpha, matrixA->mat, &incx, outputMat->mat, &incy);
}

void matrixAdditionOrig (double alpha, matrixD *matrixA, double beta, matrixD *matrixB)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixB->rows)*(matrixB->cols);
	dscal_(&dimension, &beta, matrixB->mat, &incx);
	daxpy_(&dimension, &alpha, matrixA->mat, &incx, matrixB->mat, &incy);
}

double matrixQuad (vectorD *vecA, char Trans, matrixD *matA, vectorD *vecB)
{
	vectorD *vecTmp=malloc (sizeof (vectorD));
	if (Trans=='T')
	{
		vectorInit (vecTmp, matA->cols);
	}
	else
	{
		vectorInit (vecTmp, matA->rows);
	}
	vectorMatrixMultOrig (Trans, 1.0, matA, vecB, 0, vecTmp);
	double output=vectorDotProd (vecA, vecTmp);
	freeVectorD (vecTmp);
	return output;
}

long indexOfVectorFromMatrix (vectorD *inputVector, matrixD *inputMatrix, double precision)
{
	double secondNormValue = DBL_MAX;
	if (inputVector->dimension != inputMatrix->rows){
		printf ("ERROR: indexOfVectorFromMatrix: input vector and input matrix are incompatible\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	vectorD *tmp = malloc (sizeof(vectorD));
	vectorInit (tmp, inputVector->dimension);
	long i = 0;
	while (i<inputMatrix->cols && secondNormValue > precision){
		vecExtractMat (tmp, inputMatrix, i);
		vectorAddOrig (1.0, inputVector, -1.0, tmp);
		secondNormValue = vectorNorm2 (tmp);
		i++;
	}
	if (i == inputMatrix->cols && secondNormValue > precision){  //vector doesn't belong to the matrix.
		return -1;
	}
	return i-1;
}

double distTwoVec (vectorD *vec1, vectorD *vec2)
{
	if (vec1->dimension != vec2->dimension){
		printf ("ERROR: distTwoVec: Vectors are not of same dimension\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i;
	double distance = 0;
	for (i=0; i<vec1->dimension; i++){
		distance += pow (vec1->vec[i] - vec2->vec[i], 2);
	}
	distance = sqrt (distance);
	return distance;
}

void distVecMat (vectorD *inputVec, matrixD *inputMat, vectorD *distanceVec)
{
	if (inputVec->dimension != inputMat->rows || distanceVec->dimension != inputMat->cols){
		printf ("ERROR: distVecMat: vector and matrix are not of same dimension\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i,j;
	for (i=0; i<inputMat->cols; i++){
		distanceVec->vec[i] = 0;
		for (j=0; j<inputMat->rows; j++){
			distanceVec->vec[i] += pow (inputVec->vec[j] - inputMat->mat[i*inputMat->rows+j], 2);
		}
		distanceVec->vec[i] = sqrt (distanceVec->vec[i]);
	}
}


