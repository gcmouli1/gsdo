/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _VECMATDECLARE_H
#define _VECMATDECLARE_H
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<stdbool.h> /* use C99 or above versions */
#include<float.h>
#include<limits.h>
#include<string.h>
#include<ctype.h>
#include<time.h>

#define INFS DBL_MAX
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)
#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

typedef struct
{
  int dimension;
  int *vec;
} vectorZ;

typedef struct
{
  int dimension;
  double *vec;
} vectorD;

typedef struct
{
  int rows;
  int cols;
  long size;
  double *mat;
} matrixD;

typedef struct
{
	float value;
	int index;
} vecCompareStruct;

/*Initialize vector and matrix */
void vectorInitZ (vectorZ *inputVec, int numElem);
void vectorInitZWithValue (vectorZ *inputVec, int numElem, int value);
void vectorInit(vectorD *inputVec, int numElem);
void vectorInitWithValue(vectorD *inputVec, int numElem, double value);
void matrixInit(matrixD *inputMat, int rows, int cols);
void matrixInitWithValue(matrixD *inputMat, int rows, int cols, double value);
void matrixInitFromMatrix(matrixD *inputMat, matrixD *A);
void setVectorWithValue (vectorD *v, double value);
void sequenceVectorD (vectorD *inputVec, double startValue, double endValue, double interval);
void sequenceVectorZ (vectorZ *inputVec, int startValue, int endValue, int interval);

/*Find value and index of maximum and minimum element */
void vectorMax(vectorD *inputVec, double *maxVal, int *maxIndex);
void vectorMin(vectorD *inputVec, double *minVal, int *minIndex);
void matrixMax(matrixD *inputMat, double *maxVal, int *maxIndex, int *rowNum, int *colNum);
void matrixMin(matrixD *inputMat, double *minVal, int *minIndex, int *rowNum, int *colNum);
void matrixColumnMin (matrixD *inputMat, vectorD *columnMinVector);
void matrixColumnMax (matrixD *inputMat, vectorD *columnMaxVector);
void matrixRowMin (matrixD *inputMat, vectorD *rowMinVector);
void matrixRowMax (matrixD *inputMat, vectorD *rowMaxVector);

/* Print vector and matrix in pretty format */
void printVectorZ (vectorZ *vectorA);
void printVector (vectorD *vectorA);
void printVectorL (vectorD *vectorA, int numElem);
void printMatrix (matrixD *matrixA);
void printMatrixTranspose (matrixD *matrixA);
void printSubMatrix (matrixD *matrixA, int rows, int cols);
void printSubMatrixTranspose (matrixD *matrixA, int rows, int cols);
void printSubVectorD (vectorD *inputVec, int start, int end);
void printSubVectorZ (vectorZ *inputVec, int start, int end);
void printRowSubMatrix (matrixD *matrixA, int rowStartIndex, int rowEndIndex);
void printColSubMatrix (matrixD *matrixA, int colStartIndex, int colEndIndex);
void printSubMatrixUsingIndices (matrixD *matrixA, int *indices, int numElem);

/* Intialize to ones for vectors and matrix */
void vecOnesD (vectorD *vector,  int dimension);
void matOnesD (matrixD *matrix,  int rows,  int cols);

/* Initialize to zeros for vectors and matrix */
void vecZerosZ (vectorZ *vector,  int dimension);
void vecZerosD (vectorD *vector,  int dimension);
void matZerosD (matrixD *matrix,  int rows,  int cols);

/* Intialize to e-vector (vector of zeros except one element with value=1) or identity matrix */
void vecEyeZ (vectorZ *vector,  int elemNum,  int dimension);
void vecEyeD (vectorD *vector,  int elemNum,  int dimension);
void matEyeD (matrixD *matrix,  int rows,  int cols);

/* Create vector and matrix with random elements between the bounds */
void vecRandom (vectorD *inputPt, long l, long u, int randomSeed);
void vecRandomFromBounds (vectorD *inputPt, vectorD *lb, vectorD *ub, int randomSeed);
void matRandom (matrixD *matrix, long lb, long ub, int randomSeed);

/* Create diagonal matrix from vector */
void diagMatrix (vectorD *inputVec, matrixD *outputMat);
void diagMatrixInverse (vectorD *inputVec, matrixD *outputMat);

/* Extract an element a (x) from vector "a" or A (x,y) from matrix "A" */
double elemExtractVecD ( vectorD *vector,  int elemNum);
double elemExtractMatD (matrixD *matrix, int rowNum, int colNum);
void vecExtractElemRange (vectorD *inputVector, int start, int end, vectorD *outputVector);
void vecExtractUsingIndices (vectorD *inputVector, int *indices, int numElem, vectorD *outputVector);

/* Transpose a matrix */
void matrixTranspose (matrixD *inputMat, matrixD *outputMat);
/* Copy upper or lower triangular elements to lower or upper part */
void upperLowerFull (char destination, matrixD *inputMat);

/* Extract columns */
void vecExtractMat (vectorD *vector, matrixD *matrix, int colnum);
void subMatrixExtract (matrixD *inputMat, int rows, int cols, matrixD *outputMat);
void colSubmatrixExtract (matrixD *inputMat, int *colIndex, int colElem, matrixD *outputMat);
void matrixExtractColumns (matrixD *inputMat, int colStartIndex, int colEndIndex, matrixD *outputMat);
void matrixExtractUsingIndices (matrixD *inputMat, int *indices, int numElem, matrixD *outputMat);
void subMatrixExtractUsingIndices (matrixD *inputMat, int *rowIndex, int numRows, int *colIndex, int numCols, matrixD *outputMat);

/* Append vectors and matrices */

void vectorVectorAppend (vectorD *vec1, vectorD *vec2, vectorD *outputVec);
void matrixVectorAppend (char position, matrixD *inputMat, vectorD *inputVec);
void matrixMatrixHorizontalAppend (matrixD *matrix1, matrixD *matrix2);
void matrixMatrixVerticalAppend (matrixD *matrix1, matrixD *matrix2, matrixD *outputMat);
void matrixVectorVerticalAppend (char position, matrixD *inputMat, vectorD *inputVec, matrixD *outputMat);

/* Change from vector to matrix and vice versa */
void vectorMatrixConvert(char type, vectorD *vectorA, matrixD *matrixA);

/*Sorting and finding vector */
int vectorCompareAscend (const void *vec1, const void *vec2);
int vectorCompareDescend (const void *vec1, const void *vec2);
void vectorSort (vectorD *inputPt, int *indices, vectorD *sortedValues, char order, bool inPlace);
void vectorReverse (vectorD *inputVec, int *indices);
int findValueInVector (vectorD *x, double value, double precision);
int findVectorInMatrix (vectorD *x, matrixD *M, int startPtIndex, int endPtIndex, double precision);
//get unique vectors out of A. Store the "numElem" unique indices in "indices".
void uniqueMatrix (matrixD *A, int *indices, int *numElem, double precision);

/* Free the memory */
void freeVectorD (vectorD *vector);
void freeVectorZ (vectorZ *vector);
void freeMatrixD (matrixD *matrix);

#endif
