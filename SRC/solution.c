/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "solution.h"

void initiateSolution (inputData *inputParam, solutionStruct *solution)
{
	int dimension = inputParam->dimension;
	solution->stagnation = false;
	solution->inputPtIndex = -1;
	solution->inputPtObjValue = DBL_MAX;
	solution->IsAprioriNonQRAKInfeasible = false;
	solution->IsPointStatusHidden = false;
	solution->pollCenterIndex = -1;
	solution->pollBestPtIndex = -1;
	solution->leastInfPointIndex = -1;
	solution->infeasMinConstrValue = DBL_MAX;
	solution->stepType = strdup("Initial_Feasible(LH)");
	solution->minMaxDistance = -1;
	solution->optimizationSuccessFul = false;
	solution->feasiblePtFound = false;
	solution->repeatPoint = false;
	solution->totalPts = 0;
	solution->totalNonHiddenPts = 0;
	solution->nonHiddenPtIndex = -1;
	solution->numHiddenConstrPts = 0;
	solution->numAprioriHiddenConstrPts = 0;
	solution->numFeasiblePts = 0;
	solution->numInfeasiblePts = 0;
	solution->numUnrelaxablePts = 0;
	solution->numNQInfeasiblePts = 0;
	solution->bestFeasibleObjectiveValue = DBL_MAX;
	solution->lbScaled = malloc (sizeof(vectorD));
	vecOnesD (solution->lbScaled, inputParam->dimension);
	solution->ubScaled = malloc (sizeof(vectorD));
	vecOnesD (solution->ubScaled, inputParam->dimension);
	scaleVector (solution->lbScaled, -1.0);
	solution->lhMatrixPts = malloc (sizeof(matrixD));
	matrixInit (solution->lhMatrixPts, inputParam->dimension, inputParam->numStartTestPts);	
	solution->allPtsMatrix = malloc (sizeof(matrixD));
	matrixInit (solution->allPtsMatrix, inputParam->dimension, inputParam->maxAllowedEvals);
	solution->objectiveValues = malloc (sizeof(vectorD));
	vectorInit (solution->objectiveValues, inputParam->maxAllowedEvals);
	solution->leastInfPoint = malloc (sizeof(vectorD));
	vectorInit (solution->leastInfPoint, inputParam->dimension);
	solution->allConstraintValues = malloc (sizeof(matrixD));
	matrixInitWithValue (solution->allConstraintValues, inputParam->numConstraints, inputParam->maxAllowedEvals, -DBL_MAX);
	solution->optimSolutionPt = malloc (sizeof(vectorD));
	vectorInit (solution->optimSolutionPt, inputParam->dimension);
	solution->exploitLevel = malloc (sizeof(vectorD));
	vectorInit (solution->exploitLevel, inputParam->numMultiStartPts);
	solution->bestPoint = malloc (sizeof(vectorD));
	vectorInit (solution->bestPoint, dimension);
	solution->pointType = malloc (sizeof(char)*inputParam->maxAllowedEvals);
	memset (solution->pointType,'N',sizeof(char)*inputParam->maxAllowedEvals);
	solution->maxConstrViolReached = malloc (sizeof(char)*inputParam->maxAllowedEvals);
	memset (solution->maxConstrViolReached,'F',sizeof(char)*inputParam->maxAllowedEvals);
	solution->sumConstraints = malloc (sizeof(vectorD));
	vecZerosD (solution->sumConstraints, inputParam->maxAllowedEvals);
	solution->diffMatrix = malloc (sizeof(matrixD));
	matZerosD (solution->diffMatrix, inputParam->maxAllowedEvals, inputParam->maxAllowedEvals);
	solution->pollDirections = malloc (sizeof(matrixD));
	matrixInit (solution->pollDirections, inputParam->dimension, inputParam->dimension+1);
	uniformAngles (solution->pollDirections);
}


//Considering constraints of the form g(x) >= 0
void computeLeastInfeasiblePoint (inputData *inputParam, solutionStruct *solution)
{
	int i;
	double minConstrValue = DBL_MAX;
	double constrVal = 0;
	for (i=0; i<solution->totalPts; i++){
		constrVal = fabs (solution->sumConstraints->vec[i]);
		if (minConstrValue > constrVal && solution->pointType[i] == 'I'){
			minConstrValue = constrVal;
			solution->leastInfPointIndex = i;
		}
	}
	if (solution->leastInfPointIndex >= 0){
		memcpy (solution->leastInfPoint->vec, solution->allPtsMatrix->mat + solution->leastInfPointIndex*inputParam->dimension, sizeof(double)*(inputParam->dimension));
	}
}

bool checkDuplicity (inputData *inputParam, solutionStruct *solution, vectorD *inputPt, bool updateDiff)
{
	int i,j;
	double value;
	int dimension = inputParam->dimension;
	solution->repeatPoint = false;
	vectorD *distVec = malloc (sizeof(vectorD));
	vectorInit (distVec, solution->totalPts);

	for (i=0; i < solution->totalPts; i++){
		value = 0;
		for (j=0; j<dimension; j++){
			value += pow (solution->allPtsMatrix->mat[j+i*dimension] - inputPt->vec[j], 2);
		}
		value = sqrt (value);

		if (value < inputParam->deltaMin){
			solution->IsAprioriNonQRAKInfeasible = false;
			solution->repeatPoint = true;
			solution->inputPtObjValue = solution->objectiveValues->vec[i];
			solution->inputPtIndex = i;
			if (solution->pointType[i] == 'H'){
				solution->IsPointStatusHidden = true;
				solution->feasiblePtFound = false;
			}
			else if (solution->pointType[i] == 'U'){
				solution->IsPointStatusUnrelaxable = true;
				solution->feasiblePtFound = false;
			}
			else if (solution->pointType[i] == 'F'){
				solution->feasiblePtFound = true;
			}
			else if (solution->pointType[i] == 'I'){
				solution->feasiblePtFound = false;
			}
			else if (solution->pointType[i] == 'S'){
				solution->feasiblePtFound = false;
			}
			else {
				printf ("Unknown point\n");
			}
			break;
		}
		distVec->vec[i] = value;
	}
	if (solution->repeatPoint == false){
		int count = 0;
		solution->inputPtIndex = solution->totalPts;
		for (i=0; i < solution->totalPts; i++){
			if (solution->pointType[i] != 'H'){
				count++;
			}
		}
		solution->totalNonHiddenPts = count;
		solution->nonHiddenPtIndex = count;
		if (updateDiff == true){
			updateDistMatrix (inputParam, solution, distVec);
		}
	}

	if (inputParam->debugLogLevel >= 2){
		if (solution->repeatPoint == false){
			printf ("New point found\n");
		}
		else {
			printf ("Repeated Point with index = %d\n",solution->inputPtIndex);
		}
	}
	freeVectorD (distVec);
	return solution->repeatPoint;
}

void updateSolution (inputData *inputParam, solutionStruct *solution, vectorD *inputPt)
{
	int i;
	int dimension = inputParam->dimension;
	for (i=0; i<dimension; i++){
		if (inputPt->vec[i] < inputParam->lb->vec[i] || inputPt->vec[i] > inputParam->ub->vec[i]){
			printf ("ERROR: updateSolution: Point out of bounds. x[%d] = %f while bound values are (%f, %f)\n",i,inputPt->vec[i],inputParam->lb->vec[i],inputParam->ub->vec[i]);
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
	}
	bool betterSolutionFound = false;
	int numEvalPts = solution->totalPts;
	int numConstraints = inputParam->numConstraints;
	solution->IsPointStatusHidden = false;
	solution->IsPointStatusUnrelaxable = false;
	solution->inputPtObjValue = DBL_MAX;
	solution->IsAprioriNonQRAKInfeasible = false;
	solution->feasiblePtFound = true;
	bool updateDiff = true;
	checkDuplicity (inputParam, solution, inputPt, updateDiff);

	if (solution->repeatPoint == true){
		solution->feasiblePtFound = false;
		return;
	}
	if (solution->totalPts == inputParam->maxAllowedEvals){
		printf ("ERROR: updateSolution: Trying to compute beyond already exhausted budget.\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	vectorD *constraintVector = malloc (sizeof(vectorD));
	vectorInit (constraintVector, numConstraints);
	double fValue = DBL_MAX;
	double maxConstraintViolation = 0;
	solution->feasiblePtFound = true;

	computeObjConstrValue (inputParam, inputPt->vec, &fValue, constraintVector->vec, &solution->IsPointStatusHidden);
	if (inputParam->constraintIneqSign == 'L' && solution->IsPointStatusHidden == false){
		scaleVector (constraintVector, -1.0);
	}
	memcpy (solution->allPtsMatrix->mat + numEvalPts * dimension, inputPt->vec, sizeof(double)*dimension); 
	solution->objectiveValues->vec[numEvalPts] = fValue;
	solution->inputPtObjValue = fValue;
	solution->totalPts++;
	solution->inputPtIndex = solution->totalPts-1;

	double tol = -inputParam->precision;
	int count_NaN = 0;
	int count_Unrelax = 0;
	char pt = 'F';
	char ptAorS = 'N';
	int indexQUAK = -1; //index of the only 1 quantifiable QUAK constraint as rest of all constraints are NaN.
	int indexQUSK = -1; //index of the only 1 quantifiable QUSK constraint as rest of all constraints are NaN.
	i = 0;
	if (solution->IsPointStatusHidden == false){
		while (i<inputParam->numConstraints){
			if (isnan (constraintVector->vec[i]) == true && (inputParam->constraintType->vec[i] == inputParam->indexQUAK || inputParam->constraintType->vec[i] == inputParam->indexQUSK)){
				pt = 'H';
				maxConstraintViolation = DBL_MAX;
			}
			if (constraintVector->vec[i] < tol && (pt != 'U') && (pt != 'H')){
				pt = 'I';
				if (maxConstraintViolation > constraintVector->vec[i]){
					maxConstraintViolation = constraintVector->vec[i];
				}
			}
			if ((isnan (constraintVector->vec[i]) == false) && (constraintVector->vec[i] < tol)
					&& (inputParam->constraintType->vec[i] == inputParam->indexQUAK || inputParam->constraintType->vec[i] == inputParam->indexQUSK) && (pt != 'H')){
				if (inputParam->constraintType->vec[i] == inputParam->indexQUAK){
					indexQUAK = i; 
					ptAorS = 'A';
				}
				else {
					indexQUSK = i;
					ptAorS = 'S';
				}
				pt = 'U';
				maxConstraintViolation = DBL_MAX;
				count_Unrelax++;
			}
			if (constraintVector->vec[i] < tol && (inputParam->constraintType->vec[i] == inputParam->indexNUAK || inputParam->constraintType->vec[i] == inputParam->indexNUSK)){
				pt = 'H';
				solution->IsPointStatusHidden = true;
				maxConstraintViolation = DBL_MAX;
				break;
			}
			if (isnan (constraintVector->vec[i])){
				count_NaN++;
			}
			i++;
		}
		if (count_NaN == inputParam->numConstraints || count_Unrelax > 1){
			pt = 'H';
			solution->IsPointStatusHidden = true;
			maxConstraintViolation = DBL_MAX;
		}
		else if (count_NaN > 0 && pt != 'H'){
			pt = 'U';
			maxConstraintViolation = DBL_MAX;
		}
	}
	solution->pointType[numEvalPts] = pt;
	if (solution->IsPointStatusHidden == true){
		solution->pointType[numEvalPts] = 'H';
	}

	if (solution->pointType[numEvalPts] == 'I'){
		maxConstraintViolation = fabs (maxConstraintViolation);
		i = 0;
		while (i<inputParam->numConstraints){
			if (constraintVector->vec[i] < tol && (inputParam->constraintType->vec[i] == inputParam->indexNRAK || inputParam->constraintType->vec[i] == inputParam->indexNRSK)){
				solution->pointType[numEvalPts] = 'S';
			}
			i++;
		}
	}
	if (solution->pointType[numEvalPts] == 'U'){
		solution->IsPointStatusUnrelaxable = true;
		if (ptAorS == 'A'){
			solution->allConstraintValues->mat[numConstraints*numEvalPts+indexQUAK] = constraintVector->vec[indexQUAK];
		}
		else {
			solution->allConstraintValues->mat[numConstraints*numEvalPts+indexQUSK] = constraintVector->vec[indexQUSK];
		}
	}
	if (solution->pointType[numEvalPts] == 'F' || solution->pointType[numEvalPts] == 'I' || solution->pointType[numEvalPts] == 'S'){
		memcpy (solution->allConstraintValues->mat + numConstraints*numEvalPts, constraintVector->vec, sizeof(double)*numConstraints);
	}
	if (solution->pointType[numEvalPts] == 'F'){
		solution->feasiblePtFound = true;
	}
	else {
		solution->feasiblePtFound = false;
	}

	char feasibility[20];
	if (solution->pointType[numEvalPts] == 'H'){
		strcpy (feasibility,"Hidden");
		solution->numHiddenConstrPts++;
	}
	else if (solution->pointType[numEvalPts] == 'U'){
		strcpy (feasibility,"Unrelaxable");
		solution->numUnrelaxablePts++;
	}
	else if (solution->pointType[numEvalPts] == 'S'){
		strcpy (feasibility,"Infeasible");
		solution->numNQInfeasiblePts++;
	}
	else if (solution->pointType[numEvalPts] == 'F'){
		strcpy (feasibility,"Feasible");
		if (fValue < solution->bestFeasibleObjectiveValue){
			solution->bestFeasibleObjectiveValue = fValue;
			solution->bestPtIndex = solution->inputPtIndex;
			memcpy (solution->bestPoint->vec, inputPt->vec, sizeof(double)*dimension);
			betterSolutionFound = true;
		}
		solution->numFeasiblePts++;
	}
	else {
		strcpy (feasibility,"Infeasible");
		solution->numInfeasiblePts++;
	}
	if (solution->pointType[numEvalPts] == 'I'){
		for (i=0; i<inputParam->numConstraints; i++){
			if (constraintVector->vec[i] < tol && (inputParam->constraintType->vec[i] != inputParam->indexNRAK || inputParam->constraintType->vec[i] != inputParam->indexNRSK)){
				solution->sumConstraints->vec[numEvalPts] += constraintVector->vec[i];
			}
		}
	}
	else if (solution->pointType[numEvalPts] == 'S' ||solution->pointType[numEvalPts] == 'U' || solution->pointType[numEvalPts] == 'H') {
		solution->sumConstraints->vec[numEvalPts] = -DBL_MAX;
	}
	else {
		solution->sumConstraints->vec[numEvalPts] = 0;
	}

	if (solution->pointType[numEvalPts] == 'I'){
		double constrVal = fabs (solution->sumConstraints->vec[numEvalPts]);
		if (constrVal < solution->infeasMinConstrValue){
			solution->infeasMinConstrValue = constrVal;
			solution->leastInfPointIndex = numEvalPts;
			memcpy (solution->leastInfPoint->vec, inputPt->vec, sizeof(double)*(inputParam->dimension));
		}
	}

	if (inputParam->debugLogLevel > 0){
		if (solution->IsPointStatusHidden == true){
			char hiddenConstraintViolation = ' ';
			char hiddenPtObjValue = ' ';
			char bestFeasObjValueString[12] = "Inf";
			if (solution->bestFeasibleObjectiveValue == DBL_MAX){
				printf ("%12d | %12c | %12c | %12s | %12s | %22s |\n",solution->totalPts,hiddenPtObjValue, hiddenConstraintViolation, bestFeasObjValueString, feasibility,solution->stepType);
			}
			else {
				printf ("%12d | %12c | %12c | %12g | %12s | %22s |\n",solution->totalPts,hiddenPtObjValue, hiddenConstraintViolation, solution->bestFeasibleObjectiveValue, feasibility,solution->stepType);
			}
		}
		else {
			if (betterSolutionFound == false){
				double bestObj = solution->bestFeasibleObjectiveValue;
				if (fValue == DBL_MAX){
					fValue = HUGE_VAL;
				}
				if (solution->bestFeasibleObjectiveValue == DBL_MAX){
					bestObj = HUGE_VAL;
				}
				if (maxConstraintViolation == DBL_MAX){
					maxConstraintViolation = HUGE_VAL;
				}
				printf ("%12d | %12g | %12g | %12g | %12s | %22s |\n",solution->totalPts,fValue, maxConstraintViolation, bestObj, feasibility,solution->stepType);
			}
			else{
				printf ("%12d | %12g | %12g | %12g | %12s | %22s | <-Solution updated\n",solution->totalPts,fValue, maxConstraintViolation, solution->bestFeasibleObjectiveValue, feasibility,solution->stepType);
			}
		}
	}
	freeVectorD (constraintVector);
}

void coordinateStagnate (solutionStruct *solution, inputData *inputParam)
{
	int i,j;
	int stagnationLength = inputParam->stagnationLength;
	int dimension = inputParam->dimension;
	int offset = solution->totalPts-stagnationLength;
	double *startPt = malloc (sizeof(double)*dimension);
	double *diff = malloc (sizeof(double)*dimension);
	int *count = calloc (dimension, sizeof(int));
	memcpy (startPt, solution->allPtsMatrix->mat+offset*dimension, sizeof(double)*dimension);

	for (i=offset+1; i<solution->totalPts; i++){
		for (j=0; j<dimension; j++){
			diff[j] = solution->allPtsMatrix->mat[j+i*dimension] - startPt[j];
			if (fabs (diff[j]) < 1e-4){
				count[j]++;
			}
		}
	}
	
	solution->stagnation = false;
	for (i=0; i<dimension; i++){
		if (count[i] == stagnationLength-1){
			solution->stagnation = true;
		}
	}
	free (startPt);
	free (diff);
	free (count);
}

//add a new row and new column in diffMatrix with values from distVec.
void updateDistMatrix (inputData *inputParam, solutionStruct *solution, vectorD *distVec)
{
	int i;
	int rows = inputParam->maxAllowedEvals;
	int offset = solution->totalPts;
	for (i=0; i<offset; i++){
		solution->diffMatrix->mat[offset+i*rows] = distVec->vec[i];
	}
	memcpy (solution->diffMatrix->mat+offset*rows, distVec->vec, sizeof(double)*distVec->dimension);
}

void pushInfeasIntoFeasRegion (inputData *inputParam, solutionStruct *solution, vectorD *infPt)
{
	int dimension = inputParam->dimension;
	vectorD *closestFeasPt = malloc (sizeof(vectorD));
	vectorInit (closestFeasPt, dimension);
	vectorD *tmpVector = malloc (sizeof(vectorD));
	vectorInit (tmpVector, dimension);
	int i,j;
	double minDist = DBL_MAX;
	int bestIndex = -1;
	for (i=0; i<solution->totalPts; i++){
		if (solution->pointType[i] == 'F'){
			double dist = 0;
			for (j=0; j<dimension; j++){
				dist += pow (solution->allPtsMatrix->mat[i*dimension+j] - infPt->vec[j], 2);
			}
			dist = sqrt(dist);
			if (dist < minDist){
				minDist = dist;
				bestIndex = i;
			}
		}
	}
	if (bestIndex == -1){
		freeVectorD (closestFeasPt);
		freeVectorD (tmpVector);
		return;
	}
	vecExtractMat (closestFeasPt, solution->allPtsMatrix, bestIndex);
	double alpha = 0.95;
	int numTrials = 4;
	double objOld = solution->objectiveValues->vec[solution->bestPtIndex];
	double objNew = DBL_MAX;
	i = 0;
	while (i < numTrials && solution->totalPts < inputParam->maxAllowedEvals){
		for (j=0; j<dimension; j++){
			tmpVector->vec[j] = alpha*infPt->vec[j] + (1.0-alpha)*closestFeasPt->vec[j];
		}
		updateSolution (inputParam, solution, tmpVector);
		objNew = solution->objectiveValues->vec[solution->inputPtIndex];
		if (solution->repeatPoint == true ||  solution->pointType[solution->totalPts-1] != 'F'){
			alpha *= 0.95; //decrease weightage of infeasible point to push more inside feasible region
		}
		else if (solution->pointType[solution->totalPts-1] == 'F' && solution->bestPtIndex == solution->inputPtIndex && objOld-objNew > 1.0){
			memcpy (closestFeasPt->vec, tmpVector->vec, sizeof(double)*dimension);
			objOld = objNew;
		}
		i++;
	}
	freeVectorD (closestFeasPt);
	freeVectorD (tmpVector);
}

void createNewBounds (inputData *inputParam, solutionStruct *solution, double delta, vectorD *pt)
{
	int i;
	int dimension = inputParam->dimension;
	for (i=0; i<dimension; i++){
		solution->lbScaled->vec[i] = -1.0*delta + pt->vec[i];
		if (solution->lbScaled->vec[i] < inputParam->lb->vec[i]){
			solution->lbScaled->vec[i] = inputParam->lb->vec[i];
		}
	}
	for (i=0; i<dimension; i++){
		solution->ubScaled->vec[i] = 1.0*delta + pt->vec[i];
		if (solution->ubScaled->vec[i] > inputParam->ub->vec[i]){
			solution->ubScaled->vec[i] = inputParam->ub->vec[i];
		}
	}
}

void freeSolution (solutionStruct *solution)
{
	free (solution->stepType);
	free (solution->pointType);
	free (solution->maxConstrViolReached);
	freeVectorD (solution->lbScaled);
	freeVectorD (solution->ubScaled);
	freeVectorD (solution->objectiveValues);
	freeVectorD (solution->leastInfPoint);
	freeVectorD (solution->optimSolutionPt);
	freeVectorD (solution->exploitLevel);
	freeVectorD (solution->bestPoint);
	freeMatrixD (solution->lhMatrixPts);
	freeMatrixD (solution->allPtsMatrix);
	freeMatrixD (solution->allConstraintValues);
	freeMatrixD (solution->diffMatrix);
	freeVectorD (solution->sumConstraints);
	if (solution->pollDirections != NULL){
		freeMatrixD (solution->pollDirections);
	}
	free (solution);
}
