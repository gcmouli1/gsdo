/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "knn.h"

int knn (matrixD *ptMat, double *inputPt, vectorD *distVec, char *classValues, char *classTypes, int numClasses, int *classifier, int k)
{
	int i,j;
	int *indices = malloc (sizeof(int)*ptMat->cols);
	vectorSort (distVec, indices, NULL, 'A', true);
	char *sortedClassValues = malloc (sizeof(char)*ptMat->cols);
	for (i=0; i<ptMat->cols; i++){
		sortedClassValues[i] = classValues[indices[i]];
	}
	free (indices);
	for (i=0; i<k; i++){
		for (j=0; j<numClasses; j++){
			if (sortedClassValues[i] == classTypes[j]){
				classifier[j]++;
			}
		}
	}
	free (sortedClassValues);
	int index = 0;
	double maxVal = 0;
	for (i=0; i<numClasses; i++){
		if (maxVal < classifier[i]){
			maxVal = classifier[i];
			index = i;
		}
	}
	return index;
}

