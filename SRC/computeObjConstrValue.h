/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _COMPUTEOBJCONSTRVALUE_H
#define _COMPUTEOBJCONSTRVALUE_H
#include "inputParameters.h"
#include "customFunctions.h"

void computeObjConstrValue (inputData *inputParam, double *x, double *funcValue, double *constraintValues, bool *hiddenStatus);
void aprioriConstrEval (bool udAprioriConstR, double *x, int dimension, double *constr, int *aprioriConstrType, int numAprioriConstr, int offset, char *aprioriInputPtFile, char *aprioriExec, char *aprioriOutputPtFile, double tol);
void computeObjConstrFromFile (inputData *inputParam, double *x, double *objValue, double *constraintValues, bool *hiddenStatus);
void computeAprioriConstrFromFile (double *x, int dimension, double *constr, char *aprioriInputPtFile, char *aprioriExec, char *aprioriOutputPtFile);
#endif
