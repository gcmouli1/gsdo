/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "latinHypercube.h"
void permutation (long N, vectorD *permArray, int randomSeed)
{
	srand (randomSeed);
	long i=0;
	for (i=0; i<N; i++){
		permArray->vec[i] = i+1;
	}
	for (i = N-1; i >= 0; --i){
		long j = rand() % (i+1);
		long temp = permArray->vec[i];
		permArray->vec[i] = permArray->vec[j];
		permArray->vec[j] = temp;
	}
}

void permutationZ (long N, vectorZ *permArray, int randomSeed)
{
	srand (randomSeed);
	long i=0;
	for (i=0; i<N; i++){
		permArray->vec[i] = i+1;
	}
	for (i = N-1; i >= 0; --i){
		long j = rand() % (i+1);
		long temp = permArray->vec[i];
		permArray->vec[i] = permArray->vec[j];
		permArray->vec[j] = temp;
	}
}

void latinHypercubeUniform (inputData *inputParam, int dimension, int numPts, vectorD *lb, vectorD *ub, matrixD *lhMatrix)
{
	matrixD *intMatrix = malloc (sizeof(matrixD));
	matrixInit (intMatrix, numPts, dimension);
	vectorD *permArray = malloc (sizeof(vectorD));
	vectorInit (permArray, numPts);
	int i=0;
	if (inputParam->randomGeneration == true){
		for (i=0; i<dimension; i++){
			inputParam->randomSeed++;
			permutation (numPts, permArray, inputParam->randomSeed); 
			memcpy (intMatrix->mat + i*numPts, permArray->vec, sizeof(double)*numPts);
		}
	}
	if (inputParam->debugLogLevel >= 4){
		printf ("latinHypercubeUniform: permutation matrix\n");
		printMatrix (intMatrix);
	}
	matrixD *intMatrixTrans = malloc (sizeof(matrixD));
	matrixInit (intMatrixTrans, dimension, numPts);
	matrixTranspose (intMatrix, intMatrixTrans);
	freeMatrixD (intMatrix);

	if (inputParam->randomGeneration == true){
		matRandom (lhMatrix, 0, 1, inputParam->randomSeed);
	}

	matrixAdditionOrig (1.0/numPts, intMatrixTrans, 1.0/numPts, lhMatrix);
	freeMatrixD (intMatrixTrans);
	freeVectorD (permArray);

	if (inputParam->debugLogLevel >= 4){
		printf ("latinHypercubeUniform: lhMatrix before boxed: rows = %d and cols = %d\n",lhMatrix->rows, lhMatrix->cols);
		printMatrix (lhMatrix);
	}

	int k = 0;
	for (k=0; k<numPts; k++){
		for (i=0; i<dimension; i++){
			lhMatrix->mat[i+k*dimension] = lhMatrix->mat[i+k*dimension] * (ub->vec[i]-lb->vec[i]) + lb->vec[i];
			if (lhMatrix->mat[i+k*dimension] > ub->vec[i]){
				lhMatrix->mat[i+k*dimension] = (lhMatrix->mat[i+k*dimension] - ub->vec[i]) + lb->vec[i];
			}
		}
	}
}
