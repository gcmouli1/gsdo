/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _KNN_H
#define _KNN_H
#include "vecMatDeclare.h"

/* Inputs:
 * ptMat: Matrix of given points whose classification is known.
 * inputPt: Input point
 * classValues: Classifier values for each point in matrix ptMat
 * numClasses: Number of classes in the classifier
 * k: Number of points needed for knn algorithm 
 * Outputs:
 * returns class with largest value within k elements.
 * classifier: Output class vector. Number of elements in each class*/

int knn (matrixD *ptMat, double *inputPt, vectorD *distVec, char *classValues, char *classTypes, int numClasses, int *classifier, int k);
#endif
