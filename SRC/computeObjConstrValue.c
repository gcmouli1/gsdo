/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "computeObjConstrValue.h"

void computeObjConstrValue (inputData *inputParam, double *x, double *objValue, double *constraintValues, bool *hiddenStatus)
{
	if (inputParam->udObjConstr == true){
		char pt = 'F';
		userDefinedObjConstrFunc (x, inputParam->dimension, objValue, constraintValues, inputParam->numConstraints, &pt);
		if (pt == 'H'){
			*hiddenStatus = true;
		}
		return;
	}
	else {
		computeObjConstrFromFile (inputParam, x, objValue, constraintValues, hiddenStatus);
	}
}

void computeObjConstrFromFile (inputData *inputParam, double *x, double *objValue, double *constraintValues, bool *hiddenStatus)
{
	int i;
	FILE *fp;
	fp = fopen (inputParam->inputPtFileNameAndPath,"w");
	if (fp == NULL){
		printf ("ERROR: computeObjConstrValue: Input file not found\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	for (i=0; i<inputParam->dimension; i++){
		fprintf (fp, "%.15lf\n", x[i]);
	}
	fclose (fp);
	system (inputParam->dfoExec);
	fp = fopen (inputParam->outputPtFileNameAndPath,"r");
	if (fp == NULL){
		printf ("ERROR: computeObjConstrValue: output file not found\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	i = 0;
	int k = 0;
	double num;
	while (fscanf (fp, "%lf\n",&num) == 1){
		if (k == inputParam->objectiveIndex){
			*objValue = num;
		}
		else{
			constraintValues[i] = num;
			i++;
		}
		k++;
	}
	fclose (fp);
	if (k < inputParam->numConstraints + 1){
		*hiddenStatus = true;
		*objValue = DBL_MAX;
		for (i=0; i<inputParam->numConstraints; i++){
			constraintValues[i] = -DBL_MAX;
		}
	}
}

void computeAprioriConstrFromFile (double *x, int dimension, double *constr, char *aprioriInputPtFile, char *aprioriExec, char *aprioriOutputPtFile)
{
	int i;
	FILE *fp;
	fp = fopen (aprioriInputPtFile,"w");
	if (fp == NULL){
		printf ("ERROR: computeAprioriConstraints: Input file not found\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	for (i=0; i<dimension; i++){
		fprintf (fp, "%.15lf\n", x[i]);
	}
	fclose (fp);
	system (aprioriExec);
	fp = fopen (aprioriOutputPtFile,"r");
	if (fp == NULL){
		printf ("ERROR: computeAprioriConstraints: output file not found\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	i = 0;
	double num;
	while (fscanf (fp, "%lf\n",&num) == 1){
		constr[i] = num;
		i++;
	}
	fclose (fp);
}

void aprioriConstrEval (bool udAprioriConstr, double *x, int dimension, double *constr, int *aprioriConstrType, int numAprioriConstr, int offset, char *aprioriInputPtFile, char *aprioriExec, char *aprioriOutputPtFile, double tol)
{
	char pt = 'F';
	double *value = malloc (sizeof(double)*numAprioriConstr);
	if (udAprioriConstr == true){
		userDefinedAprioriConstrEval (x, dimension, value, numAprioriConstr, &pt);
	}
	else {
		computeAprioriConstrFromFile (x, dimension, value, aprioriInputPtFile, aprioriExec, aprioriOutputPtFile);
	}
	int i, indexQUAK = -1, count_NaN = 0, count_Unrelax = 0;
	double bigNegNum = -1e10;
	if (pt != 'H'){
		pt = 'F';
		for (i=0; i<numAprioriConstr; i++){
			if ((isnan (value[i]) == true) && aprioriConstrType[i] == 3){
				pt = 'H';
			}
			if (value[i] < tol && (pt != 'U' || pt != 'H')){
				pt = 'I';
			}
			if ((isnan (value[i]) == false) && (value[i] < tol) && (aprioriConstrType[i] == 3) && (pt != 'H')){
				indexQUAK = i; 
				pt = 'U';
				count_Unrelax++;
			}
			if (value[i] < tol && (aprioriConstrType[i] == 4)){
				pt = 'H';
				break;
			}
			if (isnan (value[i])){
				count_NaN++;
			}
		}
		if (count_NaN == numAprioriConstr || count_Unrelax > 1){
			pt = 'H';
		}
		else if (count_NaN > 0 && pt != 'H'){
			pt = 'U';
		}
	}
	if (pt == 'I'){
		for (i=0; i<numAprioriConstr; i++){
			if (value[i] < tol && (aprioriConstrType[i] == 2)){
				value[i] = bigNegNum;
			}
		}
	}
	if (pt == 'H'){
		for (i=0; i<numAprioriConstr; i++){
			value[i] = bigNegNum;
		}
	}
	if (pt == 'U'){
		for (i=0; i<numAprioriConstr; i++){
			if (i != indexQUAK){
				value[i] = bigNegNum;
			}
		}
	}
	if (pt == 'F'){
		for (i=0; i<numAprioriConstr; i++){
			if (aprioriConstrType[i] == 2 || aprioriConstrType[i] == 4){
				value[i] = 1;
			}
		}
	}

	memcpy (constr+offset, value, sizeof(double)*numAprioriConstr);
	free (value);
}
