/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _GLOBALAPPROACH_H
#define _GLOBALAPPROACH_H
#include "rbfOptUsingDE.h"
#include "multipleFeasiblePoints.h"

void globalApproach (inputData *inputParam, solutionStruct *solution);
void bestSolExploitation (inputData* inputParam, solutionStruct *solution, vectorD *solutionValues, matrixD *multiStart, int numSolutions);
void globalExploration (inputData* inputParam, solutionStruct *solution);
void exploitLocalMin (inputData *inputParam, solutionStruct *solution, matrixD *multiStart, vectorD *solutionValues, int numSolutions);
char detectSearchType (inputData *inputParam, solutionStruct *solution, int iterations, double randomNum);
#endif
