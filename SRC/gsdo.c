/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "gsdo.h"

void gsdo(int argc, char **argv)
{
	time_t startTime, endTime, startTime0;
	double cpuTime = -1;
	startTime = time(NULL);
	startTime0 = startTime;
	printf ("-------------------------------------------------------------------------------\n");
	printf ("This program contains GSDO, a library for derivative free optimization.        \n");
	printf ("GSDO is released as open source code under the GNU General Public License v3.0.\n");
	printf ("The code is available at https://gitlab.com/gcmouli1/gsdo.                     \n");
	printf ("-------------------------------------------------------------------------------\n");
	inputData *inputParam = malloc (sizeof(inputData));
	inputParametersInit (inputParam, argv);
	endTime = time(NULL);
	cpuTime = (double)(endTime-startTime);
	srand (inputParam->randomSeed);
	int i;

	solutionStruct *solution = malloc (sizeof(solutionStruct));
	initiateSolution (inputParam, solution);
	char iteration[20] = "Iteration";
	char objValue[20] = "Obj_Value";
	char constrViol[20] = "Max_Constr";
	char bestObj[20] = "Best_Obj";
	char feasibility[20] = "Feasibility";
	char stepType[20] = "Search_Type";
	int stage1Pts = 0;
	int stage2Pts = 0;
	int stage1FeasiblePts = 0;
	int stage2FeasiblePts = 0;
	bool startStage2 = true;
	bool startStage3 = true;

	if (inputParam->debugLogLevel >= 1){
		printf ("Problem information:\n");
		printf ("Dimension of the problem            : %d\n",inputParam->dimension);
		printf ("Number of constraints in the problem: %d\n",inputParam->numConstraints);
		if (inputParam->numTypeConstraints[inputParam->indexQRAK-1] > 0){
			printf ("Number of QRAK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexQRAK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexQUAK-1] > 0){
			printf ("Number of QUAK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexQUAK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexNRAK-1] > 0){
			printf ("Number of NRAK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexNRAK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexNUAK-1] > 0){
			printf ("Number of NUAK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexNUAK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexQRSK-1] > 0){
			printf ("Number of QRSK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexQRSK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexQUSK-1] > 0){
			printf ("Number of QUSK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexQUSK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexNRSK-1] > 0){
			printf ("Number of NRSK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexNRSK-1]);
		}
		if (inputParam->numTypeConstraints[inputParam->indexNUSK-1] > 0){
			printf ("Number of NUSK constraints          : %d\n",inputParam->numTypeConstraints[inputParam->indexNUSK-1]);
		}
		printf ("Max alloted budget                  : %d\n",inputParam->maxAllowedEvals);
		printf ("Time taken in reading the problem data = %f sec\n",cpuTime);
		printf ("\n\n************************************************************************************************\n");
	}
	if (solution->numFeasiblePts < 1){
		if (inputParam->debugLogLevel >= 1){
			printf ("Starting stage-1. Find an initial feasible point\n");
			printf ("====================================================================================================\n");
			printf ("%12s | %12s | %12s | %12s | %12s | %22s\n", iteration, objValue,constrViol, bestObj, feasibility, stepType);
			printf ("----------------------------------------------------------------------------------------------------\n");
		}
		startTime = time(NULL);
		feasiblePtGenerate (inputParam, solution);
		endTime = time(NULL);
		cpuTime = (double)(endTime-startTime);
		if (inputParam->debugLogLevel >= 1){
			printf ("====================================================================================================\n");
		}
		stage1Pts = solution->totalPts;
		stage1FeasiblePts = solution->numFeasiblePts;
		if (solution->totalPts >= inputParam->maxAllowedEvals && inputParam->debugLogLevel > -1){
			printf ("WARNING: Budget exhausted in stage-1. Stopping the algorithm\n");
			printf ("------------------------------------------------------------\n");
			startStage2 = false;
		}
		if (inputParam->debugLogLevel >= 1){
			printf ("Stage-1 Summary:\n");
			printf ("----------------------------------------\n");
			printf ("number of points evaluated : %d\n",solution->totalPts);
			printf ("number of feasible points  : %d\n",solution->numFeasiblePts);
			printf ("number of infeasible points: %d\n",solution->numInfeasiblePts + solution->numNQInfeasiblePts);
			printf ("number of hidden points    : %d\n",solution->numHiddenConstrPts);
			printf ("Time taken in Stage-1      : %4.2f sec\n",cpuTime);
			printf ("----------------------------------------\n");
		}
	}
	else {
		if (inputParam->debugLogLevel >= 1){
			printf ("feasible point is available from given input set\n");
		}
	}

	if (solution->numFeasiblePts > 0 && startStage2 == true){
		if (solution->numFeasiblePts < inputParam->maxInitialFeasiblePoints){
			if (inputParam->debugLogLevel >= 1){
				printf ("\n\n************************************************************************************************\n");
				printf ("Starting stage-2. Creating multiple feasible points\n");
				printf ("====================================================================================================\n");
				printf ("%12s | %12s | %12s | %12s | %12s | %22s\n", iteration, objValue,constrViol, bestObj, feasibility, stepType);
				printf ("----------------------------------------------------------------------------------------------------\n");
			}
			startTime = time(NULL);
			multipleFeasiblePts (inputParam, solution, inputParam->maxInitialFeasiblePoints);
			endTime = time(NULL);
			cpuTime = (double)(endTime-startTime);
			if (inputParam->debugLogLevel >= 1){
				printf ("====================================================================================================\n");
			}
			if (solution->totalPts >= inputParam->maxAllowedEvals && inputParam->debugLogLevel > -1){
				printf ("WARNING: Budget exhausted in stage-2. Stopping the algorithm\n");
				printf ("------------------------------------------------------------\n");
				startStage3 = false;
			}
			stage2Pts = solution->totalPts;
			stage2FeasiblePts = solution->numFeasiblePts;
			if (inputParam->debugLogLevel >= 1){
				printf ("Stage-2 Summary:\n");
				printf ("---------------------------------------------------\n");
				printf ("total number of points evaluated      : %d\n",solution->totalPts);
				printf ("number of points evaluated in Stage-2 : %d\n",solution->totalPts - stage1Pts);
				printf ("number of feasible points             : %d\n",solution->numFeasiblePts);
				printf ("number of feasible points in Stage-2  : %d\n",solution->numFeasiblePts - stage1FeasiblePts);
				printf ("number of infeasible points           : %d\n",solution->numInfeasiblePts + solution->numNQInfeasiblePts);
				printf ("number of hidden points               : %d\n",solution->numHiddenConstrPts);
				printf ("Time taken in Stage-2                 : %4.2f sec\n",cpuTime);
				printf ("---------------------------------------------------\n");
			}
		}
		else {
			if (inputParam->debugLogLevel >= 1){
				printf ("INFO: Multiple feasible points are already available. Bypassing Stage-2.\n");
			}
		}

		if (startStage3 == true){
			if (inputParam->debugLogLevel >= 1){
				printf ("\n\n************************************************************************************************\n");
				printf ("Starting global approach\n");
				printf ("====================================================================================================\n");
				printf ("%12s | %12s | %12s | %12s | %12s | %22s\n", iteration, objValue,constrViol, bestObj, feasibility, stepType);
				printf ("----------------------------------------------------------------------------------------------------\n");
			}
			startTime = time(NULL);
			globalApproach (inputParam, solution);
			endTime = time(NULL);
			cpuTime = (double)(endTime-startTime);
			if (inputParam->debugLogLevel >= 1){
				printf ("====================================================================================================\n");
			}
			if (solution->totalPts >= inputParam->maxAllowedEvals && inputParam->debugLogLevel > -1){
				printf ("WARNING: Budget exhausted in stage-3. Stopping the algorithm\n");
				printf ("------------------------------------------------------------\n");
			}
			if (inputParam->debugLogLevel >= 1){
				printf ("Stage-3 Summary:\n");
				printf ("---------------------------------------------------\n");
				printf ("number of points evaluated            : %d\n",solution->totalPts);
				printf ("number of points evaluated in Stage-3 : %d\n",solution->totalPts - stage2Pts);
				printf ("number of feasible points             : %d\n",solution->numFeasiblePts);
				printf ("number of feasible points in Stage-3  : %d\n",solution->numFeasiblePts - stage2FeasiblePts);
				printf ("number of infeasible points           : %d\n",solution->numInfeasiblePts + solution->numNQInfeasiblePts);
				printf ("number of hidden points               : %d\n",solution->numHiddenConstrPts);
				printf ("Time taken in Stage-3                 : %4.2f sec\n",cpuTime);
				printf ("---------------------------------------------------\n");
			}
		}
	}

	double minObjValue = DBL_MAX;
	for (i=0; i<solution->totalPts; i++){
		if (solution->pointType[i] == 'F'){
			if (minObjValue > solution->objectiveValues->vec[i]){
				minObjValue = solution->objectiveValues->vec[i];
			}
		}
	}

	if (solution->numFeasiblePts > 0){
		if (inputParam->debugLogLevel >= 1){
			printf ("minimum value is %f\n",minObjValue);
			printf ("Optimal point is \n");
			printVector (solution->bestPoint);
		}
	}
	else {
		if (inputParam->debugLogLevel >= 1){
			printf ("No feasible point found. Need more budget\n");
		}
	}

	if (inputParam->OutputLevel == 0 || inputParam->OutputLevel == 1 || inputParam->OutputLevel == 2 || inputParam->OutputLevel == 3){
		FILE *fp;
		char filePath[1024];
		strcpy (filePath,inputParam->outputFolderPath);
		strcat (filePath, "/obj_values.csv");
		fp = fopen (filePath,"w");
		if (fp == NULL){
			printf ("Path to obj_values.csv file not found\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		int i,j;
		for (i=0; i<solution->totalPts; i++){
			fprintf (fp, "%.15lf\n", solution->objectiveValues->vec[i]);
		}
		fclose (fp);
		strcpy (filePath,inputParam->outputFolderPath);
		strcat (filePath, "/feasibility.csv");
		fp = fopen (filePath,"w");
		if (fp == NULL){
			printf ("Path to feasibility.csv file not found\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		for (i=0; i<solution->totalPts; i++){
			fprintf (fp, "%c\n", solution->pointType[i]);
		}
		fclose (fp);
		if (inputParam->OutputLevel == 1 || inputParam->OutputLevel == 2 || inputParam->OutputLevel == 3){
			strcpy (filePath,inputParam->outputFolderPath);
			strcat (filePath, "/points.csv");
			fp = fopen (filePath,"w");
			if (fp == NULL){
				printf ("Path to points.csv file not found\n");
				printf ("PROCESS TERMINATED\n");
				exit (EXIT_FAILURE);
			}
			matrixD *evalPtsMatrix = malloc (sizeof(matrixD));
			matrixInit (evalPtsMatrix, inputParam->dimension, solution->totalPts);
			matrixExtractColumns (solution->allPtsMatrix, 0, solution->totalPts-1, evalPtsMatrix);
			for (i=0; i<evalPtsMatrix->cols; i++){
				for (j=0; j<evalPtsMatrix->rows-1; j++){
					fprintf (fp, "%.15lf,", evalPtsMatrix->mat[j+i*evalPtsMatrix->rows]);
				}
				fprintf (fp, "%.15lf\n", evalPtsMatrix->mat[evalPtsMatrix->rows-1+i*evalPtsMatrix->rows]);
			}
			freeMatrixD (evalPtsMatrix);
			fclose (fp);
		}
		if (inputParam->OutputLevel == 2 || inputParam->OutputLevel == 3){
			strcpy (filePath,inputParam->outputFolderPath);
			strcat (filePath, "/constraints.csv");
			fp = fopen (filePath,"w");
			if (fp == NULL){
				printf ("Path to constraints.csv file not found\n");
				printf ("PROCESS TERMINATED\n");
				exit (EXIT_FAILURE);
			}
			matrixD *constraintMatrix = malloc (sizeof(matrixD));
			matrixInit (constraintMatrix, inputParam->numConstraints, solution->totalPts);
			matrixExtractColumns (solution->allConstraintValues, 0, solution->totalPts-1, constraintMatrix);
			for (i=0; i<constraintMatrix->cols; i++){
				for (j=0; j<constraintMatrix->rows-1; j++){
					fprintf (fp, "%.15lf,", constraintMatrix->mat[j+i*constraintMatrix->rows]);
				}
				fprintf (fp, "%.15lf\n", constraintMatrix->mat[constraintMatrix->rows-1+i*constraintMatrix->rows]);
			}
			freeMatrixD (constraintMatrix);
			fclose (fp);
		}
	}
	freeSolution (solution);
	freeInputParameters (inputParam);
	remove ("INPUT/input.txt");
	remove ("INPUT/output.txt");
	endTime = time(NULL);
	cpuTime = (double)(endTime-startTime0);
	printf ("Total time taken by gsdo solver = %4.2f sec\n",cpuTime);
	printf ("PROCESS FINISHED\n");
}
