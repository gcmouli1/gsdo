/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "customFunctions.h"

void userDefinedObjConstrFunc (double *x, int dimension, double *funcValue, double *constraintValues, int numConstraints, char *pointType)
{

}

void userDefinedAprioriConstrEval (double *x, int dimension, double *constrVal, int numAprioriConstr, char *pointType)
{
	constrVal[0] = -1.0*(-pow (x[0]-5,2) - pow (x[1]-5,2) + 100)/100;
}
