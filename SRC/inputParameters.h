/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include <math.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <stdbool.h> /* use C99 or above versions */
#include "vecLapack.h"
#ifndef _INPUTPARAMETERS_H
#define _INPUTPARAMETERS_H
#ifndef INFS
#define INFS DBL_MAX
#endif
#ifndef MAX
#define MAX(a,b) ((a) > (b) ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? a : b)
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
typedef struct
{
	char dfoExec[1024];
	char inputPtFileNameAndPath[1024];
	char outputPtFileNameAndPath[1024];
	char aprioriInputPtFile[1024];
	char aprioriExec[1024];
	char aprioriOutputPtFile[1024];
	char ptGenerationApproach[50];
	char outputFolderPath[1024];
	char *rbfType;
	char constraintIneqSign; 
	char rbfTypeSingle;
	bool staticGeneration;
	bool randomGeneration;
	bool userDefinedPtGeneration;
	bool loadFromFile;
	bool objOverFeasblePtsOnly; 
	bool NUSH; 
	bool hiddenConstraintExists;
	bool useRBF;
	bool autoSelectModel;
	bool includePolyTail;
	bool singleRBFtype;
	bool udObjConstr;
	bool udCustomAprioriConstr;

	int debugLogLevel;
	int dimension;
	int numConstraints;
	int maxAllowedEvals;
	int maxAprioriHiddenPoints;
	int largePoolSize;
	int numStartTestPts;
	int numStartPtsInitialFeasOpt;
	int numRestartPts;
	int maxInitialFeasiblePoints;
	int maxMFIterations;
	int maxVicinityPts;
	int numMultiStartPts;
	int numGlobalOptAttempts;
	int numGlobalStartPts;
	int initialReqGlobalIters;
	int OutputLevel;
	int maxGlobalIterations;
	int maxAllowedEvalsStage2; 
	int objectiveIndex;
	int randomSeed;
	int numConstraintTypes;
	int numAprioriConstraints;
	int numPointsToLoad;
	int minGlobalItersSatisfied;
	int iterHalton;
	int indexQRAK;
	int indexNRAK;
	int indexQUAK;
	int indexNUAK;
	int indexQRSK;
	int indexNRSK;
	int indexQUSK;
	int indexNUSK;
	int rbfTypeObj;
	int rbfParamObj;
	int numRBFTypes;
	int numLeaveOuts;
	int stagnationLength;

	double *rbfParam;
	double Inf;
	double stepLength;
	double constraintPrecision;
	double deltaMin;
	double minSideLength;
	double maxSideLength;
	double perturbation;
	double deltaExploitation;
	double deltaTerminateGlobal;
	double precision;
	double rbfParamSingle;
	int *numTypeConstraints;
	int knnParam;
	vectorD *lb;
	vectorD *ub;
	matrixD *inputPoints;
	matrixD *inputConstraints;
	vectorZ *indexFeasibility;
	vectorD *objValues;
	vectorZ *constraintType;
} inputData;

int countWords (char *line);
void inputParametersInit (inputData *inputParam, char **argv);
void freeInputParameters (inputData *inputParam);
#endif
