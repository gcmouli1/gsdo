/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _MULTIPLEFEASIBLEPOINTS_H
#define _MULTIPLEFEASIBLEPOINTS_H
#include "localSearch.h"

typedef struct
{
	int numConstraintsQRAK;
	int numConstraintsQRSK;
	int numConstraintsQUSK;
	int numConstraintsDC;
	int numAprioriNonQRAKConstraints;
	int sizeConstraintCoeffQRSK;
	int sizeConstraintCoeffDC;
	matrixD *evalPtsMatrix;
	matrixD *evalFeasiblePtsMatrix;
	matrixD *constraintCoeffQRSK;
	matrixD *constraintCoeffDC;
	vectorD *coefficientsQRSK;
	vectorD *coefficientsDC;
} computeFeasibilityStruct;


void multipleFeasiblePts (inputData* inputParam, solutionStruct *solution, int maxInitialFeasiblePoints);
#endif
