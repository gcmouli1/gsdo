/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "vecLapack.h"
/* copies X into Y */
void copyMatrixLAPACK (char UPLO, matrixD *matrix1,  matrixD *matrix2)
{
	int rows=matrix1->rows;
	int cols=matrix1->cols;
	int LDA=rows;
	int LDB=rows;
	matrix2->rows=rows;
	matrix2->cols=cols;
	dlacpy_(&UPLO, &rows, &cols, matrix1->mat, &LDA, matrix2->mat, &LDB);
}

void packedToFullMatrix (char UPLO, matrixD *inputMat, matrixD *outputMat)
{
	int rows=inputMat->rows;
	int LDA=rows;
	int INFO;
	dtpttr_(&UPLO, &rows, inputMat->mat, outputMat->mat, &LDA, &INFO);
}

void linearSystem (matrixD *A, vectorD *B, vectorD *outputVec, int *info)
{
	matrixD *Atemp=malloc (sizeof (matrixD));
	matrixInitFromMatrix (Atemp, A);
	copyMatrix (A, Atemp);
	int dimension=B->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, B->vec, &incx, outputVec->vec, &incy);
	int rows=A->rows;
	int LDA=rows;
	int LDB=rows;
	int NRHS=1;
	int *IPIV=malloc (sizeof (int)*rows);
	int INFO=-1;
	dgesv_(&rows, &NRHS, Atemp->mat, &LDA, IPIV, outputVec->vec, &LDB, &INFO);
	*info=INFO;
	if (INFO < 0)
	{
		 printf("%d th argument has illegal value\n",INFO);
	}
	if (INFO > 0)
	{
		 printf("LU decomposition failed. U is singular\n");
	}
	free (IPIV);
	freeMatrixD (Atemp);
}

void linearSystemMatrix (matrixD *A, matrixD *B, matrixD *outputMat, int *info)
{
	matrixD *Atemp=malloc (sizeof (matrixD));
	matrixInitFromMatrix (Atemp, A);
	copyMatrix (A, Atemp);
	copyMatrix (B, outputMat);
	int rows=A->rows;
	int LDA=rows;
	int LDB=B->rows;
	int NRHS=B->cols;
	int *IPIV=malloc (sizeof (int)*rows);
	int INFO=-1;
	dgesv_(&rows, &NRHS, Atemp->mat, &LDA, IPIV, outputMat->mat, &LDB, &INFO);
	*info=INFO;
	if (INFO < 0)
	{
		 printf("%d th argument has illegal value\n",INFO);
	}
	if (INFO > 0)
	{
		 printf("U in LU is singular\n");
	}
	free (IPIV);
	freeMatrixD (Atemp);
}
void linearSystemSym (char UPLO, matrixD *A, vectorD *B, vectorD *outputVec)
{
	matrixD *Atemp=malloc (sizeof (matrixD));
	matrixInitFromMatrix (Atemp, A);
	copyMatrixLAPACK (UPLO, A, Atemp);
	int dimension=B->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, B->vec, &incx, outputVec->vec, &incy);
	int rows=A->rows;
	int LDB=rows;
	int NRHS=1;
	int INFO=-1;
	dppsv_(&UPLO, &rows, &NRHS, Atemp->mat, outputVec->vec, &LDB, &INFO);
	freeMatrixD (Atemp);
}

void linearSystemSymOrig (char UPLO, matrixD *A, vectorD *B, vectorD *outputVec)
{
	int dimension=B->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, B->vec, &incx, outputVec->vec, &incy);
	int rows=A->rows;
	int LDB=rows;
	int NRHS=1;
	int INFO=-1;
	dppsv_(&UPLO, &rows, &NRHS, A->mat, outputVec->vec, &LDB, &INFO);
}

void leastSquare (char TRANS, matrixD *A, vectorD *B, vectorD *outputVec)
{
	matrixD *Atemp=malloc (sizeof (matrixD));
	matrixInitFromMatrix (Atemp, A);
	copyMatrix (A, Atemp);
	vectorD *vtemp=malloc (sizeof (vectorD));
	vectorInit (vtemp, B->dimension);

	int dimension=B->dimension;
	int incx=1;
	int incy=1;
	dcopy_(&dimension, B->vec, &incx, vtemp->vec, &incy);
	int rows=A->rows;
	int cols=A->cols;
	int LDA=rows;
	int LDB=B->dimension;
	int NRHS=1;
	double wkopt;
	int LWORK=-1;
	int INFO=-1;
	dgels_(&TRANS, &rows, &cols, &NRHS, Atemp->mat, &LDA, vtemp->vec, &LDB, &wkopt, &LWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgels_(&TRANS, &rows, &cols, &NRHS, Atemp->mat, &LDA, vtemp->vec, &LDB, WORK, &LWORK, &INFO);
	memcpy (outputVec->vec, vtemp->vec, sizeof(double)*(A->cols));
	freeMatrixD (Atemp);
	freeVectorD (vtemp);
	free (WORK);
}

void leastSquareOrig (char TRANS, matrixD *A, vectorD *B)
{
	int rows=A->rows;
	int cols=A->cols;
	int LDA=rows;
	int LDB=B->dimension;
	int NRHS=1;
	double wkopt;
	int LWORK=-1;
	int INFO=-1;
	dgels_(&TRANS, &rows, &cols, &NRHS, A->mat, &LDA, B->vec, &LDB, &wkopt, &LWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgels_(&TRANS, &rows, &cols, &NRHS, A->mat, &LDA, B->vec, &LDB, WORK, &LWORK, &INFO);
	free (WORK);
}

double matrixNorm (char NORM, matrixD *A)
{
	int rows=A->rows;
	int cols=A->cols;
	int LDA=rows;
	double *WORK=malloc (sizeof (double));
	double output=dlange_(&NORM, &rows, &cols, A->mat, &LDA, WORK);
	free (WORK);
	return output;
}

void matrixCholeskyDec (char UPLO, matrixD *matrixA, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixA->rows)*(matrixA->cols);
	dcopy_(&dimension, matrixA->mat, &incx, outputMat->mat, &incy);
	int rows=matrixA->rows;
	int INFO;
	dpotrf_(&UPLO, &rows, outputMat->mat, &rows, &INFO);
}

void matrixCholeskyDecOrig (char UPLO, matrixD *A)
{
	int rows=A->rows;
	int INFO;
	dpotrf_(&UPLO, &rows, A->mat, &rows, &INFO);
}

void matrixQRfact (matrixD *matrixA, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(matrixA->rows)*(matrixA->cols);
	dcopy_(&dimension, matrixA->mat, &incx, outputMat->mat, &incy);
	int rows=matrixA->rows;
	int cols=matrixA->cols;
	int INFO;
	int LDA=rows;
	double wkopt;
	int LWORK=-1;
	int rowcolmin=rows;
	if (rowcolmin>cols)
	{
		rowcolmin=cols;
	}
	double *TAU=malloc (sizeof (double)*rowcolmin);
	dgeqrf_(&rows, &cols, outputMat->mat, &LDA, TAU, &wkopt, &LWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgeqrf_(&rows, &cols, outputMat->mat, &LDA, TAU, WORK, &LWORK, &INFO);
	free (WORK);
	free (TAU);
}

void matrixQRfactOrig (matrixD *A)
{
	int rows=A->rows;
	int cols=A->cols;
	int INFO;
	int LDA=rows;
	double wkopt;
	int LWORK=-1;
	int rowcolmin=rows;
	if (rowcolmin>cols)
	{
		rowcolmin=cols;
	}
	double *TAU=malloc (sizeof (double)*rowcolmin);
	dgeqrf_(&rows, &cols, A->mat, &LDA, TAU, &wkopt, &LWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgeqrf_(&rows, &cols, A->mat, &LDA, TAU, WORK, &LWORK, &INFO);
	free (WORK);
	free (TAU);
}

void matrixQRfactGetQ (matrixD *A, matrixD *outputMat, matrixD *identityMat)
{
	int incx=1;
	int incy=1;
	int dimension=(A->rows)*(A->cols);
	dcopy_(&dimension, A->mat, &incx, outputMat->mat, &incy);
	int rows=A->rows;
	int cols=A->cols;
	int INFO;
	double wkopt;
	int LWORK=-1;
	int rowcolmin=rows;
	if (rowcolmin>cols)
	{
		rowcolmin=cols;
	}
	char side='L';
	char TRANS='N';
	int k=rows;
	int LDC=rows;
	int rowsC=identityMat->rows;
	int colsC=identityMat->cols;
	double *TAU=malloc (sizeof (double)*rowcolmin);
	dormqr_(&side, &TRANS, &rowsC, &colsC, &k, outputMat->mat, TAU, identityMat->mat, &LDC, &wkopt, &LWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dormqr_(&side, &TRANS, &rowsC, &colsC, &k, outputMat->mat, TAU, identityMat->mat, &LDC, WORK, &LWORK, &INFO);
	free (WORK);
	free (TAU);
}

void matrixSVDD (matrixD *A, matrixD *U, vectorD *S, matrixD *VT)
{
	int rows=A->rows;
	int cols=A->cols;
	int rowcolmin=rows;
	if (rowcolmin>cols)
	{
		rowcolmin=cols;
	}
	matrixD *Atemp=malloc (sizeof (matrixD));
	matrixInitFromMatrix (Atemp,A);
	copyMatrix (A, Atemp);
	char JOBZ='A';
	int LDA=rows;
	int LDU=rows;
	int LDVT=cols;
	int LWORK=-1;
	double wkopt;
	int *IWORK=malloc (8*rowcolmin*sizeof (int));
	int INFO;
	dgesdd_(&JOBZ, &rows, &cols, Atemp->mat, &LDA, S->vec, U->mat, &LDU, VT->mat, &LDVT, &wkopt, &LWORK, IWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgesdd_(&JOBZ, &rows, &cols, Atemp->mat, &LDA, S->vec, U->mat, &LDU, VT->mat, &LDVT, WORK, &LWORK, IWORK, &INFO);
	if (INFO !=0)
	{
		printf("Unsuccessful SVD decomposition\n");
	}
	freeMatrixD (Atemp);
	free (WORK);
	free (IWORK);
}

void matrixSPDInverse (char UPLO, matrixD *inputMat, matrixD *outputMat)
{
	int incx=1;
	int incy=1;
	int dimension=(inputMat->rows)*(inputMat->cols);
	dcopy_(&dimension, inputMat->mat, &incx, outputMat->mat, &incy);
	int INFO;
	int rows=inputMat->rows;                                //since rows is of int format hence casting it to int
	dpotri_(&UPLO, &rows, outputMat->mat, &rows, &INFO);
	if (INFO !=0 )
	{
		printf ("ERROR: matrixSPDInverse: Some issue with input matrix. dpotri info value = %d \n",INFO);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
}

void matrixSPDInverseOrig (char UPLO, matrixD *inputMat)
{
	int INFO;
	int rows=inputMat->rows;                                //since rows is of int format hence casting it to int
	dpotri_(&UPLO, &rows, inputMat->mat, &rows, &INFO);
	if (INFO !=0 )
	{
		printf ("ERROR: matrixSPDInverseOrig: Some issue with input matrix. dpotri info value = %d \n",INFO);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
}

void houseHolderRotation (matrixD *inputMat, vectorD *inputVec, char position, double tau)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	int INCV=1;
	int LDC=rows;
	double *WORK;
	if (position=='L')
	{
		WORK=malloc (sizeof (double)*cols);
	}
	else
	{
		WORK=malloc (sizeof (double)*rows);
	}
	dlarf_(&position, &rows, &cols, inputVec->vec, &INCV, &tau, inputMat->mat, &LDC, WORK);
	free (WORK);
}

void eigenValueCompute (matrixD *inputMat, vectorD *eigenValue)
{
	char JOBZ='N';
	char RANGE='A';
	char UPLO='L';
	int rows=inputMat->rows;
	matrixD *A=malloc (sizeof(matrixD));
	matrixInitFromMatrix (A,inputMat);
	copyMatrix (inputMat, A);
	int LDA=inputMat->rows;
	double VL, VU;
	int IL, IU;
	double ABSTOL=-1.0;
	int M;
	int LDZ=rows;
	double *Z=malloc (sizeof(double)*rows*LDZ);
	int *ISUPPZ=malloc (sizeof(int)*rows);
	int LWORK=-1;
	int LIWORK=-1;
	double wkopt;
	int iwkopt;
	int INFO;
	dsyevr_(&JOBZ, &RANGE, &UPLO, &rows, A->mat, &LDA, &VL, &VU, &IL, &IU, &ABSTOL, &M, eigenValue->vec, Z, &LDZ, ISUPPZ, &wkopt, &LWORK, &iwkopt, &LIWORK, &INFO);
	LWORK=(int)wkopt;
	double *WORK=malloc (sizeof(double)*LWORK);
	LIWORK=iwkopt;
	int *IWORK=malloc (sizeof(int)*LIWORK);
	dsyevr_(&JOBZ, &RANGE, &UPLO, &rows, A->mat, &LDA, &VL, &VU, &IL, &IU, &ABSTOL, &M, eigenValue->vec, Z, &LDZ, ISUPPZ, WORK, &LWORK, IWORK, &LIWORK, &INFO);
	if (INFO > 0)
       	{
		printf( "ERROR: eigenValueCompute: dsyevr info = %d\n", INFO);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	freeMatrixD (A);
	free(Z);
	free(ISUPPZ);
	free(WORK);
	free(IWORK);
}

int rankMatrix (matrixD *A)
{
	int rows=A->rows;
	int cols=A->cols;
	int rowcolmin=rows;
	if (rowcolmin>cols)
	{
		rowcolmin=cols;
	}
	
	double *Atemp = malloc (sizeof(double)*A->size);
	memcpy (Atemp, A->mat, sizeof(double)*A->size);
	double *S = malloc (sizeof(double)*rowcolmin);
	double *U = malloc (sizeof(double)*rows*rows);
	double *VT = malloc (sizeof(double)*cols*cols);

	char JOBZ='A';
	int LDA=rows;
	int LDU=rows;
	int LDVT=cols;
	int LWORK=-1;
	double wkopt;
	int *IWORK=malloc (8*rowcolmin*sizeof (int));
	int INFO;
	dgesdd_(&JOBZ, &rows, &cols, Atemp, &LDA, S, U, &LDU, VT, &LDVT, &wkopt, &LWORK, IWORK, &INFO);
	LWORK= (int) (wkopt);
	double *WORK=malloc (sizeof (double)*LWORK);
	dgesdd_(&JOBZ, &rows, &cols, Atemp, &LDA, S, U, &LDU, VT, &LDVT, WORK, &LWORK, IWORK, &INFO);
	if (INFO !=0)
	{
		printf("Unsuccessful SVD decomposition\n");
	}
	free (WORK);
	free (IWORK);
	int rank = 0;
	double rcond = 1E-8;
	int i;
	for (i=0; i<rowcolmin; i++){
		if (fabs (S[i]) > rcond){
			rank++;
		}
	}
	free (Atemp);
	free (S);
	free (U);
	free (VT);
	return rank;
}

void rotateMatrix (vectorD *u, vectorD *v, matrixD *A)
{
	vectorD *diff = malloc (sizeof(vectorD));
	vectorInit (diff, u->dimension);
	vectorAdd (1.0, u, -1.0, v, diff);
	double norm = vectorNorm2 (diff);
	double tau = 2.0/(norm*norm);
	houseHolderRotation (A, diff, 'L',tau);
	freeVectorD (diff);
}

void uniformAngles (matrixD *A)
{
	if (A->cols != A->rows+1){
		printf ("ERROR: uniformAngles: Matrix should have n rows and n+1 cols\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i, j;
	for (i=0; i<A->size; i++){
		A->mat[i] = 0;
	}
	int dimension = A->rows;
	matrixD *VVT = malloc (sizeof(matrixD));
	matrixInitWithValue (VVT, dimension, dimension, -1.0/dimension);
	for (i=0; i<VVT->cols; i++) {
		VVT->mat[(VVT->rows+1)*i] = 1.0;
	}
	matrixCholeskyDecOrig ('U', VVT);
	//copy upper triangle VT (from VVT) matrix into A
	for (i=0; i<VVT->cols; i++){
		memcpy (A->mat+i*VVT->rows, VVT->mat+i*VVT->rows, sizeof(double)*(i+1));
	}
	freeMatrixD (VVT);
	//find sum of first "n=dimension" cols of A and store it in last col of A
	double sum = 0;
	for (i=0; i<A->rows; i++){	
		sum = 0;
		for (j=0; j<A->cols-1; j++){
			sum += A->mat[i+j*A->rows];
		}
		A->mat[i+(A->cols-1)*A->rows] = -sum;
	}
	sum = 0;
	double val = 0;
	for (i=0; i<A->rows; i++){	
		val = A->mat[(A->cols-1)*A->rows+i];
		sum += val * val;
	}
	sum = sqrt(sum);
	for (i=0; i<A->rows; i++){	
		A->mat[(A->cols-1)*A->rows+i] /= sum;
	}
}
