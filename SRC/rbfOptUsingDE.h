/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _RBFOPTDE_H
#define _RBFOPTDE_H
#include "generatePts.h"
#include "solution.h"
#include "differentialEvolution.h"
#include "knn.h"

typedef struct{
	char *rbfType;
	double *rbfParam;
	double delta;
	double deltaSqr;
	char rbfTypeObj; 
	bool feasibilitySearch;
	bool maxDistFromFeasible;
	bool computeObj;
	bool autoSelectModel;
	bool includePolyTail;
	bool singleRBFtype;
	bool customAprioriConstr;
	bool useKNN;
	int knnParam;
	int dimension;
	int totalPts;
	int numConstraints;
	int numAprioriConstraints;
	int numConstraintsQRAK;
	int numConstraintsQRSK;
	int numConstraintsQUSK;
	int numRBFConstraints;
	int numMultiStartPts;
	int numUnrelaxablePts;
	int numHiddenConstrPts;
	int numNQInfeasiblePts;
	int numFeasiblePts;
	int numInfeasiblePts;
	double rbfParamObj;
	double constrPrecision;
	char *pointType;
	char aprioriInputPtFile[1024];
	char aprioriExec[1024];
	char aprioriOutputPtFile[1024];
	int *aprioriConstrType;
	vectorD *lb;
	vectorD *ub;
	vectorD *nqPt;
	vectorD *distVec;
	vectorD *rbfVector;
	vectorD *objectiveCoeff;
	matrixD *rbfMatrix;
	matrixD *allPtsMatrix;
	matrixD *ptsMatrixNonHidden;
	matrixD *coefficients;
	matrixD *negativeidentityAppendedOnes;
	matrixD *identityAppendedNegativeOnes;
} userData;

void constrCorrection (int numConstraints, double *constr, double precision);
bool userDataInit (inputData *inputParam, solutionStruct *solution, userData *ud, bool feasibilitySearch, bool maxDistFromFeasible, bool computeObj, double delta, vectorD *nqPt, char *fullRank);
void fPtrFeasiblePtOpt (double *x, double *obj, double *constr, int n, int m, void *udptr);
void fPtrMaxDistFeasibleOpt (double *x, double *obj, double *constr, int n, int m, void *udptr);
void fPtrGlobalOpt (double *x, double *obj, double *constr, int n, int m, void *udptr);
void fPtrMultiStartOpt (double *x, double *obj, double *constr, int n, int m, void *udptr);
void fPtrPtProject (double *x, double *obj, double *constr, int n, int m, void *ud);
void fPtrFeasTest (double *x, double *constr, int n, int m, void *ud);
void findFeasiblePtSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput);
void maxDistFeasibleSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput);
void globalPtSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput);
void localOptSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput);
void multiStartSurr (inputData *inputParam, solutionStruct *solution, matrixD *outputMat, vectorD *objValues, char *uniqueStatus, int *numPts, double delta, vectorD *lbInput, vectorD *ubInput);
bool randomPtInRadius (inputData *inputParam, solutionStruct *solution, vectorD *randomPt, double radius, bool checkFeasibility);
bool randomMatrixInRadius (inputData *inputParam, solutionStruct *solution, vectorD *randomPt, matrixD *randomMat, int numRandomPts, double radius, char *fullRank);
bool ptProjectOnFeasible (inputData *inputParam, solutionStruct *solution, vectorD *x);
double knnDecision (double *x, matrixD *ptsMat, vectorD *distVec, char *pointType, int numFeasiblePts, int numInfeasiblePts, int numNQInfeasiblePts, int numUnrelaxablePts, int numHiddenPts, int knnParam);
void pickBestFeasiblePts (matrixD *inputMat, inputData *inputParam, solutionStruct *solution);
void freeUserData (userData *ud);
#endif
