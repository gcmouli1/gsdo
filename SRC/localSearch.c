/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "localSearch.h"

bool projectToBoundary (inputData *inputParam, vectorD *pt, double delta)
{
	int i;
	bool boundaryHit = false;
	for (i=0; i<inputParam->dimension; i++){
		if (pt->vec[i] < inputParam->lb->vec[i] + delta){
			pt->vec[i] = inputParam->lb->vec[i];
			boundaryHit = true;
		}
	}
	for (i=0; i<inputParam->dimension; i++){
		if (pt->vec[i] > inputParam->ub->vec[i] - delta){
			pt->vec[i] = inputParam->ub->vec[i];
			boundaryHit = true;
		}
	}
	return boundaryHit;
}

bool truncateBounds (inputData *inputParam, solutionStruct *solution, vectorD *excludePt)
{
	int i; 
	double epsilon = 1E-5;
	int minDistIndex = -1;
	int dimension = inputParam->dimension;
	double minDist = DBL_MAX;
	vectorD *lbDist = malloc (sizeof(vectorD));
	vectorInit (lbDist, dimension);
	vectorD *ubDist = malloc (sizeof(vectorD));
	vectorInit (ubDist, dimension);
	bool lbSide = false;

	int count = 0;
	for (i=0; i<dimension; i++){
		lbDist->vec[i] = excludePt->vec[i] - solution->lbScaled->vec[i]; 
		ubDist->vec[i] = solution->ubScaled->vec[i] - excludePt->vec[i];
		if (lbDist->vec[i] >= 0 && ubDist->vec[i] >= 0){
			count++;
		}
	}
	if (count != dimension){
		freeVectorD (lbDist);
		freeVectorD (ubDist);
		return false;
	}

	for (i=0; i<dimension; i++){
		if (lbDist->vec[i] < ubDist->vec[i] && lbDist->vec[i] < minDist){
			minDist = lbDist->vec[i];
			minDistIndex = i;
			lbSide = true;
		}
		if (lbDist->vec[i] >= ubDist->vec[i] && ubDist->vec[i] < minDist){
			minDist = ubDist->vec[i];
			minDistIndex = i;
			lbSide = false;
		}
	}
	freeVectorD (lbDist);
	freeVectorD (ubDist);

	if (lbSide == true){
		solution->lbScaled->vec[minDistIndex] = excludePt->vec[minDistIndex];
		if (solution->lbScaled->vec[minDistIndex] < inputParam->lb->vec[minDistIndex]){
			solution->lbScaled->vec[minDistIndex] = inputParam->lb->vec[minDistIndex];
		}

	}
	else{
		solution->ubScaled->vec[minDistIndex] = excludePt->vec[minDistIndex];
		if (solution->ubScaled->vec[minDistIndex] > inputParam->ub->vec[minDistIndex]){
			solution->ubScaled->vec[minDistIndex] = inputParam->ub->vec[minDistIndex];
		}
	}
	if (solution->ubScaled->vec[minDistIndex] - solution->lbScaled->vec[minDistIndex] < epsilon){
		return false;
	}
	return true;
}

int localRBFSearch (inputData *inputParam, solutionStruct *solution, vectorD *inputPt, int inputPtIndex, int maxIters, char searchType)
{
	int dimension = inputParam->dimension;
	double radius = inputParam->stepLength;
	vectorD *pt = malloc (sizeof(vectorD));
	vectorInit (pt, dimension);
	memcpy (pt->vec, inputPt->vec, sizeof(double)*dimension);
	createNewBounds (inputParam, solution, radius, pt);
	double delta = 1E-3;
	bool truncateStatus = true;
	int iters = 0;
	char ptType = 'N';

	while (iters < maxIters && solution->totalPts < inputParam->maxAllowedEvals){
		switch (searchType) {
			case 'L':
				localOptSurr (inputParam, solution, solution->lbScaled, solution->ubScaled);
			case 'M':
				maxDistFeasibleSurr (inputParam, solution, solution->lbScaled, solution->ubScaled);
				break;
			case 'G':
				globalPtSurr (inputParam, solution, solution->lbScaled, solution->ubScaled);
			default:
				findFeasiblePtSurr (inputParam, solution, solution->lbScaled, solution->ubScaled);
				break;
		}

		if (solution->optimizationSuccessFul == true){
			updateSolution (inputParam, solution, solution->optimSolutionPt);
		}
		else {
			freeVectorD (pt);
			return false;
		}
		ptType = solution->pointType[solution->inputPtIndex];
		if (ptType == 'F'){
			break;
		}
		if ((ptType == 'I') && ((searchType == 'L' || searchType == 'M' || searchType == 'G') || (inputPtIndex != solution->leastInfPointIndex))){
			break;
		}
		memcpy (pt->vec, solution->optimSolutionPt->vec, sizeof(double)*dimension);
		bool boundaryHit = projectToBoundary (inputParam, pt, delta);
		if (boundaryHit == true){
			break;
		}
		if (ptType == 'S' || ptType == 'H' || ptType == 'U'){
			truncateStatus = truncateBounds (inputParam, solution, pt);
			if (truncateStatus == false){
				break;
			}
		}
		iters++;
	}
	freeVectorD (pt);
	if (truncateStatus == false){
		return -1;
	}
	if (inputPtIndex != solution->leastInfPointIndex || ptType == 'F'){
		return 1;
	}
	else {
		return 0;
	}
}
