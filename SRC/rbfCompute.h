/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#ifndef _RBFCOMPUTE_H
#define _RBFCOMPUTE_H
#include "latinHypercube.h"

typedef struct {
	matrixD *inputPtMatrixTrans;
	vectorD *vecOnes;
	matrixD *pointRepeatMatrix;
	matrixD *coeffMatrixDiag;
	vectorD *lambdaCoeff;
	matrixD *diffTimesCoeff;
	vectorD *coefficients;
	char *rbfType;
	double *rbfParam;
	int numRBFTypes;
}rbfStruct;

void phiMatrixCompute (matrixD *inputPtMatrix, matrixD *phiMatrix, char rbfType, double rbfParam);

//Functions for computing coefficients.
//bool rbfComputeCoefficients (matrixD *inputPtMatrix, matrixD *inputPtMatrixTrans, vectorD *funcValues, vectorD *coefficients, char rbfType, double rbfParam, bool includePolyTail);
bool rbfComputeCoefficients (matrixD *samplePtMatrix, matrixD *samplePtMatrixTrans, matrixD *phiMatrix, vectorD *funcValues, vectorD *coefficients, char rbfType, double rbfParam, bool includePolyTail);

//Functions for computing rbf value at inputPt.
void rbfValueMatrixSingleType (double *inputPt, matrixD *samplePtMatrix, matrixD *coefficients, double *rbfValues, char rbfType, double rbfParam, vectorD *rbfVector);
double rbfValueSingle (double *inputPt, matrixD *samplePtMatrix, vectorD *coefficients, char rbfType, double rbfParam, vectorD *rbfVector);
#endif
