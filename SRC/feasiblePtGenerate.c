/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "feasiblePtGenerate.h"

void feasiblePtGenerate (inputData *inputParam, solutionStruct *solution)
{
	int i,j;
	int dimension = inputParam->dimension;
	int numConstraints = inputParam->numConstraints;
	vectorD *tmpVector = malloc (sizeof(vectorD));
	vectorInit (tmpVector, inputParam->dimension);

	int *numPtsContr = calloc (numConstraints, sizeof(int));
	char *sufficientRank = malloc (sizeof(char)*numConstraints);
	for (i=0; i<numConstraints; i++){ //rank for RBF. Needed only for QRSK and QUSK.
		if (inputParam->constraintType->vec[i] == inputParam->indexQRSK || inputParam->constraintType->vec[i] == inputParam->indexQUSK){
			sufficientRank[i] = 'F';
		}
		else {
			sufficientRank[i] = 'T';
		}
	}
	int repeatLHSearch = true;
	int count = 0;
	bool computeAll = true;
	bool feasPtExists = false;
	int feasIndex = -1;
	vectorD *lb = malloc (sizeof(vectorD));
	vectorInit (lb, dimension);
	memcpy (lb->vec, inputParam->lb->vec, sizeof(double)*dimension);
	vectorD *ub = malloc (sizeof(vectorD));
	vectorInit (ub, dimension);
	memcpy (ub->vec, inputParam->ub->vec, sizeof(double)*dimension);

	while (solution->totalPts <= inputParam->maxAllowedEvals && repeatLHSearch == true){
		for (i=solution->totalPts-1; i>=0; i--){
			if (solution->pointType[i] == 'F'){
				feasPtExists = true;
				feasIndex = i;
				break;
			}
		}
		if (feasPtExists == true){
			vecExtractMat (tmpVector, solution->allPtsMatrix, feasIndex);
		}
		//create multiple feasible pts around current feasible pt till rbf rank is satisfied
		if (feasPtExists == true && solution->numHiddenConstrPts > 0){
			double deltaFeas = inputParam->stepLength;
			for (i=0; i<dimension; i++){
				lb->vec[i] = solution->allPtsMatrix->mat[i+dimension*feasIndex] - deltaFeas;
				if (lb->vec[i] < inputParam->lb->vec[i]){
					lb->vec[i] = inputParam->lb->vec[i];
				}
			}
			for (i=0; i<dimension; i++){
				ub->vec[i] = solution->allPtsMatrix->mat[i+dimension*feasIndex] + deltaFeas;
				if (ub->vec[i] > inputParam->ub->vec[i]){
					ub->vec[i] = inputParam->ub->vec[i];
				}
			}
		}

		if (count == 0 || count == inputParam->numStartTestPts){
			if (solution->totalPts < inputParam->numStartTestPts){
				generatePts (inputParam, inputParam->numStartTestPts, lb, ub, solution->lhMatrixPts);
			}
			else{
				double radius = -1;
				if (feasPtExists == true){
					radius = inputParam->stepLength;
				}
				bool randomStatus = randomMatrixInRadius (inputParam, solution, tmpVector, solution->lhMatrixPts, inputParam->numStartTestPts, radius, sufficientRank);
				if (randomStatus == false){
					generatePts (inputParam, inputParam->numStartTestPts, lb, ub, solution->lhMatrixPts);
				}
			}
			count = 0;
		}
		if (solution->stepType != NULL){
			free (solution->stepType);
		}
		solution->stepType = strdup("Initial_Feasible(LH)");
		if (computeAll == true){
			for (i=0; i<inputParam->numStartTestPts; i++){
				vecExtractMat (tmpVector, solution->lhMatrixPts, i);
				if (solution->totalPts < inputParam->maxAllowedEvals){
					updateSolution (inputParam, solution, tmpVector);
				}
			}
		}
		else {
			vecExtractMat (tmpVector, solution->lhMatrixPts, count);
			if (solution->totalPts < inputParam->maxAllowedEvals){
				updateSolution (inputParam, solution, tmpVector);
			}
			count++;
		}
		computeAll = false;

		if (solution->totalPts == inputParam->maxAllowedEvals){
			break;
		}

		//count no. of pts available for each constr. Needed for rank of RBF.
		for (i=0; i<numConstraints; i++){
			numPtsContr[i] = 0;
		}

		int numRBFPts = 0; //total rbf pts for all constraints. Same rbf pt will be repeated for 2 constr.
		for (i=0; i<solution->totalPts; i++){
			char pt = solution->pointType[i];
			for (j=0; j<numConstraints; j++){
				double constrValue = solution->allConstraintValues->mat[j+i*numConstraints];
				if ((pt == 'F' || pt == 'I' || pt == 'S') && inputParam->constraintType->vec[j] == inputParam->indexQRSK){
					numPtsContr[j]++;
					numRBFPts++;
				}
				else if (pt != 'H' && inputParam->constraintType->vec[j] == inputParam->indexQUSK && constrValue > -1e30){
					numPtsContr[j]++;
					numRBFPts++;
				}
			}
		}
		repeatLHSearch = false;
		for (j=0; j<inputParam->numConstraints; j++){
			if (((inputParam->constraintType->vec[j] == inputParam->indexQRSK || inputParam->constraintType->vec[j] == inputParam->indexQUSK) && (numPtsContr[j] < dimension))){
				repeatLHSearch = true;
			}
		}
		if (repeatLHSearch == false){
			bool rankStatus = rankOfRBFPts (inputParam, solution, numPtsContr, sufficientRank);
			if (rankStatus == false){
				repeatLHSearch = true;
			}
		}
	}
	free (numPtsContr);
	free (sufficientRank);
	freeVectorD (lb);
	freeVectorD (ub);

	if (solution->numFeasiblePts > 0){
		freeVectorD (tmpVector);
		return;
	}
	if (solution->feasiblePtFound == true){
		freeVectorD (tmpVector);
		return;
	}

	matrixD *restartPtMat = malloc (sizeof(matrixD));
	matrixInit (restartPtMat, inputParam->dimension, inputParam->numRestartPts);
	solution->optimizationSuccessFul = false;
	bool multiFail = false;
	int numIters = 0;
	//double delta = inputParam->stepLength;
	//double psRadius = 5*inputParam->stepLength;

	while (solution->totalPts < inputParam->maxAllowedEvals){
		if (solution->stepType != NULL){
			free (solution->stepType);
		}
		solution->stepType = strdup("Initial_Feasible(RBF)");
		findFeasiblePtSurr (inputParam, solution, NULL, NULL);

		if (solution->optimizationSuccessFul == true){
			updateSolution (inputParam, solution, solution->optimSolutionPt);
			if (solution->feasiblePtFound == true){
				freeMatrixD (restartPtMat);
				freeVectorD (tmpVector);
				return;
			}
			if (solution->repeatPoint == true){ //if RBF fails, try a random pt within unit radius.
				if (solution->stepType != NULL){
					free (solution->stepType);
				}
				solution->stepType = strdup("Initial_Feasible(Rand)");
				double radius = inputParam->stepLength;
				memcpy (tmpVector->vec, solution->optimSolutionPt->vec, sizeof(double)*dimension);
				bool checkFeasibility = false;
				randomPtInRadius (inputParam, solution, tmpVector, radius, checkFeasibility);
				updateSolution (inputParam, solution, tmpVector);
				if (solution->feasiblePtFound == true){
					freeMatrixD (restartPtMat);
					freeVectorD (tmpVector);
					return;
				}
			}
			numIters++;
		}
		else {
			printf ("RBF based feasible opt failed\n");
		}
		int maxIters = 1 * (dimension + 1);
		localRBFSearch (inputParam, solution, solution->leastInfPoint, solution->leastInfPointIndex, maxIters, 'F');
		if (solution->feasiblePtFound == true){
			freeMatrixD (restartPtMat);
			freeVectorD (tmpVector);
			return;
		}

		coordinateStagnate (solution, inputParam);//check if some coordinate is stagnated.
		if (numIters >= 3*(dimension+1)){
			multiFail = true;
			numIters = 0;
		}
		else {
			multiFail = false;
		}
		if (solution->optimizationSuccessFul == false || solution->repeatPoint == true || solution->stagnation == true || multiFail == true){
			solution->repeatPoint = true;
			generateNewPts (inputParam, solution, inputParam->numRestartPts, inputParam->lb, inputParam->ub, restartPtMat);
			i=0;
			if (solution->stepType != NULL){
				free (solution->stepType);
			}
			solution->stepType = strdup("Initial_Feasible(LH)");
			while (solution->totalPts < inputParam->maxAllowedEvals && i<inputParam->numRestartPts){
				vecExtractMat (tmpVector, restartPtMat, i);
				updateSolution (inputParam, solution, tmpVector);
				if (solution->feasiblePtFound == true){
					freeMatrixD (restartPtMat);
					freeVectorD (tmpVector);
					return;
				}
				i++;
			}
			if (i == inputParam->numRestartPts && solution->repeatPoint == true){
				printf ("ERROR: feasiblePtGenerate: All points repeated in LH\n");
				printf ("PROCESS TERMINATED\n");
				exit (EXIT_FAILURE);
			}
		}
	}
	if (solution->totalPts >= inputParam->maxAllowedEvals && inputParam->debugLogLevel > -1){
		printf ("WARNING: feasiblePtGenerate: Budget exhausted. Stopping the algorithm\n");
	}
	freeMatrixD (restartPtMat);
	freeVectorD (tmpVector);
	return;
}

bool rankOfRBFPts (inputData *inputParam, solutionStruct *solution, int *numPtsForConstr, char *sufficientRank)
{
	int i,j,k;
	int dimension = inputParam->dimension;
	int numConstraints = inputParam->numConstraints;
	bool status = true;
	int rank = 0;
	for (i=0; i<numConstraints; i++){
		if (sufficientRank[i] == 'F' && inputParam->constraintType->vec[i] == inputParam->indexQRSK){
			if (numPtsForConstr[i] == 0){
				return false;
			}
			k = 0;
			matrixD *ptsMatrix = malloc (sizeof(matrixD));
			matrixInit (ptsMatrix, dimension+1, numPtsForConstr[i]);
			for (j=0; j<solution->totalPts; j++){
				if (solution->pointType[j] == 'F' || solution->pointType[j] == 'I' || solution->pointType[j] == 'S'){
					memcpy (ptsMatrix->mat+k*ptsMatrix->rows, solution->allPtsMatrix->mat + j*dimension, sizeof(double)*dimension);
					k++;
				}
			}
			for (j=1; j<ptsMatrix->cols+1; j++){
				ptsMatrix->mat[j*ptsMatrix->rows-1] = 1.0;
			}
			matrixD *ptsMatrixTrans = malloc (sizeof(matrixD));
			matrixInit (ptsMatrixTrans, ptsMatrix->cols, ptsMatrix->rows);
			matrixTranspose (ptsMatrix, ptsMatrixTrans);
			freeMatrixD (ptsMatrix);
			rank = rankMatrix (ptsMatrixTrans);
			freeMatrixD (ptsMatrixTrans);
			if (rank < dimension+1){
				status = false;
			}
			else {
				sufficientRank[i] = 'T';
				status = true;
			}
		}
		if (sufficientRank[i] == 'F' && (inputParam->constraintType->vec[i] == inputParam->indexQUSK)){
			if (numPtsForConstr[i] == 0){
				return false;
			}
			k = 0;
			matrixD *ptsMatrix = malloc (sizeof(matrixD));
			matrixInit (ptsMatrix, dimension+1, numPtsForConstr[i]);
			for (j=0; j<solution->totalPts; j++){
				if (solution->pointType[j] != 'H' && solution->allConstraintValues->mat[j*numConstraints + i] >= -1e30){
					memcpy (ptsMatrix->mat+k*ptsMatrix->rows, solution->allPtsMatrix->mat + j*dimension, sizeof(double)*dimension);
					k++;
				}
			}
			for (j=1; j<ptsMatrix->cols+1; j++){
				ptsMatrix->mat[j*ptsMatrix->rows-1] = 1.0;
			}
			matrixD *ptsMatrixTrans = malloc (sizeof(matrixD));
			matrixInit (ptsMatrixTrans, ptsMatrix->cols, ptsMatrix->rows);
			matrixTranspose (ptsMatrix, ptsMatrixTrans);
			freeMatrixD (ptsMatrix);
			rank = rankMatrix (ptsMatrixTrans);
			freeMatrixD (ptsMatrixTrans);
			if (rank < dimension+1){
				status = false;
			}
			else {
				sufficientRank[i] = 'T';
				status = true;
			}
		}
	}
	return status;
}

void generateNewPts (inputData *inputParam, solutionStruct *solution, int numPts, vectorD *lb, vectorD *ub, matrixD *ptsMat)
{
	int i, j, k, t = 0;
	int dimension = inputParam->dimension;
	int np = 100 * numPts;
	double radius = inputParam->stepLength;
	bool rejectPt = false;
	int numReject = 0;
	vectorD *tmpVector = malloc (sizeof(vectorD));
	vectorInit (tmpVector, dimension);

	while (t < numPts){
		matrixD *tmpMat = malloc (sizeof(matrixD));
		matrixInit (tmpMat, dimension, np);
		generatePts (inputParam, np, inputParam->lb, inputParam->ub, tmpMat);
		numReject = 0;
		i = 0;
		while (i<np && t < numPts){
			rejectPt = false;
			vecExtractMat (tmpVector, tmpMat, i);
			//find distance from all known pts and reject if distance is less than radius
			for (j=0; j<solution->totalPts; j++){
				double dist = 0;
				for (k=0; k<dimension; k++){
					dist += pow (tmpVector->vec[k]-solution->allPtsMatrix->mat[k+j*dimension], 2.0);
				}
				if (sqrt(dist) < radius){
					numReject++;
					rejectPt = true;
				}
			}
			if (rejectPt == false){
				memcpy (ptsMat->mat+t*dimension, tmpVector->vec, sizeof(double)*dimension);
				t++;
			}
			i++;
		}
		np = numReject;
		freeMatrixD (tmpMat);
	}
	freeVectorD (tmpVector);
}
