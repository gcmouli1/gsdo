/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "vecMatDeclare.h"
void vectorInitZ (vectorZ *inputVec, int numElem)
{
	inputVec->dimension = numElem;
	inputVec->vec = malloc (sizeof(int)*numElem);
}

void vectorInitZWithValue (vectorZ *inputVec, int numElem, int value)
{
	inputVec->dimension = numElem;
	inputVec->vec = malloc (sizeof(int)*numElem);
	int i;
	for (i=0; i<numElem; i++){
		inputVec->vec[i] = value;
	}
}

void vectorInit (vectorD *inputVec, int numElem)
{
	inputVec->dimension = numElem;
	inputVec->vec = malloc (sizeof(double)*numElem);
}

void vectorInitWithValue (vectorD *inputVec, int numElem, double value)
{
	inputVec->dimension = numElem;
	inputVec->vec = malloc (sizeof(double)*numElem);
	int i;
	for (i=0; i<numElem; i++){
		inputVec->vec[i] = value;
	}
}

void matrixInit (matrixD *inputMat, int rows, int cols)
{
	inputMat->rows = rows;
	inputMat->cols = cols;
	inputMat->mat = malloc (sizeof(double)*rows*cols);
	inputMat->size = rows * cols;
}

void matrixInitWithValue (matrixD *inputMat, int rows, int cols, double value)
{
	inputMat->rows = rows;
	inputMat->cols = cols;
	inputMat->size = rows * cols;
	inputMat->mat = malloc (sizeof(double)*rows*cols);
	int i;
	for (i=0; i<inputMat->size; i++){
		inputMat->mat[i] = value;
	}
}

void matrixInitFromMatrix (matrixD *inputMat, matrixD *A)
{
	inputMat->rows = A->rows;
	inputMat->cols = A->cols;
	inputMat->mat = malloc (sizeof(double)*(inputMat->rows)*(inputMat->cols));
	inputMat->size = A->size;
}

void setVectorWithValue (vectorD *v, double value)
{
	int i;
	for (i=0; i<v->dimension; i++){
		v->vec[i] = value;
	}
}

void sequenceVectorD (vectorD *inputVec, double startValue, double endValue, double interval)
{
	int numPts = (endValue - startValue)/interval;
	if (inputVec->dimension != numPts){
		printf ("ERROR: sequenceVectorZ: Size of sequence vector is not equal to number of sequence elements\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i;
	for (i=0; i<numPts; i++){
		inputVec->vec[i] = startValue + i * interval;
	}
}

void sequenceVectorZ (vectorZ *inputVec, int startValue, int endValue, int interval)
{
	int numPts = (endValue - startValue)/interval;
	if (inputVec->dimension != numPts){
		printf ("ERROR: sequenceVectorZ: Size of sequence vector is not equal to number of sequence elements\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i;
	for (i=0; i<numPts; i++){
		inputVec->vec[i] = startValue + i * interval;
	}
}

void vectorMax (vectorD *inputVec, double *maxVal, int *maxIndex)
{
	int index;
	*maxVal=-1*INFS;
	for(index=0; index<inputVec->dimension; index++) {
		if(*maxVal<inputVec->vec[index]) {
			*maxVal=inputVec->vec[index];
			*maxIndex=index;
		}
	}
}

void vectorMin (vectorD *inputVec, double *minVal, int *minIndex)
{
	int index;
	*minVal=INFS;
	for(index=0; index<inputVec->dimension; index++) {
		if(*minVal>inputVec->vec[index]) {
			*minVal=inputVec->vec[index];
			*minIndex=index;
		}
	}
}

void matrixMax (matrixD *inputMat, double *maxVal, int *maxIndex, int *rowNum, int *colNum)
{
	int index;
	*maxVal=-INFS;
	int numElem=(inputMat->rows)*(inputMat->cols);
	for(index=0; index<numElem; index++) {
		if(*maxVal<inputMat->mat[index]) {
			*maxVal=inputMat->mat[index];
			*maxIndex=index;
		}
	}
	*colNum=index/(inputMat->rows);
	*rowNum=index-(*colNum)*(inputMat->rows);
}

void matrixMin (matrixD *inputMat, double *minVal, int *minIndex, int *rowNum, int *colNum)
{
	int index;
	*minVal=INFS;
	int numElem=(inputMat->rows)*(inputMat->cols);
	for(index=0; index<numElem; index++) {
		if(*minVal>inputMat->mat[index]) {
			*minVal=inputMat->mat[index];
			*minIndex=index;
		}
	}
	*colNum=index/(inputMat->rows);
	*rowNum=index-(*colNum)*(inputMat->rows);
}

void matrixColumnMin (matrixD *inputMat, vectorD *columnMinVector)
{
	long i,j;
	for (i=0; i<inputMat->cols; i++){
		columnMinVector->vec[i] = DBL_MAX;
		for (j=0; j<inputMat->rows; j++){
			if (columnMinVector->vec[i] > inputMat->mat[j+i*(inputMat->rows)]){
				columnMinVector->vec[i] = inputMat->mat[j+i*(inputMat->rows)];
			}
		}
	}
}

void matrixColumnMax (matrixD *inputMat, vectorD *columnMaxVector)
{
	long i,j;
	for (i=0; i<inputMat->cols; i++){
		columnMaxVector->vec[i] = -DBL_MAX;
		for (j=0; j<inputMat->rows; j++){
			if (columnMaxVector->vec[i] < inputMat->mat[j+i*(inputMat->rows)]){
				columnMaxVector->vec[i] = inputMat->mat[j+i*(inputMat->rows)];
			}
		}
	}
}

void matrixRowMin (matrixD *inputMat, vectorD *rowMinVector)
{
	long i,j;
	for (i=0; i<inputMat->rows; i++){
		rowMinVector->vec[i] = DBL_MAX;
		for (j=0; j<inputMat->cols; j++){
			if (rowMinVector->vec[i] > inputMat->mat[j*(inputMat->rows) + i]){
				rowMinVector->vec[i] = inputMat->mat[j*(inputMat->rows) + i];
			}
		}
	}
}

void matrixRowMax (matrixD *inputMat, vectorD *rowMaxVector)
{
	long i,j;
	for (i=0; i<inputMat->rows; i++){
		rowMaxVector->vec[i] = -DBL_MAX;
		for (j=0; j<inputMat->cols; j++){
			if (rowMaxVector->vec[i] < inputMat->mat[j*(inputMat->rows) + i]){
				rowMaxVector->vec[i] = inputMat->mat[j*(inputMat->rows) + i];
			}
		}
	}
}

void printVectorZ (vectorZ *vectorA)
{
	int i;
	for (i=0; i<vectorA->dimension; i++) {
		printf ("%d\n", vectorA->vec[i]);
	}
}

void printVector (vectorD *vectorA)
{
	int i;
	for (i=0; i<vectorA->dimension; i++) {
		printf ("%12g\n", vectorA->vec[i]);
	}
}

void printVectorL (vectorD *vectorA, int numElem)
{
	int i;
	if (numElem > vectorA->dimension) {
		printf ("given size is beyond the vector dimension\n");
	}
	else {
		for (i=0; i<numElem; i++) {
			printf ("%G\n", vectorA->vec[i]);
		}
	}
}

void printMatrix (matrixD *matrixA)
{
	int i, j;
	int rows=matrixA->rows;
	int cols=matrixA->cols;
	for (i=0; i<rows; i++) {
		for (j=0; j<cols; j++) {
			printf ("%12g  ", matrixA->mat[i+j*rows]);
		}
		printf ("\n");
	}
}

void printMatrixTranspose (matrixD *matrixA)
{
	int i, j;
	int rows=matrixA->rows;
	int cols=matrixA->cols;
	for (i=0; i<cols; i++) {
		for (j=0; j<rows; j++) {
			printf ("%12g  ", matrixA->mat[i*rows+j]);
		}
		printf ("\n");
	}
}

void printSubVectorD (vectorD *inputVec, int start, int end)
{
	int i;
	for (i=start; i<end; i++){
		printf ("%G\n",inputVec->vec[i]);
	}
}

void printSubVectorZ (vectorZ *inputVec, int start, int end)
{
	int i;
	for (i=start; i<end; i++){
		printf ("%d\n",inputVec->vec[i]);
	}
}

void printColSubMatrix (matrixD *matrixA, int colStartIndex, int colEndIndex)
{
	int i, j;
	int rows = matrixA->rows;
	for (i=0; i<rows; i++) {
		for (j=colStartIndex; j<colEndIndex; j++) {
			printf ("%12g  ", matrixA->mat[i+j*rows]);
		}
		printf ("\n");
	}
}

void printRowSubMatrix (matrixD *matrixA, int rowStartIndex, int rowEndIndex)
{
	int i, j;
	int cols = matrixA->cols;
	int rowsActual = matrixA->rows;
	for (i=rowStartIndex; i<rowEndIndex; i++) {
		for (j=0; j<cols; j++) {
			printf ("%12g  ", matrixA->mat[i+j*rowsActual]);
		}
		printf ("\n");
	}
}

void printSubMatrixUsingIndices (matrixD *matrixA, int *indices, int numElem)
{
	int i, j;
	int rows=matrixA->rows;
	matrixD *outputMat = malloc (sizeof(matrixD));
	matrixInit (outputMat, rows, numElem);
	matrixExtractUsingIndices (matrixA, indices, numElem, outputMat);
	for (i=0; i<rows; i++) {
		for (j=0; j<numElem; j++) {
			printf ("%12g  ", outputMat->mat[i+j*rows]);
		}
		printf ("\n");
	}
	freeMatrixD (outputMat);
}

void printSubMatrix (matrixD *matrixA, int rows, int cols)
{
	int i, j;
	if (rows > matrixA->rows || cols > matrixA->cols ) {
		printf ("given dimensions are beyond the dimension of matrix\n");
	}
	else {
		int rowsActual = matrixA->rows;
		for (i=0; i<rows; i++) {
			for (j=0; j<cols; j++) {
				printf ("%G ", matrixA->mat[i+j*rowsActual]);
			}
			printf ("\n");
		}
	}
}

void printSubMatrixTranspose (matrixD *matrixA, int rows, int cols)
{
	int i, j;
	int rowsActual = matrixA->rows;
	for (i=0; i<cols; i++) {
		for (j=0; j<rows; j++) {
			printf ("%12g  ", matrixA->mat[i*rowsActual+j]);
		}
		printf ("\n");
	}
}

void vecOnesZ (vectorZ *vector, int dimension)
{
	vector->dimension=dimension;
	vector->vec=malloc (sizeof (int)*dimension);
	int i;
	for (i=0; i<vector->dimension; i++) {
		vector->vec[i]=1;
	}
}

void vecOnesD (vectorD *vector, int dimension)
{
	vector->dimension=dimension;
	vector->vec=malloc (sizeof (double)*dimension);
	int i;
	for (i=0; i<vector->dimension; i++) {
		vector->vec[i]=1;
	}
}

void vecZerosZ (vectorZ *vector, int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (int));
}

void vecZerosD (vectorD *vector, int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (double));
}

void vecEyeD (vectorD *vector, int elemNum, int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (double));
	vector->vec[elemNum]=1.0;
}

void matEyeD (matrixD *matrix, int rows, int cols)
{
	matrix->rows=rows;
	matrix->cols=cols;
	matrix->mat=calloc (rows*cols, sizeof (double));
	int i;
	int iterLimit = cols;
	if (cols > rows){
		iterLimit = rows;
	}
	for (i=0; i<iterLimit; i++) {
		matrix->mat[(rows+1)*i]=1.0;
	}
}

void matOnesD (matrixD *matrix, int rows, int cols)
{
	matrix->rows = rows;
	matrix->cols = cols;
	matrix->mat = malloc (sizeof (double)*(matrix->rows*matrix->cols));
	int i=0;
	for (i=0; i<rows*cols; i++) {
		matrix->mat[i] = 1.0;
	}
}

void diagMatrix (vectorD *inputVec, matrixD *outputMat)
{
	int dimension = inputVec->dimension;
	int i=0;
	for (i=0; i<dimension; i++) {
		outputMat->mat[(outputMat->rows+1)*i] = inputVec->vec[i];
	}
}

void diagMatrixInverse (vectorD *inputVec, matrixD *outputMat)
{
	int dimension=inputVec->dimension;
	int i=0;
	for (i=0; i<dimension; i++) {
		outputMat->mat[(outputMat->rows+1)*i]=1.0/inputVec->vec[i];
	}
}

void matZerosD (matrixD *matrix, int rows, int cols)
{
	matrix->rows = rows;
	matrix->cols = cols;
	matrix->size = rows * cols;
	matrix->mat=calloc ( (matrix->rows)*(matrix->cols), sizeof (double));
}

void vecRandom (vectorD *inputPt, long l, long u, int randomSeed)
{
	srand (randomSeed);
	int i=0;
	for (i=0; i < inputPt->dimension; i++){
		inputPt->vec[i] = ((double)rand()/(double)RAND_MAX)*(u-l) + l;
	}
}

void vecRandomFromBounds (vectorD *inputPt, vectorD *lb, vectorD *ub, int randomSeed)
{
	srand (randomSeed);
	int i=0;
	for (i=0; i < inputPt->dimension; i++){
		inputPt->vec[i] = ((double)rand()/(double)RAND_MAX)*(ub->vec[i]-lb->vec[i]) + lb->vec[i];
	}
}

void matRandom (matrixD *matrix, long l, long u, int randomSeed)
{
	srand (randomSeed);
	int i=0;
	for (i=0; i < (matrix->rows)*(matrix->cols); i++){
		matrix->mat[i] = ((double)rand()/(double)RAND_MAX)*(u-l) + l;
	}
}

double elemExtractVecD ( vectorD *vector,  int elemNum)
{
	if (elemNum > vector->dimension){
		printf ("ERROR: elemExtractVecD: Trying to obtain value beyond vector range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	return vector->vec[elemNum];
}

double elemExtractMatD (matrixD *matrix, int rowNum, int colNum)
{
	if (rowNum > matrix->rows || colNum > matrix->cols){
		printf ("ERROR: elemExtractMatD: Trying to obtain value beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	return matrix->mat[rowNum + colNum * matrix->rows];
}

void matrixTranspose (matrixD *inputMat, matrixD *outputMat)
{
	int i, j;
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	if (outputMat->cols != inputMat->rows || outputMat->rows != inputMat->cols){
		printf ("ERROR: Transpose matrix dimension doesn't conform with input matrix\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	for (i=0; i<rows; i++) {
		for (j=0; j<cols; j++) {
			outputMat->mat[j+i*cols]=inputMat->mat[i+j*rows];
		}
	}
}

void upperLowerFull (char destination, matrixD *inputMat)
{
	int rows=inputMat->rows;
	int i, j;
	if (destination=='U') { /*Copy from lower to upper */
		for (i=0; i<rows-1; i++) {
			for (j=i+1; j<rows; j++) {
				inputMat->mat[i+j*rows]=inputMat->mat[j+i*rows];
			}
		}
	}
	else if (destination=='L') {/*Copy from upper to lower */
		for (i=0; i<rows-1; i++) {
			for (j=i+1; j<rows; j++) {
				inputMat->mat[j+i*rows]=inputMat->mat[i+j*rows];
			}
		}
	}
	else {
		printf ("Wrong input for matrix type \n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

}
void vecExtractElemRange (vectorD *inputVector, int start, int end, vectorD *outputVector)
{
	memcpy (outputVector->vec, inputVector->vec+start, sizeof(double)*(end-start));
}

void vecExtractUsingIndices (vectorD *inputVector, int *indices, int numElem, vectorD *outputVector)
{
	int i, index;
	for (i=0; i<numElem; i++){
		index = indices[i];
		if (index < 0 || index >= inputVector->dimension){
			printf ("ERROR: vecExtractMat: Cannot extract outside vector range\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		outputVector->vec[i] = inputVector->vec[index];
	}
}

void vecExtractMat (vectorD *vector, matrixD *matrix, int colnum)
{
	if (colnum < 0 || colnum >= matrix->cols){
		printf ("ERROR: vecExtractMat: Index = %d is beyond range of matrix (%d, %d)\n",colnum, matrix->rows, matrix->cols);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int rows=matrix->rows;
	memcpy (vector->vec, matrix->mat+(rows*colnum), rows * sizeof(double));
}

void matrixExtractColumns (matrixD *inputMat, int colStartIndex, int colEndIndex, matrixD *outputMat)
{
	if (colStartIndex < 0 || colEndIndex < 0){
		printf ("ERROR: matrixExtractColumns: Cannot extract matrix for negative indices.\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (colStartIndex >= inputMat->cols || colEndIndex >= inputMat->cols){
		printf ("ERROR: matrixExtractColumns: Cannot extract vector %d beyond matrix range %d\n",colEndIndex, inputMat->cols-1);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (outputMat->rows != inputMat->rows){
		printf ("ERROR: matrixExtractColumns: Non-conforming matrices.\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	memcpy (outputMat->mat, inputMat->mat + (colStartIndex)* inputMat->rows, sizeof(double)* (colEndIndex+1-colStartIndex)*(inputMat->rows)); //+1 to colEndIndex to get actual no. of elements.
}

void matrixExtractUsingIndices (matrixD *inputMat, int *indices, int numElem, matrixD *outputMat)
{
	if (outputMat->rows != inputMat->rows){
		printf ("ERROR: matrixExtractUsingIndices: Non-conforming matrices.\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int rows=inputMat->rows;
	int i,colnum;
	for (i=0; i<numElem; i++){
		colnum = indices[i];
		if (colnum < 0 || colnum >= inputMat->cols){
			printf ("ERROR: matrixExtractUsingIndices: Cannot extract outside matrix range\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		memcpy (outputMat->mat + i*rows, inputMat->mat+(rows*colnum), rows * sizeof(double));
	}
}

void subMatrixExtract (matrixD *inputMat, int rows, int cols, matrixD *outputMat)
{
	if (rows > inputMat->rows || cols > inputMat->cols){
		printf ("ERROR: subMatrixExtract: Cannot extract beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (rows > outputMat->rows || cols > outputMat->cols){
		printf ("ERROR: subMatrixExtract: Cannot access beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int j;
	int rowsActual = inputMat->rows;
	for (j=0; j<cols; j++) {
		memcpy (outputMat->mat+j*rows, inputMat->mat+j*rowsActual, sizeof(double)*rows);
	}
}

void colSubmatrixExtract (matrixD *inputMat, int *colIndex, int numColElem, matrixD *outputMat)
{
	if (numColElem < 0 || numColElem >= inputMat->cols){ 
		printf ("ERROR: colSubmatrixExtract: Cannot extract beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (outputMat->rows != inputMat->rows){
		printf ("ERROR: colSubmatrixExtract: Cannot extract beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int rows=inputMat->rows;
	int cols;
	for (cols=0; cols<numColElem; cols++) {
		memcpy (outputMat->mat+rows*cols, inputMat->mat+(rows*colIndex[cols]), rows*sizeof (double));
	}
}

void subMatrixExtractUsingIndices (matrixD *inputMat, int *rowIndex, int numRows, int *colIndex, int numCols, matrixD *outputMat)
{
	if (numRows > inputMat->rows || numCols > inputMat->cols){
		printf ("ERROR: subMatrixExtractUsingIndices: Cannot extract beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numRows > outputMat->rows || numCols > outputMat->cols){
		printf ("ERROR: subMatrixExtractUsingIndices: Cannot access beyond matrix range\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

	int i, j, k=0;
	int rowsActual = inputMat->rows;
	int row = -1;
	int col = -1;
	bool extractError = false;
	for (i=0; i<numCols; i++){
		col = colIndex[i];
		if (col < 0 || col >= inputMat->cols){
			extractError = true;
			break;
		}
		for (j=0; j<numRows; j++){
			row = rowIndex[j];
			if (row < 0 || row >= inputMat->rows){
				extractError = true;
				break;
			}
			outputMat->mat[k] = inputMat->mat[row + col*rowsActual];
			k++;
		}
		if (extractError == true){
			break;
		}
	}
	if (extractError == true){
		printf ("ERROR: subMatrixExtractUsingIndices: Cannot extract beyond matrix range. Row = %d, Col = %d. Row range = (0,%d), Column range = (0,%d)\n",row, col, inputMat->rows, inputMat->cols);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
}

void vectorVectorAppend (vectorD *vec1, vectorD *vec2, vectorD *outputVec)
{
	memcpy (outputVec->vec, vec1->vec, sizeof(double)*(vec1->dimension));
	memcpy (outputVec->vec+vec1->dimension, vec2->vec, sizeof(double)*(vec2->dimension));
}

void matrixVectorAppend (char position, matrixD *inputMat, vectorD *inputVec)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	if (rows != inputVec->dimension) {
		printf ("ERROR: matrixVectorAppend: matrix and vector are not of compatible sizes\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	double *tmp;
	if (position=='F') {
		tmp=malloc (sizeof (double)*rows*(cols+1));
		memcpy (tmp, inputVec->vec, rows*sizeof (double));
		memcpy (tmp+inputVec->dimension, inputMat->mat, sizeof(double)*rows*cols);
		free (inputMat->mat);
	}
	else if (position=='B') {
		tmp=realloc (inputMat->mat, sizeof(double)*rows*(cols+1));
		memcpy (tmp+rows*cols, inputVec->vec, rows*sizeof(double));
		if (tmp == NULL) {
			printf ("ERROR: matrixVectorAppend: realloc returning NULL pointer. May be not enough memory case\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
	}
	else {
		printf ("ERROR: matrixVectorAppend: Invalid position. Choose either F or B\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	inputMat->mat=tmp;
	inputMat->cols=cols+1;
}

void matrixMatrixHorizontalAppend (matrixD *matrix1, matrixD *matrix2)
{
	int rows1=matrix1->rows;
	int cols1=matrix1->cols;
	int rows2=matrix2->rows;
	int cols2=matrix2->cols;
	double *tmp;
	if (rows1 != rows2) {
		printf ("ERROR: matrixMatrixHorizontalAppend: Horizontal append. Matrices are not of compatible sizes \n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	tmp=realloc (matrix1->mat, sizeof (double)*( (rows1+rows2)*(cols1+cols2)));
	if (tmp == NULL) {
		printf ("ERROR: matrixMatrixHorizontalAppend: realloc returning NULL pointer. May be not enough memory case\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	matrix1->mat=tmp;
	memcpy (matrix1->mat+rows1*cols1, matrix2->mat, sizeof (double)*rows2*cols2);
	matrix1->cols=cols1+cols2;
}

void matrixVectorVerticalAppend (char position, matrixD *inputMat, vectorD *inputVec, matrixD *outputMat)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	if (cols != inputVec->dimension) {
		printf ("ERROR: matrixVectorVerticalAppend: matrix and vector are not of compatible sizes \n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	int i;
	if (position=='T') {
		for (i=0; i<cols; i++) {
			memcpy (outputMat->mat+(rows+1)*i, inputVec->vec+i, sizeof (double)*1);
			memcpy (outputMat->mat+(rows+1)*i+1, inputMat->mat+(rows*i), sizeof (double)*rows);
		}
	}
	if (position=='B') {
		for (i=0; i<cols; i++) {
			memcpy (outputMat->mat+(rows+1)*i, inputMat->mat+(rows*i), sizeof (double)*rows);
			memcpy (outputMat->mat+(rows+1)*(i+1)-1, inputVec->vec+i, sizeof (double)*1);
		}
	}
}

void matrixMatrixVerticalAppend (matrixD *matrix1, matrixD *matrix2, matrixD *outputMat)
{
	int rows1=matrix1->rows;
	int cols1=matrix1->cols;
	int rows2=matrix2->rows;
	int cols2=matrix2->cols;
	if (cols1 != cols2) {
		printf ("ERROR: matrixMatrixVerticalAppend: Matrices are not of compatible sizes \n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

	int i;
	for (i=0; i<cols1; i++) {
		memcpy (outputMat->mat+(rows1+rows2)*i, matrix1->mat+(rows1*i), sizeof (double)*rows1);
		memcpy (outputMat->mat+(rows1+rows2)*i+rows1, matrix2->mat+(rows2*i), sizeof (double)*rows2);
	}
}

void vectorMatrixConvert(char type, vectorD *vectorA, matrixD *matrixA)
{
	if (type=='V') {
		vectorA->dimension=matrixA->rows * matrixA->cols;
		vectorA->vec=malloc (sizeof(double)*vectorA->dimension);
		memcpy (vectorA->vec, matrixA->mat, sizeof(double)*(vectorA->dimension));
	}
	if (type=='M') {
		matrixA->rows=vectorA->dimension;
		matrixA->cols=1;
		matrixA->mat=malloc (sizeof(double)*vectorA->dimension);
		memcpy (matrixA->mat, vectorA->vec, sizeof(double)*(vectorA->dimension));
	}
}

int vectorCompareAscend (const void *vec1, const void *vec2)
{
	double l = (double)((vecCompareStruct *)vec1)->value; 
	double r = (double)((vecCompareStruct *)vec2)->value; 
	if (l > r){
		return 1;
	}
	else if (l < r){
		return -1;
	}
	else{
		return 0;
	}
}

int vectorCompareDescend (const void *vec1, const void *vec2)
{
	double l = (double)((vecCompareStruct *)vec1)->value; 
	double r = (double)((vecCompareStruct *)vec2)->value; 
	if (l < r){
		return 1;
	}
	else if (l > r){
		return -1;
	}
	else{
		return 0;
	}
}

void vectorSort (vectorD *inputPt, int *indices, vectorD *sortedValues, char order, bool inPlace)
{
	int dimension = inputPt->dimension;
	int i;
	if (dimension <= 0){
		printf ("ERROR: vectorSort: Cannot sort an empty array\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (dimension == 1){
		if (inPlace == false){
			indices[0] = 0;
			sortedValues->vec[0] = inputPt->vec[0];
		}
		else {
			indices[0] = 0;
		}
	}
	else {
		vecCompareStruct *vectorObjects = malloc (sizeof(vecCompareStruct)*dimension);
		for (i=0; i<dimension; i++){
			vectorObjects[i].value = inputPt->vec[i];
			vectorObjects[i].index = i;
		}
		if (order == 'A'){
			qsort (vectorObjects, dimension, sizeof(vectorObjects[0]), vectorCompareAscend);
		}
		else if (order == 'D'){
			qsort (vectorObjects, dimension, sizeof(vectorObjects[0]), vectorCompareDescend);
		}
		else {
			printf ("ERROR: vectorSort: Unknown order. It must be (A)scend or (D)escend\n");
		}
		if (inPlace == false){
			for (i=0; i<dimension; i++){
				indices[i] = vectorObjects[i].index;
				sortedValues->vec[i] = vectorObjects[i].value;
			}
		}
		else {
			for (i=0; i<dimension; i++){
				indices[i] = vectorObjects[i].index;
				inputPt->vec[i] = vectorObjects[i].value;
			}
		}
		free (vectorObjects);
	}
}

void vectorReverse (vectorD *inputVec, int *indices)
{
	int startIndex = 0;
	int endIndex = inputVec->dimension-1;
	int tmp;
	while (startIndex < endIndex){
		tmp = inputVec->vec[startIndex];
		inputVec->vec[startIndex] = inputVec->vec[endIndex];
		inputVec->vec[endIndex] = tmp;
		indices[startIndex] = endIndex;
		indices[endIndex] = startIndex;
		startIndex++;
		endIndex -= 1;
	}
}

int findValueInVector (vectorD *x, double value, double precision)
{
	int index = -1;
	int i = 0;
	while (i<x->dimension){
		if (fabs(x->vec[i]-value) < precision){
			index = i;
			break;
		}
		i++;
	}
	return index;
}

int findVectorInMatrix (vectorD *x, matrixD *M, int startPtIndex, int endPtIndex, double precision)
{
	int index = -1; //if point doesn't exist inside matrix range, it returns -1.
	int i,j;
	for (i=startPtIndex; i<endPtIndex; i++){
		double dist = 0;
		for (j=0; j<M->rows; j++){
			dist += pow (x->vec[j] - M->mat[j+i*M->rows], 2);
		}
		if (sqrt(dist) < precision){
			index = i;
		}
	}
	return index;
}

void uniqueMatrix (matrixD *A, int *indices, int *numElem, double precision)
{
	int i,j,k;
	char *uniquePt = malloc (sizeof(char)*A->cols);
	memset (uniquePt, 'U', sizeof(char)*A->cols);
	double dist;
	for (i=0; i<A->cols-1; i++){
		if (uniquePt[i] == 'U'){
			for (j=i+1; j<A->cols; j++){
				dist = 0;
				for (k=0; k<A->rows; k++){
					dist += pow (A->mat[i*A->rows + k] - A->mat[j*A->rows + k], 2);
				}
				if (sqrt(dist) < precision){
					uniquePt[j] = 'R';
				}
			}
		}
	}
	k = 0;
	for (i=0; i<A->cols; i++){
		if (uniquePt[i] == 'U'){
			indices[k] = i;
			k++;
		}
	}
	*numElem = k;
	free (uniquePt);
}

void freeVectorD (vectorD *vector)
{
	free (vector->vec);
	free (vector);
}

void freeVectorZ (vectorZ *vector)
{
	free (vector->vec);
	free (vector);
}

void freeMatrixD (matrixD *matrix)
{
	free (matrix->mat);
	free (matrix);
}
