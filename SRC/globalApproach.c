/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "globalApproach.h"

void globalApproach (inputData *inputParam, solutionStruct *solution)
{
	int numSolutions = 0;
	solution->globalOptimaReached = false;
	int numMultiStartPts = inputParam->numMultiStartPts;
	matrixD *multiStart = malloc (sizeof(matrixD));
	matrixInit (multiStart, inputParam->dimension, numMultiStartPts);
	inputParam->randomSeed++;
	vectorD *solutionValues = malloc (sizeof(vectorD));
	vectorInit (solutionValues, multiStart->cols);
	char *solutionValuesTrue = malloc (sizeof(char)*solutionValues->dimension); //true(T), false(F), or repeat(R)
	memset (solutionValuesTrue, 'F',sizeof(char)*solutionValues->dimension);
	int iterations = 0;
	solution->allPointsRepeat = false;
	int k = 0;
	double *randomNum = malloc (sizeof(double)*inputParam->maxAllowedEvals);
	for (k=0; k<inputParam->maxAllowedEvals; k++){
		randomNum[k] = (double)rand()/(double)RAND_MAX;
	}

	k = 0;
	while (solution->totalPts < inputParam->maxAllowedEvals && solution->globalOptimaReached == false && iterations < inputParam->maxGlobalIterations){
		if (solution->allPointsRepeat == false){
			double delta = inputParam->stepLength;
			int restarts = 0;
			memset (solutionValuesTrue, 'F', sizeof(char) * solutionValues->dimension);
			do {
				multiStartSurr (inputParam, solution, multiStart, solutionValues, solutionValuesTrue, &numSolutions, delta, NULL, NULL);
				delta = delta/4.0;
				restarts++;
			} while (solution->optimizationSuccessFul == false && restarts < 10);

			if (solution->optimizationSuccessFul == false){
				solution->searchType = 'G';
			}
			else{
				exploitLocalMin (inputParam, solution, multiStart, solutionValues, numSolutions); 
				if (iterations < inputParam->initialReqGlobalIters){
					solution->searchType = 'G';
				}
				if (iterations >= inputParam->initialReqGlobalIters){
					solution->searchType = detectSearchType (inputParam, solution, iterations, randomNum[k]);
					k++;
				}
			}
		}
		else{
			if (solution->totalPts >= inputParam->maxAllowedEvals){
				break;
			}
			solution->searchType = 'G';
			globalExploration (inputParam, solution);
		}

		if (solution->searchType == 'B'){
			if (solution->totalPts >= inputParam->maxAllowedEvals){
				break;
			}
			bestSolExploitation (inputParam, solution, solutionValues, multiStart, numSolutions);
		}
		else {
			if (solution->totalPts >= inputParam->maxAllowedEvals){
				break;
			}
			globalExploration (inputParam, solution);
		}
		iterations++;
	}
	if (iterations >= inputParam->maxGlobalIterations && inputParam->debugLogLevel > -1){
		printf ("INFO: globalApproach: Maximum iterations for global approach crossed. Terminating the algorithm\n");
	}

	if (solution->globalOptimaReached == true && inputParam->debugLogLevel > -1){
		printf ("INFO: globalApproach: found global optimum within the given precision\n");
	}

	freeMatrixD (multiStart);
	freeVectorD (solutionValues);
	free (solutionValuesTrue);
	free (randomNum);
}

void bestSolExploitation (inputData* inputParam, solutionStruct *solution, vectorD *solutionValues, matrixD *multiStart, int numSolutions)
{
	vectorD *tmpVector = malloc (sizeof(vectorD));
	vectorInit (tmpVector, inputParam->dimension);
	vectorD *solutionValuesTmp = malloc (sizeof(vectorD));
	vectorInit (solutionValuesTmp, numSolutions);
	memcpy (solutionValuesTmp->vec, solutionValues->vec, sizeof(double)*numSolutions);
	int maxIters = inputParam->dimension + 1;

	int index = 0;
	while (index < numSolutions && solution->totalPts < inputParam->maxAllowedEvals){
		if (solution->exploitLevel->vec[index] == 1){
			index++;
		}
		if (index >= numSolutions){
			solution->allPointsRepeat = true;
			break;
		}
		vecExtractMat (tmpVector, multiStart, index);
		if (solution->stepType != NULL){
			free (solution->stepType);
		}
		solution->stepType = strdup ("Exploitation(RBF)");
		updateSolution (inputParam, solution, tmpVector);
		if (solution->repeatPoint == true){
			index++;
		}
		else{
			solution->allPointsRepeat = false;
			break;
		}
	}
	char ptType = solution->pointType[solution->inputPtIndex];
	memcpy (tmpVector->vec, solution->bestPoint->vec, sizeof(double)*inputParam->dimension);
	int localRBFSearchStatus = 1;
	if (ptType == 'H' || ptType == 'S' || ptType == 'U'){
		localRBFSearchStatus = localRBFSearch (inputParam, solution, tmpVector, solution->bestPtIndex, maxIters, 'L');
		if (localRBFSearchStatus >= 0 && solution->repeatPoint == false){
			freeVectorD (solutionValuesTmp);
			freeVectorD (tmpVector);
			return;
		}
	}

	if ((localRBFSearchStatus == -1 || solution->repeatPoint == true) && solution->totalPts < inputParam->maxAllowedEvals){ //best point from RBF is repeated so trying random point.
		if (solution->numUnrelaxablePts > 0 || solution->numNQInfeasiblePts > 0 || solution->numHiddenConstrPts > 0){
			localRBFSearchStatus = localRBFSearch (inputParam, solution, tmpVector, solution->bestPtIndex, maxIters, 'L');
			if (localRBFSearchStatus >= 0 && solution->repeatPoint == false){
				freeVectorD (solutionValuesTmp);
				freeVectorD (tmpVector);
				return;
			}
		}
		if (solution->stepType != NULL){
			free (solution->stepType);
		}
		solution->stepType = strdup("Exploitation(Rand)");
		double radius = inputParam->stepLength;
		int index = solution->inputPtIndex;
		//Generate random pts only around feasible or infeasible pts.
		if (solution->pointType[index] != 'F' || solution->pointType[index] != 'I'){
			memcpy (tmpVector->vec, solution->bestPoint->vec, sizeof(double)*inputParam->dimension);
		}
		else {
			vecExtractMat (tmpVector, multiStart, index);
		}
		bool checkFeasibility = true;
		randomPtInRadius (inputParam, solution, tmpVector, radius, checkFeasibility);
		updateSolution (inputParam, solution, tmpVector);
	}
	double epsilon = 1E-3;
	if (solution->repeatPoint == false
			&& solution->totalPts < inputParam->maxAllowedEvals
			&& solution->sumConstraints->vec[solution->totalPts-1] > -epsilon
			&& solution->objectiveValues->vec[solution->totalPts-1] < solution->bestFeasibleObjectiveValue
		       	&& solution->pointType[solution->totalPts-1] == 'I'){
		bool projectionStatus = ptProjectOnFeasible (inputParam, solution, tmpVector);
		if (projectionStatus == true){
			updateSolution (inputParam, solution, solution->optimSolutionPt);
			if (solution->repeatPoint == true){
				projectionStatus = false;
			}
		}
		if (projectionStatus == false || solution->pointType[solution->totalPts-1] == 'I'){
			pushInfeasIntoFeasRegion (inputParam, solution, tmpVector);
		}
	}

	if (index >= numSolutions && solution->repeatPoint == true){
		solution->allPointsRepeat = true;
	}
	freeVectorD (solutionValuesTmp);
	freeVectorD (tmpVector);
}

void globalExploration (inputData* inputParam, solutionStruct *solution)
{
	double epsilon = 1E-3;
	vectorD *feasiblePt = malloc (sizeof(vectorD));
	vectorInit (feasiblePt, inputParam->dimension);
	if (solution->stepType != NULL){
		free (solution->stepType);
	}
	solution->stepType = strdup ("Exploration(RBF)");
	globalPtSurr (inputParam, solution, NULL, NULL);
	if (solution->optimizationSuccessFul == true && solution->totalPts < inputParam->maxAllowedEvals){
		updateSolution (inputParam, solution, solution->optimSolutionPt);
	}
	char ptType = solution->pointType[solution->inputPtIndex];
	int maxIters = inputParam->dimension + 1;
	int localRBFSearchStatus = 1;
	if ( ptType == 'H' || ptType == 'S' || ptType == 'U'){
		memcpy (feasiblePt->vec, solution->bestPoint->vec, sizeof(double)*inputParam->dimension);
		localRBFSearchStatus = localRBFSearch (inputParam, solution, feasiblePt, solution->bestPtIndex, maxIters, 'G');
	}
	memcpy (feasiblePt->vec, solution->optimSolutionPt->vec, sizeof(double)*inputParam->dimension);
	if (solution->repeatPoint == false
			&& solution->totalPts < inputParam->maxAllowedEvals
			&& solution->sumConstraints->vec[solution->totalPts-1] > -epsilon
			&& solution->objectiveValues->vec[solution->totalPts-1] < solution->bestFeasibleObjectiveValue
		       	&& solution->pointType[solution->totalPts-1] == 'I'){
		bool projectionStatus = ptProjectOnFeasible (inputParam, solution, solution->optimSolutionPt);
		if (projectionStatus == true){
			updateSolution (inputParam, solution, solution->optimSolutionPt);
			if (solution->repeatPoint == true){
				projectionStatus = false;
			}
		}
		if (projectionStatus == false || solution->pointType[solution->totalPts-1] == 'I'){
			pushInfeasIntoFeasRegion (inputParam, solution, feasiblePt);
		}
	}

	if (solution->totalPts < inputParam->maxAllowedEvals && (solution->optimizationSuccessFul == false || solution->repeatPoint == true || localRBFSearchStatus == -1)){
		int lastFeasibleIndex = solution->totalPts - 1;
		while (solution->pointType[lastFeasibleIndex] != 'F' && lastFeasibleIndex >= 0){
			lastFeasibleIndex -= 1;
		}
		vecExtractMat (feasiblePt, solution->allPtsMatrix, lastFeasibleIndex);
		//double explorationRadius = solution->minMaxDistance;//using old minMaxDistance.
		double explorationRadius = 10 * sqrt(inputParam->dimension);
		if (solution->stepType != NULL){
			free (solution->stepType);
		}
		solution->stepType = strdup ("Exploration(Rand)");
		bool checkFeasibility = true;
		randomPtInRadius (inputParam, solution, feasiblePt, explorationRadius, checkFeasibility);
		updateSolution (inputParam, solution, feasiblePt);
		memcpy (solution->optimSolutionPt->vec, feasiblePt->vec, sizeof(double)*inputParam->dimension);
		if (solution->repeatPoint == false
				&& solution->totalPts < inputParam->maxAllowedEvals
				&& solution->sumConstraints->vec[solution->totalPts-1] > -epsilon
				&& solution->objectiveValues->vec[solution->totalPts-1] < solution->bestFeasibleObjectiveValue
				&& solution->pointType[solution->totalPts-1] == 'I'){
			bool projectionStatus = ptProjectOnFeasible (inputParam, solution, solution->optimSolutionPt);
			if (projectionStatus == true){
				updateSolution (inputParam, solution, solution->optimSolutionPt);
				if (solution->repeatPoint == true){
					projectionStatus = false;
				}
			}
			if (projectionStatus == false || solution->pointType[solution->totalPts-1] == 'I'){
				pushInfeasIntoFeasRegion (inputParam, solution, feasiblePt);
			}
			solution->repeatPoint = false;
		}
	}
	freeVectorD (feasiblePt);
	if (solution->repeatPoint == false){
		solution->allPointsRepeat = false;
	}
	else{
		if (solution->totalPts < inputParam->maxAllowedEvals){
			printf ("ERROR: globalExploration: Repeated point in global optimization.\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
	}
}

void exploitLocalMin (inputData *inputParam, solutionStruct *solution, matrixD *multiStart, vectorD *solutionValues, int numSolutions)
{
	int dimension = inputParam->dimension;
	vectorD *inputPt = malloc (sizeof(double) *dimension);
	vectorInit (inputPt, dimension);
	vectorD *tmpVector = malloc (sizeof(double) *dimension);
	vectorInit (tmpVector, dimension);
	int i,k;
	int count = 0;
	double value;
	for (k=0; k<numSolutions; k++){
		vecExtractMat (tmpVector, multiStart, k);
		count = 0;
		for (i=0; i<solution->totalPts; i++){
			if (solution->pointType[i] == 'F'){
				vecExtractMat (inputPt, solution->allPtsMatrix, i);
				vectorAddOrig (1.0, inputPt, -1.0, tmpVector);
				value = vectorNorm2 (tmpVector);
				if (value < inputParam->deltaExploitation){
					count++;
				}
			}
		}
		if (count >= inputParam->maxVicinityPts){
			solution->exploitLevel->vec[k] = 1;
		}
		else{
			solution->exploitLevel->vec[k] = 0;
		}
	}
	freeVectorD (inputPt);
	freeVectorD (tmpVector);
}

char detectSearchType (inputData *inputParam, solutionStruct *solution, int iterations, double randomNum)
{
	if (randomNum < 0.50){
		return 'B';
	}
	else {
		return 'G';
	}
}

