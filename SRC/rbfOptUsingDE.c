/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "rbfOptUsingDE.h"

void constrCorrection (int numConstraints, double *constr, double precision)
{
	int i;
	for (i=0; i<numConstraints; i++){
		if (constr[i] < 0 && constr[i] > -precision){
			constr[i] = 0;
		}
	}
}

bool userDataInit (inputData *inputParam, solutionStruct *solution, userData *ud, bool feasibilitySearch, bool maxDistFromFeasible, bool computeObj, double delta, vectorD *nqPt, char *fullRank)
{
	int i, k;
	int dimension = inputParam->dimension;
	ud->knnParam = inputParam->knnParam;
	ud->rbfVector = NULL;
	ud->rbfMatrix = NULL;
	ud->customAprioriConstr = inputParam->udCustomAprioriConstr;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	ud->numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	if (ud->numAprioriConstraints > 0){
		ud->aprioriConstrType = malloc (sizeof(int)*ud->numAprioriConstraints);
	}

	k = 0;
	for (i=0; i<inputParam->numConstraints; i++){
		if (inputParam->constraintType->vec[i] == inputParam->indexQRAK){
			ud->aprioriConstrType[k] = inputParam->indexQRAK;
			k++;
		}
		else if (inputParam->constraintType->vec[i] == inputParam->indexNRAK){
			ud->aprioriConstrType[k] = inputParam->indexNRAK;
			k++;
		}
		else if (inputParam->constraintType->vec[i] == inputParam->indexQUAK){
			ud->aprioriConstrType[k] = inputParam->indexQUAK;
			k++;
		}
		else if (inputParam->constraintType->vec[i] == inputParam->indexNUAK){
			ud->aprioriConstrType[k] = inputParam->indexNUAK;
			k++;
		}
	}

	if (ud->numAprioriConstraints > 0 && ud->customAprioriConstr){
		strcpy (ud->aprioriInputPtFile, inputParam->aprioriInputPtFile);
		strcpy (ud->aprioriExec, inputParam->aprioriExec);
		strcpy (ud->aprioriOutputPtFile, inputParam->aprioriOutputPtFile);
	}
	ud->delta = delta;
	ud->deltaSqr = delta * delta;
	if (nqPt != NULL){
		ud->nqPt = nqPt;
	}
	else {
		ud->nqPt = NULL;
	}
	ud->totalPts = solution->totalPts;
	ud->constrPrecision = inputParam->precision;
	ud->numHiddenConstrPts = solution->numHiddenConstrPts;
	ud->numUnrelaxablePts = solution->numUnrelaxablePts;
	ud->numNQInfeasiblePts = solution->numNQInfeasiblePts;
	ud->numFeasiblePts = solution->numFeasiblePts;
	ud->numInfeasiblePts = solution->numInfeasiblePts;
	ud->useKNN = false;
	if ((ud->numFeasiblePts > 2 || ud->numInfeasiblePts > 2) && (ud->numNQInfeasiblePts > 2 || ud->numHiddenConstrPts > 2 || ud->numUnrelaxablePts > 2)){
		ud->useKNN = true;
	}
	ud->feasibilitySearch = feasibilitySearch;
	ud->maxDistFromFeasible = maxDistFromFeasible;
	ud->computeObj = computeObj;
	ud->dimension = dimension;
	ud->numConstraints = numConstraints;
	ud->numRBFConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	ud->includePolyTail = inputParam->includePolyTail;
	ud->lb = inputParam->lb;
	ud->ub = inputParam->ub;
	ud->allPtsMatrix = malloc (sizeof(matrixD));
	matrixInit (ud->allPtsMatrix, dimension, solution->totalPts);
	memcpy (ud->allPtsMatrix->mat, solution->allPtsMatrix->mat, sizeof(double)*dimension*solution->totalPts);

	ud->pointType = solution->pointType;
	ud->singleRBFtype = inputParam->singleRBFtype;

	ud->numConstraintsQRSK = inputParam->numTypeConstraints[inputParam->indexQRSK-1];
	ud->numConstraintsQUSK = inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	ud->identityAppendedNegativeOnes = NULL;
	ud->negativeidentityAppendedOnes = NULL;
	if (feasibilitySearch == true){
		int n = dimension+1;
		ud->identityAppendedNegativeOnes = malloc (sizeof(matrixD));
		matEyeD (ud->identityAppendedNegativeOnes, n, dimension);
		for (i=0; i<dimension; i++){
			ud->identityAppendedNegativeOnes->mat[i*n +(n-1)] = -1;
		}
		ud->negativeidentityAppendedOnes = malloc (sizeof(matrixD));
		matEyeD (ud->negativeidentityAppendedOnes, n, dimension);
		for (i=0; i<dimension; i++){
			ud->negativeidentityAppendedOnes->mat[i*n +(n-1)] = 1;
		}
		scaleMatrix (ud->negativeidentityAppendedOnes, -1);
	}

	matrixD *constraintMatrix = malloc (sizeof(matrixD));
	int nPts = solution->numInfeasiblePts + solution->numUnrelaxablePts + solution->numFeasiblePts + solution->numNQInfeasiblePts;
	matrixInit (constraintMatrix, numConstraints, nPts);
	int j;
	k = 0;
	for (i=0; i<solution->totalPts; i++){
		if (solution->pointType[i] != 'H'){
			for (j=0; j<inputParam->numConstraints; j++){
				if (inputParam->constraintType->vec[j] == inputParam->indexQUSK || inputParam->constraintType->vec[j] == inputParam->indexQRSK){
					constraintMatrix->mat[k] = solution->allConstraintValues->mat[j+i*inputParam->numConstraints];
					k++;
				}
			}
		}
	}

	matrixD *constraintMatrixTrans = malloc (sizeof(matrixD));
	matrixInit (constraintMatrixTrans, constraintMatrix->cols, constraintMatrix->rows);
	matrixTranspose (constraintMatrix, constraintMatrixTrans);
	freeMatrixD (constraintMatrix);

	k = 0;
	matrixD *allPtsMatrixTrans = malloc (sizeof(matrixD));
	matrixInit (allPtsMatrixTrans, ud->allPtsMatrix->cols, ud->allPtsMatrix->rows);
	matrixTranspose (ud->allPtsMatrix, allPtsMatrixTrans);

	char *isConstraintNaN = malloc (sizeof(char)*ud->allPtsMatrix->cols);
	int sizeCoefficients = ud->allPtsMatrix->cols + dimension + 1;
	ud->coefficients = malloc (sizeof(matrixD));
	matZerosD (ud->coefficients, solution->totalPts + dimension + 1, ud->numRBFConstraints);
	ud->rbfType = malloc (sizeof(char)*ud->numRBFConstraints);
	ud->rbfParam = malloc (sizeof(double)*ud->numRBFConstraints);
	double smallNum = -1e30;

	int t = 0; //constraint counter. Only for QRSK and QUSK.
	int rows = ud->coefficients->rows;
	bool rankStatus = 0;
	if (fullRank == NULL){
		fullRank = malloc (sizeof(char)*inputParam->numConstraints);
		memset (fullRank, 'T', sizeof(char)*inputParam->numConstraints);
	}
	else {
		rankStatus = 1;
	}
	for (i=0; i<inputParam->numConstraints; i++){
		if ((inputParam->constraintType->vec[i] == inputParam->indexQUSK || inputParam->constraintType->vec[i] == inputParam->indexQRSK) && fullRank[i] == 'F'){
			ud->rbfType[t] = inputParam->rbfType[i];
			ud->rbfParam[t] = inputParam->rbfParam[i];
			ud->coefficients->mat[t*rows + rows-1] = 1;
			t++;
		}
	}

	t =0;
	for (i=0; i<inputParam->numConstraints; i++){
		if ((inputParam->constraintType->vec[i] == inputParam->indexQUSK || inputParam->constraintType->vec[i] == inputParam->indexQRSK) && fullRank[i] == 'T'){
			ud->rbfType[t] = inputParam->rbfType[i];
			ud->rbfParam[t] = inputParam->rbfParam[i];
			memset (isConstraintNaN, 'F', sizeof(char)*ud->allPtsMatrix->cols);
			int numConstraintNaN = 0;
			for (j=0; j<constraintMatrixTrans->rows; j++){
				if (constraintMatrixTrans->mat[j + constraintMatrixTrans->rows*t] < smallNum){
					isConstraintNaN[j] = 'T';
					numConstraintNaN++;
				}
			}
			if (solution->totalPts - numConstraintNaN > 2){
				matrixD *ptsMatrix = malloc (sizeof(matrixD)); //picking pts which didn't fail.
				matrixInit (ptsMatrix, dimension, constraintMatrixTrans->rows-numConstraintNaN);
				vectorD *constraintVector = malloc (sizeof(vectorD));
				vectorInit (constraintVector, ptsMatrix->cols);
				int *ptsIndices = malloc (sizeof(int)*ptsMatrix->cols);
				for (j=0; j<ptsMatrix->cols; j++){
					ptsIndices[j] = -1;
				}
				int k = 0;
				for (j=0; j<constraintMatrixTrans->rows; j++){
					double constrVal = constraintMatrixTrans->mat[t*constraintMatrixTrans->rows + j];
					if (constrVal > smallNum){
						constraintVector->vec[k] = constrVal;
						k++;
					}
				}
				if (k < constraintVector->dimension){
					printf ("ERROR: userDataInit: Value missing in constraintVector\n");
					exit (EXIT_FAILURE);
				}
				k = 0;
				for (j=0; j<solution->totalPts; j++){
					if (isConstraintNaN[j] == 'F' && solution->pointType[j] != 'H'){ 
						memcpy (ptsMatrix->mat + k*dimension, solution->allPtsMatrix->mat + j*dimension, sizeof(double)*dimension);
						ptsIndices[k] = j;
						k++;
					}
				}
				matrixD *ptsMatrixTrans = malloc (sizeof(matrixD));
				matrixInit (ptsMatrixTrans, ptsMatrix->cols, ptsMatrix->rows);
				matrixTranspose (ptsMatrix, ptsMatrixTrans);
				vectorD *coefficient = malloc (sizeof(vectorD));
				vectorInit (coefficient, ptsMatrix->cols + dimension + 1);
				matrixD *diffMatrix = malloc (sizeof(matrixD));
				matrixInit (diffMatrix, ptsMatrix->cols, ptsMatrix->cols);
				subMatrixExtractUsingIndices (solution->diffMatrix, ptsIndices, ptsMatrix->cols, ptsIndices, ptsMatrix->cols, diffMatrix);
				free (ptsIndices);
				bool rbfStatus = rbfComputeCoefficients (ptsMatrix, ptsMatrixTrans, diffMatrix, constraintVector, coefficient, ud->rbfType[t], ud->rbfParam[t], ud->includePolyTail);
				freeMatrixD (diffMatrix);
				if (rbfStatus == false){
					solution->optimizationSuccessFul = false;
					freeMatrixD (ptsMatrix);
					freeVectorD (coefficient);
					freeVectorD (constraintVector);
					freeMatrixD (constraintMatrixTrans);
					free (isConstraintNaN);
					freeMatrixD (ptsMatrixTrans);
					freeMatrixD (allPtsMatrixTrans);
					freeUserData (ud);
					return false;
				}
				k = 0;
				for (j=0; j<solution->totalPts; j++){
					if (solution->pointType[j] == 'H' || isConstraintNaN[j] == 'T'){
						ud->coefficients->mat[t* ud->coefficients->rows + j] = 0;
					}
					else {
						ud->coefficients->mat[t* ud->coefficients->rows + j] = coefficient->vec[k];
						k++;
					}
				}
				memcpy (ud->coefficients->mat + t*sizeCoefficients + ud->allPtsMatrix->cols, coefficient->vec + ptsMatrix->cols, sizeof(double)*(dimension+1)); //to get last dim+1 elements from coefficient vec.
				freeVectorD (constraintVector);
				freeMatrixD (ptsMatrix);
				freeMatrixD (ptsMatrixTrans);
				freeVectorD (coefficient);
			}
			t++;
		}
	}
	freeMatrixD (constraintMatrixTrans);
	freeMatrixD (allPtsMatrixTrans);
	free (isConstraintNaN);
	if (rankStatus == 0){
		free (fullRank);
	}

	ud->objectiveCoeff = NULL;
	if (computeObj == true){
		vectorD *objectiveValues = malloc (sizeof(vectorD));
		int numFeasibleAndInfeasiblePts = solution->numFeasiblePts + solution->numInfeasiblePts + solution->numNQInfeasiblePts;
		matrixD *ptsMatrix = malloc (sizeof(matrixD));
		matrixInit (ptsMatrix, dimension, numFeasibleAndInfeasiblePts);
		int *ptsIndices = malloc (sizeof(int)*ptsMatrix->cols);
		for (i=0; i<ptsMatrix->cols; i++){
			ptsIndices[i] = -1;
		}
		int i, k = 0;
		for (i=0; i<solution->totalPts; i++){
			if (solution->pointType[i] == 'F' || solution->pointType[i] == 'I' || solution->pointType[i] == 'S'){
				memcpy (ptsMatrix->mat + k*dimension, solution->allPtsMatrix->mat+i*dimension, sizeof(double)*dimension);
				ptsIndices[k] = i;
				k++;
			}
		}
		matrixD *ptsMatrixTrans = malloc (sizeof(matrixD));
		matrixInit (ptsMatrixTrans, ptsMatrix->cols, ptsMatrix->rows);
		matrixTranspose (ptsMatrix, ptsMatrixTrans);
		vectorInit (objectiveValues, numFeasibleAndInfeasiblePts);
		vectorD *objectiveCoeff = malloc (sizeof(vectorD));
		vectorInit (objectiveCoeff, numFeasibleAndInfeasiblePts + dimension + 1);
		ud->rbfTypeObj = inputParam->rbfTypeObj;
		ud->rbfParamObj = inputParam->rbfParamObj;
		k=0;
		for (i=0; i<solution->totalPts; i++){
			if (solution->pointType[i] == 'F' || solution->pointType[i] == 'I' || solution->pointType[i] == 'S'){
				objectiveValues->vec[k] = solution->objectiveValues->vec[i];
				k++;
			}
		}
		matrixD *diffMatrix = malloc (sizeof(matrixD));
		matrixInit (diffMatrix, ptsMatrix->cols, ptsMatrix->cols);
		subMatrixExtractUsingIndices (solution->diffMatrix, ptsIndices, ptsMatrix->cols, ptsIndices, ptsMatrix->cols, diffMatrix);
		free (ptsIndices);
		rbfComputeCoefficients (ptsMatrix, ptsMatrixTrans, diffMatrix, objectiveValues, objectiveCoeff, ud->rbfTypeObj, ud->rbfParamObj, ud->includePolyTail);
		freeMatrixD (diffMatrix);
		ud->objectiveCoeff = malloc (sizeof(vectorD));
		vecZerosD (ud->objectiveCoeff, solution->totalPts + dimension + 1);
		k = 0;
		for (i=0; i<solution->totalPts; i++){
			if (solution->pointType[i] == 'F' || solution->pointType[i] == 'I' || solution->pointType[i] == 'S'){
				ud->objectiveCoeff->vec[i] = objectiveCoeff->vec[k];
				k++;
			}
		}
		memcpy (ud->objectiveCoeff->vec+solution->totalPts, objectiveCoeff->vec+numFeasibleAndInfeasiblePts, sizeof(double)*(dimension+1));
		freeVectorD (objectiveValues);
		freeVectorD (objectiveCoeff);
		freeMatrixD (ptsMatrix);
		freeMatrixD (ptsMatrixTrans);
	}
	ud->rbfVector = malloc (sizeof(vectorD));
	vectorInit (ud->rbfVector, ud->allPtsMatrix->cols);
	ud->distVec = malloc (sizeof(vectorD));
	vectorInit (ud->distVec, ud->allPtsMatrix->cols);
	if (ud->singleRBFtype == false){
		ud->rbfMatrix = malloc (sizeof(matrixD));
		matrixInit (ud->rbfMatrix, ud->allPtsMatrix->cols, ud->coefficients->cols);
	}
	return true;
}

void fPtrFeasiblePtOpt (double *x, double *obj, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	*obj = -x[n-1];
	int i,j;
	int offset = 0;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	double dist, distSqr;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	for (i=0; i<udptr->coefficients->cols; i++){
		constr[i+offset] -= x[n-1];
	}
	offset += udptr->coefficients->cols;

	for (i=0; i<n-1; i++) {
		constr[i+offset] = x[i] - x[n-1] - udptr->lb->vec[i];
	}
	offset += n-1;
	for (i=0; i<n-1; i++) {
		constr[i+offset] = -x[i] - x[n-1] + udptr->ub->vec[i]; 
	}
	offset += n-1;

	//Distance constraints for hidden and NRSK infeasible points.
	if (udptr->numHiddenConstrPts > 0 || udptr->numNQInfeasiblePts > 0){
		int k = 0;
		double xnSqr = x[n-1] * x[n-1];
		for (i=0; i<udptr->totalPts; i++) {
			if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
				constr[k+offset] = udptr->distVec->vec[i] - udptr->deltaSqr - xnSqr;
				k++;
			}
		}
		offset += k;
	}

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void fPtrMaxDistFeasibleOpt (double *x, double *obj, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	*obj = -x[n-1];
	int i,j;
	int offset = 0;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	double dist, distSqr;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	offset += udptr->coefficients->cols;

	//Distance from all computed points.
	double xnSqr = x[n-1] * x[n-1];
	for (i=0; i<udptr->totalPts; i++) {
		if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
			constr[i+offset] = udptr->distVec->vec[i] - udptr->deltaSqr - xnSqr;
		}
		else {
			constr[i+offset] = udptr->distVec->vec[i] - xnSqr;
		}
	}
	offset += udptr->totalPts;

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void fPtrGlobalOpt (double *x, double *obj, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	double dist, distSqr;
	int i,j;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	*obj = rbfValueSingle (x, udptr->allPtsMatrix, udptr->objectiveCoeff, udptr->rbfTypeObj, udptr->rbfParamObj, udptr->rbfVector);
	int offset = 0;
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	offset += udptr->coefficients->cols;

	//Distance from all computed points.
	double nonHiddenDelta = udptr->deltaSqr/16.0;
	for (i=0; i<udptr->totalPts; i++) {
		if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
			constr[i+offset] = udptr->distVec->vec[i] - udptr->deltaSqr;
		}
		else {
			constr[i+offset] = udptr->distVec->vec[i] - nonHiddenDelta;
		}
	}
	offset += udptr->totalPts;

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void fPtrMultiStartOpt (double *x, double *obj, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	int i,j;
	double dist, distSqr;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	*obj = rbfValueSingle (x, udptr->allPtsMatrix, udptr->objectiveCoeff, udptr->rbfTypeObj, udptr->rbfParamObj, udptr->rbfVector);
	int offset = 0;
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	offset += udptr->coefficients->cols;

	//Distance constraints for hidden and NRSK infeasible points.
	if (udptr->numHiddenConstrPts > 0 || udptr->numNQInfeasiblePts > 0){
		int k = 0;
		for (i=0; i<udptr->totalPts; i++) {
			if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
				constr[k+offset] = udptr->distVec->vec[i] - udptr->deltaSqr;
				k++;
			}
		}
		offset += k;
	}

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void fPtrPtProject (double *x, double *obj, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	int i,j;
	double dist, distSqr = 0;
	for (i=0; i<udptr->dimension; i++){
		dist = x[i]-udptr->nqPt->vec[i];
		distSqr += dist * dist;
	}
	*obj = sqrt (distSqr);
	int offset = 0;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	offset += udptr->coefficients->cols;

	//Distance constraints for hidden and NRSK infeasible points.
	if (udptr->numHiddenConstrPts > 0 || udptr->numNQInfeasiblePts > 0){
		int k = 0;
		for (i=0; i<udptr->totalPts; i++) {
			if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
				constr[k+offset] = udptr->distVec->vec[i] - udptr->deltaSqr;
				k++;
			}
		}
		offset += k;
	}

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void fPtrFeasTest (double *x, double *constr, int n, int m, void *ud)
{
	userData *udptr = (userData *)ud;
	int offset = 0;
	int rows = udptr->allPtsMatrix->rows;
	int cols = udptr->allPtsMatrix->cols;
	double dist, distSqr;
	int i,j;
	for (i=0; i<cols; i++){
		distSqr = 0;
		for (j=0; j<rows; j++){
			dist = x[j] - udptr->allPtsMatrix->mat[j+i*rows];
			distSqr += dist * dist;
		}
		udptr->distVec->vec[i] = distSqr;
	}
	memcpy (udptr->rbfVector->vec, udptr->distVec->vec, sizeof(double)*cols);
	rbfValueMatrixSingleType (x, udptr->allPtsMatrix, udptr->coefficients, constr, udptr->rbfType[0], udptr->rbfParam[0], udptr->rbfVector);
	offset += udptr->coefficients->cols;

	//Distance constraints for hidden and NRSK infeasible points.
	if (udptr->numHiddenConstrPts > 0 || udptr->numNQInfeasiblePts > 0){
		int k = 0;
		for (i=0; i<udptr->totalPts; i++) {
			if (udptr->pointType[i] == 'H' || udptr->pointType[i] == 'S'){
				constr[k+offset] = udptr->distVec->vec[i] - udptr->deltaSqr;
				k++;
			}
		}
		offset += k;
	}

	//Apriori constraints
	if (udptr->numAprioriConstraints > 0){
		aprioriConstrEval (udptr->customAprioriConstr, x, udptr->dimension, constr, udptr->aprioriConstrType, udptr->numAprioriConstraints, offset, udptr->aprioriInputPtFile, udptr->aprioriExec, udptr->aprioriOutputPtFile, -udptr->constrPrecision);
		offset += udptr->numAprioriConstraints;
	}
	constr[offset] = 1;
	if (udptr->useKNN == true){
		constr[offset] = knnDecision (x, udptr->allPtsMatrix, udptr->distVec, udptr->pointType, udptr->numFeasiblePts, udptr->numInfeasiblePts, udptr->numNQInfeasiblePts, udptr->numUnrelaxablePts, udptr->numHiddenConstrPts, udptr->knnParam);
	}
	constrCorrection (m, constr, udptr->constrPrecision);
}

void findFeasiblePtSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = 1;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = true;
	bool maxDistFromFeasible = false;
	bool computeObj = false;
	double delta = 2*inputParam->stepLength;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("findFeasiblePtFromRBF: rbf input data not sufficient\n");
		return;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension+1, numOutputs);
	vectorD *f = malloc (sizeof(vectorD));
	vectorD *lb = malloc (sizeof(vectorD));
	vectorInit (lb, inputParam->dimension+1);
	vectorD *ub = malloc (sizeof(vectorD));
	vectorInit (ub, inputParam->dimension+1);
	if (lbInput == NULL){
		memcpy (lb->vec, inputParam->lb->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	else {
		memcpy (lb->vec, lbInput->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	if (ubInput == NULL){
		memcpy (ub->vec, inputParam->ub->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	else {
		memcpy (ub->vec, ubInput->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	lb->vec[inputParam->dimension] = 0;
	ub->vec[inputParam->dimension] = 1e15;
	vectorInit (f, 1);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.useInputPts = false;
	diffEvol.dimension = inputParam->dimension+1;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + 2*inputParam->dimension + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	diffEvol.numOutputs = numOutputs;
	diffEvol.lb = lb;
	diffEvol.ub = ub;
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * (inputParam->dimension+1);
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	void (*funcPtr)(double *, double *, double *, int, int, void*)  = &fPtrFeasiblePtOpt;
	void *udPtr = ud;
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtr, udPtr);
	double *x = malloc (sizeof(double)*diffEvol.dimension);
	memcpy (x, A->mat, sizeof(double)*A->rows);
	double *constr = malloc (sizeof(double)*diffEvol.numConstraints);
	double obj = -DBL_MAX;
	fPtrFeasiblePtOpt (x, &obj, constr, diffEvol.dimension, diffEvol.numConstraints, udPtr);
	bool solInf = false;
	int i;
	for (i=0; i<diffEvol.numConstraints; i++){
		if (constr[i] < 0){
			solInf = true;
			break;
		}
	}
	free (x);
	free (constr);
	if (solInf == false){
		solution->optimizationSuccessFul = true;
	}
	memcpy (solution->optimSolutionPt->vec, A->mat, sizeof(double)*(inputParam->dimension));
	free (feasibilityIndex);
	freeVectorD (lb);
	freeVectorD (ub);
	freeVectorD (f);
	freeMatrixD (A);
	freeUserData (ud);
}

void maxDistFeasibleSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = 1;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = true;
	bool computeObj = false;
	double delta = 2*inputParam->stepLength;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("maxDistFromFeasible: rbf input data not sufficient\n");
		return;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension+1, numOutputs);
	vectorD *lb = malloc (sizeof(vectorD));
	vectorInit (lb, inputParam->dimension+1);
	vectorD *ub = malloc (sizeof(vectorD));
	vectorInit (ub, inputParam->dimension+1);
	if (lbInput == NULL){
		memcpy (lb->vec, inputParam->lb->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	else {
		memcpy (lb->vec, lbInput->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	if (ubInput == NULL){
		memcpy (ub->vec, inputParam->ub->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	else {
		memcpy (ub->vec, ubInput->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	lb->vec[inputParam->dimension] = 0;
	ub->vec[inputParam->dimension] = 1e15;

	vectorD *f = malloc (sizeof(vectorD));
	vectorInit (f, numOutputs);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.useInputPts = false;
	diffEvol.dimension = inputParam->dimension+1;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->totalPts + 1;
	diffEvol.numOutputs = numOutputs;
	diffEvol.lb = lb;
	diffEvol.ub = ub;
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * (inputParam->dimension+1);
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	void (*funcPtr)(double *, double *, double *, int, int, void*)  = &fPtrMaxDistFeasibleOpt;
	void *udPtr = ud;
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtr, udPtr);
	double *x = malloc (sizeof(double)*diffEvol.dimension);
	double *constr = malloc (sizeof(double)*diffEvol.numConstraints);
	double obj = -DBL_MAX;
	memcpy (x, A->mat, sizeof(double)*A->rows);
	fPtrMaxDistFeasibleOpt (x, &obj, constr, diffEvol.dimension, diffEvol.numConstraints, udPtr);
	bool solInf = false;
	int i;
	for (i=0; i<diffEvol.numConstraints; i++){
		if (constr[i] < 0){
			solInf = true;
			break;
		}
	}
	free (x);
	free (constr);

	if (solInf == false){
		solution->optimizationSuccessFul = true;
	}
	memcpy (solution->optimSolutionPt->vec, A->mat, sizeof(double)*(inputParam->dimension));
	solution->minMaxDistance = -f->vec[0];
	free (feasibilityIndex);
	freeVectorD (lb);
	freeVectorD (ub);
	freeVectorD (f);
	freeMatrixD (A);
	freeUserData (ud);
}

void globalPtSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = 1;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = true;
	bool computeObj = true; //should be true to compute obj coeff for global opt.
	double delta = 1.0;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("globalPtAutoDeltaFromRBF: rbf input data not sufficient\n");
		return;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension+1, numOutputs);
	vectorD *lb = malloc (sizeof(vectorD));
	vectorInit (lb, inputParam->dimension+1);
	vectorD *ub = malloc (sizeof(vectorD));
	vectorInit (ub, inputParam->dimension+1);
	if (lbInput == NULL){
		memcpy (lb->vec, inputParam->lb->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	else {
		memcpy (lb->vec, lbInput->vec, sizeof(double)*(inputParam->lb->dimension));
	}
	if (ubInput == NULL){
		memcpy (ub->vec, inputParam->ub->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	else {
		memcpy (ub->vec, ubInput->vec, sizeof(double)*(inputParam->ub->dimension));
	}
	lb->vec[inputParam->dimension] = 0;
	ub->vec[inputParam->dimension] = 1e15;

	vectorD *f = malloc (sizeof(vectorD));
	vectorInit (f, numOutputs);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.useInputPts = false;
	diffEvol.dimension = inputParam->dimension+1;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->totalPts + 1;
	diffEvol.numOutputs = numOutputs;
	diffEvol.lb = lb;
	diffEvol.ub = ub;
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * (inputParam->dimension+1);
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	void (*funcPtrMaxDist)(double *, double *, double *, int, int, void*)  = &fPtrMaxDistFeasibleOpt;
	void *udPtr = ud;
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtrMaxDist, udPtr);
	solution->optimizationSuccessFul = true;
	memcpy (solution->optimSolutionPt->vec, A->mat, sizeof(double)*(inputParam->dimension));
	solution->minMaxDistance = -f->vec[0];
	freeVectorD (lb);
	freeVectorD (ub);
	freeMatrixD (A);
	int i;
	double maxBoxLength = 0;
	double minBoxLength = DBL_MAX;
	for (i=0; i<inputParam->dimension; i++){
		if (maxBoxLength < (inputParam->ub->vec[i] - inputParam->lb->vec[i])){
			maxBoxLength = (inputParam->ub->vec[i] - inputParam->lb->vec[i]);
		}
		if (minBoxLength > (inputParam->ub->vec[i] - inputParam->lb->vec[i])){
			minBoxLength = (inputParam->ub->vec[i] - inputParam->lb->vec[i]);
		}
	}
	if (solution->minMaxDistance > maxBoxLength){
		solution->minMaxDistance = MIN (minBoxLength, 1.0);
	}
	ud->delta = solution->minMaxDistance;
	if (ud->delta < inputParam->stepLength){
		ud->delta = inputParam->stepLength;
	}

	ud->maxDistFromFeasible = false;
	matrixD *Ag = malloc (sizeof(matrixD));
	matrixInit (Ag, inputParam->dimension, numOutputs);
	diffEvol.dimension = inputParam->dimension;
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->totalPts + 1;
	diffEvol.numOutputs = numOutputs;
	diffEvol.lb = inputParam->lb;
	diffEvol.ub = inputParam->ub;
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * inputParam->dimension;
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	void (*funcPtrGlobal)(double *, double *, double *, int, int, void*)  = &fPtrGlobalOpt;
	bool repeatStatus = true;
	int numIters = 0;
	int maxIters = 2;
	bool updateDiff = false;
	do {
		differentialEvolution (&diffEvol, Ag, f, feasibilityIndex, funcPtrGlobal, udPtr);
		vecExtractMat (solution->optimSolutionPt, Ag, 0);
		repeatStatus = checkDuplicity (inputParam, solution, solution->optimSolutionPt, updateDiff);
		ud->delta = MIN (1.0, ud->delta/4.0);
		numIters++;
	}while (repeatStatus == true && ud->delta > inputParam->deltaMin && numIters < maxIters);
	if (repeatStatus == true){
		solution->optimizationSuccessFul = false;
	}
	else {
		solution->optimizationSuccessFul = true;
	}
	free (feasibilityIndex);
	freeVectorD (f);
	freeMatrixD (Ag);
	freeUserData (ud);
}

void localOptSurr (inputData *inputParam, solutionStruct *solution, vectorD *lbInput, vectorD *ubInput)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = 1;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = false;
	bool computeObj = true;
	double delta = 2*inputParam->stepLength;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("optSurr: rbf input data not sufficient\n");
		return;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension, numOutputs);
	vectorD *f = malloc (sizeof(vectorD));
	vectorInit (f, numOutputs);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	memset (feasibilityIndex, 'I', sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.dimension = inputParam->dimension;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	diffEvol.numOutputs = numOutputs;
	if (lbInput == NULL){
		diffEvol.lb = inputParam->lb;
	}
	else {
		diffEvol.lb = lbInput;
	}
	if (ubInput == NULL){
		diffEvol.ub = inputParam->ub;
	}
	else {
		diffEvol.ub = ubInput;
	}
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * inputParam->dimension;
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	diffEvol.useInputPts = true;
	void (*funcPtr)(double *, double *, double *, int, int, void*)  = &fPtrMultiStartOpt;
	void *udPtr = ud;
	int i;
	if (diffEvol.useInputPts == true){
		int numFPts = MIN (5, solution->numFeasiblePts);
		diffEvol.inputMat = malloc (sizeof(matrixD));
		matrixInit (diffEvol.inputMat, inputParam->dimension, numFPts);
		pickBestFeasiblePts (diffEvol.inputMat, inputParam, solution);
		diffEvol.inputFuncVal = malloc (sizeof(vectorD));
		vectorInit (diffEvol.inputFuncVal, diffEvol.inputMat->cols);
		diffEvol.inputConstrMat = malloc (sizeof(matrixD));
		matrixInit (diffEvol.inputConstrMat, diffEvol.numConstraints, diffEvol.inputMat->cols);
		double *constr = malloc (sizeof (double) * diffEvol.numConstraints);
		vectorD *tmpVector = malloc (sizeof(vectorD));
		vectorInit (tmpVector, diffEvol.inputMat->rows);
		for (i=0; i<diffEvol.inputMat->cols; i++){
			vecExtractMat (tmpVector, diffEvol.inputMat, i);
			fPtrMultiStartOpt (tmpVector->vec, &diffEvol.inputFuncVal->vec[i], constr, diffEvol.dimension, diffEvol.numConstraints, ud);
			memcpy (diffEvol.inputConstrMat->mat+i*diffEvol.numConstraints, constr, sizeof(double)*diffEvol.numConstraints);
		}
		free (constr);
		freeVectorD (tmpVector);
	}
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtr, udPtr);
	if (diffEvol.useInputPts == true){
		freeMatrixD (diffEvol.inputMat);
		freeMatrixD (diffEvol.inputConstrMat);
		freeVectorD (diffEvol.inputFuncVal);
	}
	vecExtractMat (solution->optimSolutionPt, A, 0);
	free (feasibilityIndex);
	freeVectorD (f);
	freeMatrixD (A);
	freeUserData (ud);
}

void multiStartSurr (inputData *inputParam, solutionStruct *solution, matrixD *outputMat, vectorD *objValues, char *uniqueStatus, int *numPts, double delta, vectorD *lbInput, vectorD *ubInput)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = inputParam->numMultiStartPts;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = false;
	bool computeObj = true;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("multiStartFromRBF: rbf input data not sufficient\n");
		return;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension, numOutputs);
	vectorD *f = malloc (sizeof(vectorD));
	vectorInit (f, numOutputs);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	memset (feasibilityIndex, 'I', sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.dimension = inputParam->dimension;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	diffEvol.numOutputs = numOutputs;
	if (lbInput == NULL){
		diffEvol.lb = inputParam->lb;
	}
	else {
		diffEvol.lb = lbInput;
	}
	if (ubInput == NULL){
		diffEvol.ub = inputParam->ub;
	}
	else {
		diffEvol.ub = ubInput;
	}
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * inputParam->dimension;
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	diffEvol.useInputPts = true;
	void (*funcPtr)(double *, double *, double *, int, int, void*)  = &fPtrMultiStartOpt;
	void *udPtr = ud;
	int i;
	if (diffEvol.useInputPts == true){
		int numFPts = MIN (5, solution->numFeasiblePts);
		diffEvol.inputMat = malloc (sizeof(matrixD));
		matrixInit (diffEvol.inputMat, inputParam->dimension, numFPts);
		pickBestFeasiblePts (diffEvol.inputMat, inputParam, solution);
		diffEvol.inputFuncVal = malloc (sizeof(vectorD));
		vectorInit (diffEvol.inputFuncVal, diffEvol.inputMat->cols);
		diffEvol.inputConstrMat = malloc (sizeof(matrixD));
		matrixInit (diffEvol.inputConstrMat, diffEvol.numConstraints, diffEvol.inputMat->cols);
		double *constr = malloc (sizeof (double) * diffEvol.numConstraints);
		vectorD *tmpVector = malloc (sizeof(vectorD));
		vectorInit (tmpVector, diffEvol.inputMat->rows);
		for (i=0; i<diffEvol.inputMat->cols; i++){
			vecExtractMat (tmpVector, diffEvol.inputMat, i);
			fPtrMultiStartOpt (tmpVector->vec, &diffEvol.inputFuncVal->vec[i], constr, diffEvol.dimension, diffEvol.numConstraints, ud);
			memcpy (diffEvol.inputConstrMat->mat+i*diffEvol.numConstraints, constr, sizeof(double)*diffEvol.numConstraints);
		}
		free (constr);
		freeVectorD (tmpVector);
	}
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtr, udPtr);
	if (diffEvol.useInputPts == true){
		freeMatrixD (diffEvol.inputMat);
		freeMatrixD (diffEvol.inputConstrMat);
		freeVectorD (diffEvol.inputFuncVal);
	}
	*numPts = diffEvol.numSortedUniquePts;
	memcpy (outputMat->mat, A->mat, sizeof(double)*(A->rows)*(diffEvol.numSortedUniquePts));
	memcpy (objValues->vec, f->vec, sizeof(double)*(diffEvol.numSortedUniquePts));
	memset (uniqueStatus, 'T', sizeof(char)*(diffEvol.numSortedUniquePts));
	free (feasibilityIndex);
	freeVectorD (f);
	freeMatrixD (A);
	freeUserData (ud);
}

bool ptProjectOnFeasible (inputData *inputParam, solutionStruct *solution, vectorD *x)
{
	solution->optimizationSuccessFul = true;
	int numOutputs = 1;
	userData *ud = malloc (sizeof(userData));
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = false;
	bool computeObj = false;
	double delta = 1.0;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, x, NULL);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("ptProjectOnFeasible: rbf input data not sufficient\n");
		return false;
	}
	matrixD *A = malloc (sizeof(matrixD));
	matrixInit (A, inputParam->dimension, numOutputs);
	vectorD *f = malloc (sizeof(vectorD));
	vectorInit (f, numOutputs);
	char *feasibilityIndex = malloc (sizeof(char)*numOutputs);
	diffEvolStruct diffEvol;
	diffEvol.useInputPts = false;
	diffEvol.dimension = inputParam->dimension;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1];
	int numAprioriConstraints = inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	diffEvol.numConstraints = numConstraints + numAprioriConstraints + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	diffEvol.numOutputs = numOutputs;
	diffEvol.lb = inputParam->lb;
	diffEvol.ub = inputParam->ub;
	diffEvol.maxIterations = 1000;
	diffEvol.populationSize = 20 * inputParam->dimension;
	diffEvol.CR = 0.9;
	diffEvol.F = 0.9;
	diffEvol.numSortedUniquePts = numOutputs;
	void (*funcPtr)(double *, double *, double *, int, int, void*)  = &fPtrPtProject;
	void *udPtr = ud;
	differentialEvolution (&diffEvol, A, f, feasibilityIndex, funcPtr, udPtr);
	solution->optimizationSuccessFul = true;
	double epsilon = 1e-5;
	if (f->vec[0] > 0 && f->vec[0] < inputParam->deltaMin + epsilon){
		int i;
		bool tolViol = false;
		vectorD *direction = malloc (sizeof(vectorD));
		vectorInit (direction, inputParam->dimension);
		for (i=0; i<inputParam->dimension; i++){
			direction->vec[i] = A->mat[i] - x->vec[i];
			if (direction->vec[i] < inputParam->precision){
				tolViol = true;
				break;
			}
		}
		if (tolViol == false){
			vectorNormalize2 (direction);
			for (i=0; i<inputParam->dimension; i++){
				solution->optimSolutionPt->vec[i] = x->vec[i] + (3*inputParam->deltaMin) * direction->vec[i];
				if (solution->optimSolutionPt->vec[i] < inputParam->lb->vec[i]){
					solution->optimSolutionPt->vec[i] = inputParam->lb->vec[i];
				}
				if (solution->optimSolutionPt->vec[i] > inputParam->ub->vec[i]){
					solution->optimSolutionPt->vec[i] = inputParam->ub->vec[i];
				}
			}
		}
		else {
			memcpy (solution->optimSolutionPt->vec, A->mat, sizeof(double)*(inputParam->dimension));
		}
		freeVectorD (direction);
	}
	else {
		memcpy (solution->optimSolutionPt->vec, A->mat, sizeof(double)*(inputParam->dimension));
	}
	free (feasibilityIndex);
	freeVectorD (f);
	freeMatrixD (A);
	freeUserData (ud);
	return true;
}

bool randomPtInRadius (inputData *inputParam, solutionStruct *solution, vectorD *randomPt, double radius, bool checkFeasibility)
{
	bool randomPtInRadiusStatus = true;
	int i;
	int dimension = inputParam->dimension;
	matrixD *largePoolMatrix = malloc (sizeof(matrixD));
	int largePoolSize = inputParam->largePoolSize;
	matrixInit (largePoolMatrix, dimension, largePoolSize);
	createNewBounds (inputParam, solution, radius, randomPt);
	generatePts (inputParam, largePoolSize, solution->lbScaled, solution->ubScaled, largePoolMatrix);
	vectorD *inputPt = malloc (sizeof(vectorD));
	vectorInit (inputPt, dimension);
	bool repeatedPt = true;

	userData *ud = NULL;
	double *constr = NULL;
	double epsilon = 1e-5;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1] + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	numConstraints += inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	if (checkFeasibility == true){
		ud = malloc (sizeof(userData));
		constr = malloc (sizeof(double)*numConstraints);
		bool feasibilitySearch = false;
		bool maxDistFromFeasible = false;
		bool computeObj = false;
		double delta = 1.0;
		bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, NULL);
		if (status == false){
			solution->optimizationSuccessFul = false;
			printf ("randomPtInRadius: rbf input data not sufficient\n");
			return false;
		}
	}
	bool storeNonRepeatPt = true; //just to store first non repeated pt in case feasibility fails for all pts.
	i=0;
	while (repeatedPt == true && i<largePoolSize){
		vecExtractMat (inputPt, largePoolMatrix, i);
		bool updateDiff = false;
		repeatedPt = checkDuplicity (inputParam, solution, inputPt, updateDiff);
		if (repeatedPt == false){
			if (checkFeasibility == true){
				if (storeNonRepeatPt == true){
					memcpy (randomPt->vec, inputPt->vec, sizeof(double)*dimension);
					storeNonRepeatPt = false;
				}
				fPtrFeasTest (inputPt->vec, constr, dimension, numConstraints, ud);
				bool feasibility = true;
				int j = 0;
				while (j<numConstraints){
					if (constr[j] < -epsilon){
						feasibility = false;
						break;
					}
					j++;
				}
				if (feasibility == true){
					memcpy (randomPt->vec, inputPt->vec, sizeof(double)*dimension);
					randomPtInRadiusStatus = true;
				}
				else {
					repeatedPt = true;
				}
			}
			else {
				memcpy (randomPt->vec, inputPt->vec, sizeof(double)*dimension);
				randomPtInRadiusStatus = true;
			}
		}
		i++;
	}
	if (checkFeasibility == true){
		free (constr);
		freeUserData (ud);
	}
	freeVectorD (inputPt);
	freeMatrixD (largePoolMatrix);
	solution->optimizationSuccessFul = true;
	return randomPtInRadiusStatus;
}

bool randomMatrixInRadius (inputData *inputParam, solutionStruct *solution, vectorD *randomPt, matrixD *randomMat, int numRandomPts, double radius, char *fullRank)
{
	if (randomMat == NULL && numRandomPts > 1){
		printf ("ERROR: randomMatrixInRadiusStatus: randomMat cannot be empty\n");
		exit (EXIT_FAILURE);
	}
	bool randomPtInRadiusStatus = true;
	int i;
	int dimension = inputParam->dimension;
	if (radius >= 0){
		createNewBounds (inputParam, solution, radius, randomPt);
	}
	else {
		memcpy (solution->lbScaled->vec, inputParam->lb->vec, sizeof(double)*dimension);
		memcpy (solution->ubScaled->vec, inputParam->ub->vec, sizeof(double)*dimension);
	}

	double epsilon = 1e-5;
	int numConstraints = inputParam->numTypeConstraints[inputParam->indexQRSK-1] + inputParam->numTypeConstraints[inputParam->indexQUSK-1] + solution->numHiddenConstrPts + solution->numNQInfeasiblePts + 1;
	numConstraints += inputParam->numTypeConstraints[inputParam->indexQRAK-1] + inputParam->numTypeConstraints[inputParam->indexNRAK-1] + inputParam->numTypeConstraints[inputParam->indexQUAK-1] + inputParam->numTypeConstraints[inputParam->indexNUAK-1];
	userData *ud = malloc (sizeof(userData));
	double *constr = malloc (sizeof(double)*numConstraints);
	bool feasibilitySearch = false;
	bool maxDistFromFeasible = false;
	bool computeObj = false;
	double delta = 1.0;
	bool status = userDataInit (inputParam, solution, ud, feasibilitySearch, maxDistFromFeasible, computeObj, delta, NULL, fullRank);
	if (status == false){
		solution->optimizationSuccessFul = false;
		printf ("randomPtInRadius: rbf input data not sufficient\n");
		return false;
	}

	matrixD *largePoolMatrix = malloc (sizeof(matrixD));
	int largePoolSize = numRandomPts;
	matrixInit (largePoolMatrix, dimension, largePoolSize);
	vectorD *inputPt = malloc (sizeof(vectorD));
	vectorInit (inputPt, dimension);

	int numPts = 0;
	int maxIters = 100;
	int iters = 0;
	while (numPts < numRandomPts && iters < maxIters){
		generatePts (inputParam, largePoolSize, solution->lbScaled, solution->ubScaled, largePoolMatrix);
		i = 0;
		while (i<largePoolSize && numPts < numRandomPts){
			vecExtractMat (inputPt, largePoolMatrix, i);
			bool repeatedPt = checkDuplicity (inputParam, solution, inputPt, false);
			if (repeatedPt == false){
				fPtrFeasTest (inputPt->vec, constr, dimension, numConstraints, ud);
				bool feasibility = true;
				int j = 0;
				while (j<numConstraints){
					if (constr[j] < -epsilon){
						feasibility = false;
						break;
					}
					j++;
				}
				if (feasibility == true){
					memcpy (randomMat->mat+numPts*dimension, inputPt->vec, sizeof(double)*dimension);
					numPts++;
				}
			}
			i++;
		}
		iters++;
	}
	free (constr);
	freeUserData (ud);
	freeVectorD (inputPt);
	freeMatrixD (largePoolMatrix);
	solution->optimizationSuccessFul = true;
	if (numPts < numRandomPts){
		randomPtInRadiusStatus = false;
	}
	return randomPtInRadiusStatus;
}	

double knnDecision (double *x, matrixD *ptsMat, vectorD *distVec, char *pointType, int numFeasiblePts, int numInfeasiblePts, int numNQInfeasiblePts, int numUnrelaxablePts, int numHiddenPts, int knnParam)
{
	double val = 1;
	if ((numFeasiblePts > 2 || numInfeasiblePts > 2) && (numNQInfeasiblePts > 2 || numHiddenPts > 2 || numUnrelaxablePts > 2)){
		int numClasses = 5;
		char *classTypes = malloc (sizeof(char)*numClasses);
		int *classifier = calloc (numClasses, sizeof(int));
		classTypes[0] = 'F';
		classTypes[1] = 'I';
		classTypes[2] = 'S';
		classTypes[3] = 'U';
		classTypes[4] = 'H';
		int ptClassVal = knn (ptsMat, x, distVec, pointType, classTypes, numClasses, classifier, knnParam);
		if (numNQInfeasiblePts > 2 && ptClassVal == 2){ 
			val = -1;
		}
		if (numUnrelaxablePts > 2 && ptClassVal == 3){ 
			val = -10;
		}
		if (numHiddenPts > 2 && ptClassVal == 4){ 
			val = -100;
		}
		free (classTypes);
		free (classifier);
	}
	return val;
}

void pickBestFeasiblePts (matrixD *inputMat, inputData *inputParam, solutionStruct *solution)
{
	int dimension = inputParam->dimension;
	memcpy (inputMat->mat, solution->allPtsMatrix->mat + (solution->bestPtIndex * dimension), sizeof(double)*dimension);
	int numPts = inputMat->cols;
	int i, k = 1;
	for (i=0; i<solution->bestPtIndex; i++){
		if (solution->pointType[i] == 'F'){
			memcpy (inputMat->mat+k*dimension, solution->allPtsMatrix->mat+(i*dimension), sizeof(double)*dimension);
			k++;
		}
		if (k == numPts){
			break;
		}
	}
	if (k < numPts){
		for (i=solution->bestPtIndex+1; i<solution->totalPts; i++){
			if (solution->pointType[i] == 'F'){
				memcpy (inputMat->mat+k*dimension, solution->allPtsMatrix->mat+(i*dimension), sizeof(double)*dimension);
				k++;
			}
			if (k == numPts){
				break;
			}
		}
	}
}

void freeUserData (userData *ud)
{
	if (ud->computeObj == true){
		freeVectorD (ud->objectiveCoeff);
	}
	free (ud->rbfType);
	free (ud->rbfParam);
	if (ud->rbfVector != NULL){
		freeVectorD (ud->rbfVector);
	}
	if (ud->distVec != NULL){
		freeVectorD (ud->distVec);
	}
	if (ud->singleRBFtype == false || ud->rbfMatrix != NULL){
		freeMatrixD (ud->rbfMatrix);
	}
	freeMatrixD (ud->allPtsMatrix);
	freeMatrixD (ud->coefficients);
	if (ud->feasibilitySearch == true){
		freeMatrixD (ud->negativeidentityAppendedOnes);
		freeMatrixD (ud->identityAppendedNegativeOnes);
	}
	if (ud->numAprioriConstraints > 0){
		free (ud->aprioriConstrType);
	}
	free (ud);
}

