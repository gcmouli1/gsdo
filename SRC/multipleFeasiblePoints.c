/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "multipleFeasiblePoints.h"

void multipleFeasiblePts (inputData* inputParam, solutionStruct *solution, int maxInitialFeasiblePoints)
{
	int dimension = inputParam->dimension;
	vectorD *feasiblePt = malloc (sizeof(vectorD));
	vectorInit (feasiblePt, dimension);
	int lastFeasibleIndex = solution->totalPts - 1;
	while (solution->pointType[lastFeasibleIndex] != 'F' && lastFeasibleIndex >= 0){
		lastFeasibleIndex -= 1;
	}
	vecExtractMat (feasiblePt, solution->allPtsMatrix, lastFeasibleIndex);

	int numComputedPts = 0;
	int numStartComputedPts = solution->totalPts;
	int iterations = 0;
	int maxIters = 1 * (dimension + 1);

	while (numComputedPts < inputParam->maxAllowedEvalsStage2 && solution->totalPts < inputParam->maxAllowedEvals && iterations < inputParam->maxMFIterations){
		maxDistFeasibleSurr (inputParam, solution, NULL, NULL);
		if (solution->optimizationSuccessFul == true){
			if (solution->stepType != NULL){
				free (solution->stepType);
			}
			solution->stepType = strdup("Multiple_Feasible(RBF)");
			updateSolution (inputParam, solution, solution->optimSolutionPt);
		}
		char ptType = solution->pointType[solution->inputPtIndex];
		int localRBFSearchStatus = 1;
		if ( ptType == 'H' || ptType == 'S' || ptType == 'U'){
			maxIters = inputParam->maxAllowedEvalsStage2 - numComputedPts;
			localRBFSearchStatus = localRBFSearch (inputParam, solution, feasiblePt, lastFeasibleIndex, maxIters, 'M');
		}
		

		if (solution->totalPts < inputParam->maxAllowedEvals && (solution->optimizationSuccessFul == false || solution->repeatPoint == true || localRBFSearchStatus == -1)){
			ptType = solution->pointType[solution->inputPtIndex];
			if (ptType == 'H' || ptType == 'S' || ptType == 'U'){
				if (solution->stepType != NULL){
					free (solution->stepType);
				}
				solution->stepType = strdup("Multiple_Feasible(RBF)");
				maxIters = inputParam->maxAllowedEvalsStage2 - numComputedPts;
				localRBFSearch (inputParam, solution, feasiblePt, lastFeasibleIndex, maxIters, 'M');
			}
			else {
				vecExtractMat (feasiblePt, solution->allPtsMatrix, lastFeasibleIndex);
				double radius = inputParam->stepLength;
				bool checkFeasibility = false;
				randomPtInRadius (inputParam, solution, feasiblePt, radius, checkFeasibility);
				if (solution->stepType != NULL){
					free (solution->stepType);
				}
				solution->stepType = strdup("Multiple_Feasible(Rand)");
				updateSolution (inputParam, solution, feasiblePt);
			}
		}
		iterations++;
		if (solution->numFeasiblePts >= maxInitialFeasiblePoints){
			break;
		}
		numComputedPts = solution->totalPts - numStartComputedPts;
	}
	freeVectorD (feasiblePt);
}
