/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "inputParameters.h"

int countWords (char *line)
{
	int i = 0, count = 0;
	while (line[i] != '\0'){
		if (line[i] == ' ' && line[i+1] != ' '){
			count++;
		}
		i++;
	}
	count++;
	return count;
}

void inputParametersInit (inputData *inputParam, char **argv)
{
	int i,j;
	int numLB = 0;
	int numUB = 0;
	int numRBFTypeForConstr = 0;
	int numRBFParamForConstr = 0;
	int numConstrInput = 0;

	inputParam->knnParam = 5;
	inputParam->numRBFTypes = 4;
	inputParam->numStartTestPts = 0;
	inputParam->objectiveIndex = -1;
	inputParam->autoSelectModel = false;
	inputParam->singleRBFtype = true;
	inputParam->rbfTypeSingle = 'C';
	inputParam->rbfParamSingle = 0;
	inputParam->includePolyTail = true;
	inputParam->numLeaveOuts = 5;
	inputParam->constraintIneqSign = 'G';
	inputParam->deltaTerminateGlobal = 1E-5;
	inputParam->precision = 1E-10;
	inputParam->loadFromFile = false;
	inputParam->numPointsToLoad = 0;
	inputParam->debugLogLevel = 1;
	inputParam->minGlobalItersSatisfied = 0;
	inputParam->OutputLevel = 2;
	inputParam->randomGeneration = true;
	inputParam->NUSH = false;
	inputParam->numConstraintTypes = 10;
	inputParam->numAprioriConstraints = 0;
	inputParam->rbfTypeObj = 'C';
	inputParam->rbfParamObj = 0;
	inputParam-> udObjConstr = false;
	inputParam->udCustomAprioriConstr = false;
	bool executableExists = false;
	bool aprioriExecutableExists = false;
	bool inputPtFileNameAndPathExists = false;
	bool outputPtFileNameAndPathExists = false;
	strcpy (inputParam->ptGenerationApproach,"RANDOM");
	strcpy (inputParam->outputFolderPath, ".");
	int randomSeed = 0;
	FILE *fpArgs;
	fpArgs=fopen(argv[1],"r");
	if (fpArgs == NULL){
		printf ("paramter file not found\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

	int optionSize = 1024;
	char option[optionSize];
	char optionName[1024];
	char optionValue[1024];
	while (fgets (option, optionSize,fpArgs) != NULL) {
		sscanf (option,"%s %[^\n]\n",optionName,optionValue);
		if (optionValue[0] == '#'){
			continue;
		}
		else if (strcasecmp(optionName,"Dimension") == 0) {
			inputParam->dimension = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"NumConstraints") == 0) {
			inputParam->numConstraints = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"ConstraintIneqSign") == 0) {
			inputParam->constraintIneqSign = optionValue[0];
		}
		else if (strcasecmp(optionName,"GlobalPrecision") == 0) {
			inputParam->deltaTerminateGlobal = atof (optionValue);
		}
		else if (strcasecmp(optionName,"AutoSelectModel") == 0) {
			inputParam->autoSelectModel = atoi (optionValue);
		}
		else if (strcasecmp(optionName,"ObjectiveIndex") == 0) {
			inputParam->objectiveIndex = atoi (optionValue) - 1;
		}
		else if (strcasecmp(optionName,"SingleRBFModel") == 0) {
			inputParam->singleRBFtype = atoi (optionValue);
		}
		else if (strcasecmp(optionName,"SingleRBFType") == 0) {
			inputParam->rbfTypeSingle = optionValue[0];
		}
		else if (strcasecmp(optionName,"SingleRBFParam") == 0) {
			inputParam->rbfParamSingle = atof (optionValue);
		}
		else if (strcasecmp (optionName,"RBFTypeObj") == 0){
			inputParam->rbfTypeObj = optionValue[0];
		}
		else if (strcasecmp (optionName,"RBFParamObj") == 0){
			inputParam->rbfParamObj = atof (optionValue);
		}
		else if (strcasecmp(optionName,"IncludePolynomialTail") == 0) {
			inputParam->includePolyTail = atoi (optionValue);
		}
		else if (strcasecmp(optionName,"MaxEvals") == 0) {
			inputParam->maxAllowedEvals = atof(optionValue);
		}
		else if (strcasecmp(optionName,"PointGenerationApproach") == 0) {
			strcpy (inputParam->ptGenerationApproach, optionValue);
		}
		else if (strcasecmp(optionName,"CustomObjConstrFunction") == 0) {
			inputParam->udObjConstr = atoi (optionValue);
		}
		else if (strcasecmp(optionName,"Executable") == 0) {
			executableExists = true;
			strcpy (inputParam->dfoExec, optionValue);
		}
		else if (strcasecmp(optionName,"ExecutableInputPtPath") == 0) {
			inputPtFileNameAndPathExists = true;
			strcpy (inputParam->inputPtFileNameAndPath, optionValue);
		}
		else if (strcasecmp(optionName,"ExecutableOutputPtPath") == 0) {
			outputPtFileNameAndPathExists = true;
			strcpy (inputParam->outputPtFileNameAndPath, optionValue);
		}
		else if (strcasecmp(optionName,"CustomAprioriConstrFunction") == 0) {
			inputParam->udCustomAprioriConstr = atoi (optionValue);
		}
		else if (strcasecmp(optionName,"AprioriExecutable") == 0) {
			aprioriExecutableExists = true;
			strcpy (inputParam->aprioriExec, optionValue);
		}
		else if (strcasecmp(optionName,"AprioriExecutableInputPtPath") == 0) {
			strcpy (inputParam->aprioriInputPtFile, optionValue);
		}
		else if (strcasecmp(optionName,"AprioriExecutableOutputPtPath") == 0) {
			strcpy (inputParam->aprioriOutputPtFile, optionValue);
		}
		else if (strcasecmp(optionName,"OutputFolder") == 0) {
			strcpy (inputParam->outputFolderPath, optionValue);
		}
		else if (strcasecmp(optionName,"OutputLevel") == 0) {
			inputParam->OutputLevel = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"LoadFromFile") == 0) {
			inputParam->loadFromFile = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"NumPointsToLoad") == 0) {
			inputParam->numPointsToLoad = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"RandomSeedHalton") == 0) {
			randomSeed = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"RandomSeed") == 0) {
			randomSeed = atoi(optionValue);
		}
		else if (strcasecmp(optionName,"HiddenConstraint") == 0) {
			int tmp = atoi(optionValue);
			if (tmp == 1){
				inputParam->NUSH = true;
			}
			else if (tmp == 0){
				inputParam->NUSH = false;
			}
			else{
				printf ("Invalid input for hidden constraint. Must be 0 or 1. Assuming hidden constraint is absent");
			}
		}
	}
	fclose (fpArgs);

	double *lb = NULL;
	double *ub = NULL;
	char *rbfTypeForConstr = NULL;
	double *rbfParamForConstr = NULL;
	int *constr = NULL;
	char *param = NULL;
	char delimiter[2] = " ";
	fpArgs = fopen(argv[1],"r");
	while (fgets (option, optionSize,fpArgs) != NULL){
		int t = 0;
		while (option[t] == ' '){
			t++;
		}
		if (option[t] == '#'){
			continue;
		}
		int numWords = countWords (option);
		char *linePtr = option;
		param = strtok_r (option, delimiter, &linePtr);

		if ((param != NULL) && (strcasecmp (param, "LB") == 0)){
			numLB = numWords - 1;
			lb = malloc (sizeof(double)*numLB);
			i = 0;
			while (param != NULL){
				if (i > 0){
					lb[i-1] = atof (param);
				}
				i++;
				param = strtok_r(NULL, delimiter, &linePtr);
			}
		}
		else if ((param != NULL) && (strcasecmp (param, "UB") == 0)){
			numUB = numWords - 1;
			ub = malloc (sizeof(double)*numUB);
			i = 0;
			while (param != NULL){
				if (i > 0){
					ub[i-1] = atof (param);
				}
				i++;
				param = strtok_r(NULL, delimiter, &linePtr);
			}
		}
		else if ((param != NULL) && (strcasecmp (param, "RBFType") == 0)){
			numRBFTypeForConstr = numWords - 1;
			rbfTypeForConstr = malloc (sizeof(char)*numRBFTypeForConstr);
			i = 0;
			while (param != NULL){
				if (i > 0){
					rbfTypeForConstr[i-1] =  param[0];
				}
				i++;
				param = strtok_r(NULL, delimiter, &linePtr);
			}
		}
		else if ((param != NULL) && (strcasecmp (param, "RBFParam") == 0)){
			numRBFParamForConstr = numWords - 1;
			rbfParamForConstr = malloc (sizeof(double)*numRBFParamForConstr);
			i = 0;
			while (param != NULL){
				if (i > 0){
					rbfParamForConstr[i-1] = atof (param);
				}
				i++;
				param = strtok_r(NULL, delimiter, &linePtr);
			}
		}
		else if ((param != NULL) && (strcasecmp (param, "ConstraintType") == 0)){
			numConstrInput = numWords - 1;
			constr = malloc (sizeof(int)*numConstrInput);
			i = 0;
			while (param != NULL){
				if (i > 0){
					constr[i-1] = atoi (param);
				}
				i++;
				param = strtok_r(NULL, delimiter, &linePtr);
			}
		}
	}
	fclose (fpArgs);

	if (inputParam->dimension == 0){
		printf ("ERROR: inputParametersInit: unable to read dimension\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (inputParam->udObjConstr == false && executableExists == false){
		printf ("ERROR: inputParametersInit: Executable is not provided\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (inputParam->udObjConstr == false && inputPtFileNameAndPathExists == false){
		printf ("ERROR: inputParametersInit: file path for input point to executable is not provided\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (inputParam->udObjConstr == false && outputPtFileNameAndPathExists == false){
		printf ("ERROR: inputParametersInit: file path for output point to executable is not provided\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

	if (numLB < inputParam->dimension){
		printf ("ERROR: inputParametersInit: Data missing for lower bound\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numLB > inputParam->dimension){
		printf ("WARNING: inputParametersInit: Truncating extra data for lower bound\n");
	}
	inputParam->lb = malloc (sizeof(vectorD));
	vectorInit (inputParam->lb, inputParam->dimension);
	memcpy (inputParam->lb->vec, lb, sizeof(double)*inputParam->dimension);
	free (lb);

	if (numUB < inputParam->dimension){
		printf ("ERROR: inputParametersInit: Data missing for upper bound\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numUB > inputParam->dimension){
		printf ("WARNING: inputParametersInit: Truncating extra data for upper bound\n");
	}
	inputParam->ub = malloc (sizeof(vectorD));
	vectorInit (inputParam->ub, inputParam->dimension);
	memcpy (inputParam->ub->vec, ub, sizeof(double)*inputParam->dimension);
	free (ub);

	if (inputParam->autoSelectModel == true){
		inputParam->singleRBFtype = false;
	}
	inputParam->rbfType = malloc (sizeof(char) * inputParam->numConstraints);
	inputParam->rbfParam = malloc (sizeof(double) * inputParam->numConstraints);
	if (inputParam->singleRBFtype == false){
		if (numRBFTypeForConstr < inputParam->numConstraints){
			printf ("ERROR: inputParametersInit: Number of RBFType should be equal to number of constraints\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		if (numRBFTypeForConstr > inputParam->numConstraints){
			printf ("WARNING: inputParametersInit: Truncating extra data for RBF types\n");
		}
		memcpy (inputParam->rbfType, rbfTypeForConstr, sizeof(char)*inputParam->numConstraints);

		if (numRBFParamForConstr < inputParam->numConstraints){
			printf ("ERROR: inputParametersInit: Number of RBFParam should be equal to number of constraints\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		if (numRBFParamForConstr > inputParam->numConstraints){
			printf ("WARNING: inputParametersInit: Truncating extra data for RBFParam\n");
		}
		memcpy (inputParam->rbfParam, rbfParamForConstr, sizeof(double)*inputParam->numConstraints);
	}
	else{
		memset (inputParam->rbfType, inputParam->rbfTypeSingle, sizeof(char)*inputParam->numConstraints);
		for (i=0; i<inputParam->numConstraints; i++){
			inputParam->rbfParam[i] = inputParam->rbfParamSingle;
		}
		inputParam->rbfTypeObj = inputParam->rbfTypeSingle;
		inputParam->rbfParamObj = inputParam->rbfParamSingle;
	}
	if (rbfTypeForConstr != NULL){
		free (rbfTypeForConstr);
	}
	if (rbfParamForConstr != NULL){
		free (rbfParamForConstr);
	}

	inputParam->stagnationLength = MAX (7,inputParam->dimension+1);
	if (inputParam->constraintIneqSign != 'G' && inputParam->constraintIneqSign != 'L'){
		printf ("ERROR: Unknown constraint inequality sign\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (inputParam->numStartTestPts == 0){
		inputParam->numStartTestPts = 2*(inputParam->dimension+1);
	}
	inputParam->maxInitialFeasiblePoints = inputParam->dimension+1;

	inputParam->indexQRAK = 1;
	inputParam->indexNRAK = 2;
	inputParam->indexQUAK = 3;
	inputParam->indexNUAK = 4;
	inputParam->indexQRSK = 5;
	inputParam->indexNRSK = 6;
	inputParam->indexQUSK = 7;
	inputParam->indexNUSK = 8;
	inputParam->constraintType = malloc (sizeof(vectorZ));
	inputParam->numTypeConstraints = calloc (inputParam->numConstraintTypes, sizeof(int));
	vectorInitZ (inputParam->constraintType, inputParam->numConstraints);

	for (i=0; i<inputParam->numConstraints; i++){
		inputParam->constraintType->vec[i] = inputParam->indexQRSK;
	}
	inputParam->numTypeConstraints[inputParam->indexQRSK-1] = inputParam->numConstraints;

	if (constr != NULL){
		if (numConstrInput < inputParam->numConstraints){
			printf ("ERROR: inputParametersInit: Number of constraint types should be equal to number of constraints\n");
			printf ("PROCESS TERMINATED\n");
			exit (EXIT_FAILURE);
		}
		if (numConstrInput > inputParam->numConstraints){
			printf ("WARNING: inputParametersInit: Truncating extra data for ConstraintType\n");
		}
		memcpy (inputParam->constraintType->vec, constr, sizeof(int)*inputParam->numConstraints);
		free (constr);

		inputParam->numAprioriConstraints = 0;
		for (i=0; i<inputParam->numConstraints; i++){
			if ((inputParam->constraintType->vec[i] == inputParam->indexQRAK) || (inputParam->constraintType->vec[i] == inputParam->indexNRAK) || (inputParam->constraintType->vec[i] == inputParam->indexQUAK) || (inputParam->constraintType->vec[i] == inputParam->indexNUAK)){
				inputParam->numAprioriConstraints++;
			}
		}
		if (inputParam->numAprioriConstraints > 0 && inputParam->udCustomAprioriConstr == false && aprioriExecutableExists == false){
			printf ("WARNING: inputParametersInit: Replacing apriori constraints with simulation constraints\n");
			for (i=0; i<inputParam->numConstraints; i++){
				if (inputParam->constraintType->vec[i] == inputParam->indexQRAK){
					printf ("Constraint %d: Replacing QRAK with QRSK\n",i+1);
					inputParam->constraintType->vec[i] = inputParam->indexQRSK;
				}
				if (inputParam->constraintType->vec[i] == inputParam->indexNRAK){
					printf ("Constraint %d: Replacing NRAK with NRSK\n",i+1);
					inputParam->constraintType->vec[i] = inputParam->indexNRSK;
				}
				if (inputParam->constraintType->vec[i] == inputParam->indexQUAK){
					printf ("Constraint %d: Replacing QUAK with QUSK\n",i+1);
					inputParam->constraintType->vec[i] = inputParam->indexQUSK;
				}
				if (inputParam->constraintType->vec[i] == inputParam->indexNUAK){
					printf ("Constraint %d: Replacing NUAK with NUSK\n",i+1);
					inputParam->constraintType->vec[i] = inputParam->indexNUSK;
				}
			}
		}

		for (i=0; i<inputParam->numConstraintTypes; i++){
			inputParam->numTypeConstraints[i] = 0;
			for (j=0; j<inputParam->numConstraints; j++){
				if (inputParam->constraintType->vec[j] == i+1){
					inputParam->numTypeConstraints[i]++;
				}
			}
		}
	}

	if (inputParam->objectiveIndex < 0){
		printf ("Objective function index missing.\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}

	inputParam->hiddenConstraintExists = false;
	if (inputParam->NUSH == true 
			|| inputParam->numTypeConstraints[inputParam->indexNUSK-1]  > 0
			|| inputParam->numTypeConstraints[inputParam->indexNUAK-1]  > 0
	   ){
		inputParam->hiddenConstraintExists = true;
	}

	inputParam->stepLength = 100 * sqrt(inputParam->dimension);
	for (i=0; i<inputParam->dimension; i++){
		if (inputParam->stepLength  > (inputParam->ub->vec[i] - inputParam->lb->vec[i])/10.0){
			inputParam->stepLength  = (inputParam->ub->vec[i] - inputParam->lb->vec[i])/10.0;
		}
	}

	inputParam->staticGeneration = false;
	inputParam->randomGeneration = true;
	inputParam->userDefinedPtGeneration = false;

	if (strcasecmp (inputParam->ptGenerationApproach, "STATIC") == 0){
		inputParam->staticGeneration = true;
	}
	else if (strcasecmp (inputParam->ptGenerationApproach, "RANDOM") == 0){
		srand(time(NULL));
		inputParam->randomGeneration = true;
	}
	else if (strcasecmp (inputParam->ptGenerationApproach, "USER") == 0){
		inputParam->userDefinedPtGeneration = true;
	}
	else {
		inputParam->randomGeneration = true;
	}

	inputParam->initialReqGlobalIters = inputParam->dimension+1;
	inputParam->numGlobalOptAttempts = 5;
	inputParam->numGlobalStartPts = 5;
	inputParam->numMultiStartPts = 5;
	inputParam->objOverFeasblePtsOnly = false;
	inputParam->largePoolSize = 1000;
	inputParam->deltaExploitation = 1E-3;
	inputParam->perturbation = 100;
	inputParam->maxMFIterations = 100;
	inputParam->maxAllowedEvalsStage2 = 2*(inputParam->dimension+1);
	inputParam->maxGlobalIterations = 10000;
	inputParam->Inf = 2E19;
	inputParam->constraintPrecision = 1E-5;
	inputParam->userDefinedPtGeneration = false;
	inputParam->numStartPtsInitialFeasOpt = inputParam->dimension+1;
	inputParam->numRestartPts = inputParam->dimension+1;
	inputParam->deltaMin = 1E-5;
	inputParam->maxVicinityPts = 5;
	inputParam->randomSeed = randomSeed;
	if (inputParam->randomSeed == 0){
		inputParam->randomSeed = 1;
	}
}

void freeInputParameters (inputData *inputParam)
{
	freeVectorD (inputParam->lb);
	freeVectorD (inputParam->ub);
	if (inputParam->loadFromFile != 0){
		freeMatrixD (inputParam->inputPoints);
		freeMatrixD (inputParam->inputConstraints);
		freeVectorZ (inputParam->indexFeasibility);
		freeVectorD (inputParam->objValues);
	}
	freeVectorZ (inputParam->constraintType);
	free (inputParam->numTypeConstraints);
	free (inputParam->rbfType);
	free (inputParam->rbfParam);
	free (inputParam);
}
