/*
   This file is part of GSDO

   GSDO is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GSDO is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
   */

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
 */

#include "rbfCompute.h"

bool rbfComputeCoefficients (matrixD *samplePtMatrix, matrixD *samplePtMatrixTrans, matrixD *phiMatrix, vectorD *funcValues, vectorD *coefficients, char rbfType, double rbfParam, bool includePolyTail)
{
	if (rbfType == 'C' || rbfType == 'T'){
		includePolyTail = true;
	}
	int i;
	for (i=0; i<coefficients->dimension; i++){
		coefficients->vec[i] = 0;
	}
	int dimension = samplePtMatrix->rows;
	int numSamplePoints = samplePtMatrix->cols;
	if (numSamplePoints < 2){
		printf ("ERROR: rbfComputeCoefficients: Cannot fit RBF with less than 2 points\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	phiMatrixCompute (samplePtMatrix, phiMatrix, rbfType, rbfParam);
	matrixD *finalMatrix = malloc (sizeof(matrixD));
	vectorD *funcValuesExt = malloc (sizeof(vectorD));
	int phirows = phiMatrix->rows;
	int inputPtRows = samplePtMatrix->rows;
	int inputPtCols = samplePtMatrix->cols;
	int info = 0;

	if (includePolyTail == true){
		matZerosD (finalMatrix, numSamplePoints+dimension+1, numSamplePoints+dimension+1); 
		int shift = 0;
		for (i=0; i<numSamplePoints; i++){
			memcpy (finalMatrix->mat+shift, phiMatrix->mat + i*phirows, sizeof(double)*phirows);
			shift += phirows;
			memcpy (finalMatrix->mat+shift, samplePtMatrix->mat + i*inputPtRows, sizeof(double)*inputPtRows);
			shift += inputPtRows;
			finalMatrix->mat[shift] = 1.0;
			shift++;
		}
		for (i=0; i<dimension; i++){
			memcpy (finalMatrix->mat+shift, samplePtMatrixTrans->mat + i*inputPtCols, sizeof(double)*inputPtCols);
			shift += inputPtCols+dimension+1;
		}
		for (i=shift; i<shift+numSamplePoints; i++){
			finalMatrix->mat[i] = 1;
		}
		vecZerosD (funcValuesExt, numSamplePoints+dimension+1);
		memcpy (funcValuesExt->vec, funcValues->vec, sizeof(double)*numSamplePoints);
		linearSystem (finalMatrix, funcValuesExt, coefficients, &info);
	}
	else{
		matZerosD (finalMatrix, numSamplePoints+1, numSamplePoints+1); 
		int shift = 0;
		for (i=0; i<numSamplePoints; i++){
			memcpy (finalMatrix->mat+shift, phiMatrix->mat + i*phirows, sizeof(double)*phirows);
			shift += phirows;
			finalMatrix->mat[shift] = 1.0;
			shift++;
		}
		for (i=shift; i<shift+numSamplePoints; i++){
			finalMatrix->mat[i] = 1;
		}
		vecZerosD (funcValuesExt, numSamplePoints+1);
		memcpy (funcValuesExt->vec, funcValues->vec, sizeof(double)*numSamplePoints);
		vectorD *coefficientsTrim = malloc (sizeof(vectorD));
		vectorInit (coefficientsTrim, numSamplePoints+1);
		linearSystem (finalMatrix, funcValuesExt, coefficientsTrim, &info);
		if (info <= 0){
			memcpy (coefficients->vec, coefficientsTrim->vec, sizeof(double)*numSamplePoints);
			coefficients->vec[numSamplePoints+dimension] = coefficientsTrim->vec[numSamplePoints];
		}
		freeVectorD (coefficientsTrim);
	}
	if (info > 2){
		printf ("rbfType = %c and rbfParam = %f, include polynomial tail %d\n",rbfType,rbfParam, includePolyTail);
		printf ("Input pts\n");
		printMatrix (samplePtMatrix);
		printf ("phiMatrix for failed linear system\n");
		printMatrix (phiMatrix);
		printf ("finalMatrix for failed linear system\n");
		printMatrix (finalMatrix);
		printf ("funcValuesExt for failed linear system\n");
		printVector (funcValuesExt);
	}
	freeMatrixD (finalMatrix);
	freeVectorD (funcValuesExt);
	if (info > 0){
		return false;
	}
	return true;
}

void phiMatrixCompute (matrixD *samplePtMatrix, matrixD *phiMatrix, char rbfType, double rbfParam)
{
	int i;
	double val = 0;
	for (i=0; i<phiMatrix->size; i++){
		val = phiMatrix->mat[i] + rbfParam;
		phiMatrix->mat[i] = val * val * val;
	}
}

//rbfVector is preloaded with square of distances (2nd norm).
void rbfValueMatrixSingleType (double *inputPt, matrixD *samplePtMatrix, matrixD *coefficients, double *rbfValues, char rbfType, double rbfParam, vectorD *rbfVector)
{
	int numSamplePoints = samplePtMatrix->cols;
	int i,j;
	double val;
	for (i=0; i<numSamplePoints; i++){
		val = sqrt (rbfVector->vec[i]) + rbfParam;
		rbfVector->vec[i] = val * val * val;
	}

	for (i=0; i<coefficients->cols; i++){
		double sum = 0;
		for (j=0; j<numSamplePoints; j++){
			sum += coefficients->mat[j+i*coefficients->rows] * rbfVector->vec[j];
		}
		for (j=numSamplePoints; j<coefficients->rows-1; j++){
			sum += coefficients->mat[j+i*coefficients->rows] * inputPt[j-numSamplePoints];
		}
		sum += coefficients->mat[(i+1)*coefficients->rows-1];
		rbfValues[i] = sum;
	}
}

double rbfValueSingle (double *inputPt, matrixD *samplePtMatrix, vectorD *coefficients, char rbfType, double rbfParam, vectorD *rbfVector)
{
	int numSamplePoints = samplePtMatrix->cols;
	double val;
	int i;
	for (i=0; i<numSamplePoints; i++){
		val = sqrt (rbfVector->vec[i]) + rbfParam;
		rbfVector->vec[i] = val * val * val;
	}
	double rbfValue = 0;
	for (i=0; i<numSamplePoints; i++){
		rbfValue += rbfVector->vec[i]*coefficients->vec[i];
	}
	for (i=numSamplePoints; i<coefficients->dimension-1; i++){
		rbfValue += inputPt[i-numSamplePoints] * coefficients->vec[i];
	}
	rbfValue += coefficients->vec[coefficients->dimension-1];
	return rbfValue;
}
