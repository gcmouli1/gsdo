/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

/* Min f(x) where x in R^n
 * subject to: g_i(x) >= 0 for i = 1,...,m
 * Algorithm based on the paper "A Constraint Handling Approach for the Differential Evolution Algorithm"
 * by Jouni Lampinen */

/*General choice of initialization
* populationSize = 20 * dimension;
* CR = 0.9;
* F = 0.9;
* maxIterations = 1000;
*/

#ifndef _DIFFERENTIALEVOLUTION_H
#define _DIFFERENTIALEVOLUTION_H
#include "vecMatDeclare.h"

typedef void *userDataPtr;
typedef struct {
	bool useInputPts;
	int dimension;
	int numConstraints;
	int populationSize;
	int maxIterations;
	int numOutputs;
	int numSortedUniquePts;
	double F;
	double CR;
	vectorD *lb;
	vectorD *ub;
	vectorD *inputFuncVal;
	matrixD *inputMat;
	matrixD *inputConstrMat;
} diffEvolStruct;

typedef struct {
	int index;
	double funcVal;
	matrixD *constrMat;
} fcCompareStruct;

bool compareConstraints (vectorD *constrVec1, vectorD *constrVec2, bool *feasStatusPt1, bool *feasStatusPt2);
int fcCompare (const void *fc1, const void *fc2);
int fcMin (matrixD *constrMat, vectorD *funcValues);
void fcSort (matrixD *constrMat, vectorD *funcValues, int *indices);
void differentialEvolution (diffEvolStruct *diffEvol, matrixD *outputMat, vectorD *outputVec, char *ptFeasibility, void funcConstrEval (double *, double *, double *, int, int, void *), userDataPtr udptr);
#endif
