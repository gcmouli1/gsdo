/*
This file is part of GSDO

    GSDO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GSDO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GSDO.  If not, see <https://www.gnu.org/licenses/>.
*/

/*  GSDO: Global optimization using Surrogates for Derivative free Optimization.
 *  Author: Gannavarapu Chandramouli
*/

#include "differentialEvolution.h"

bool compareConstraints (vectorD *constrVec1, vectorD *constrVec2, bool *feasStatusPt1, bool *feasStatusPt2)
{
	int i;
	double negPrecision = 0;
	bool betterPtStatus = false;
	int numFeas1 = 0;
	int numFeas2 = 0;
	int pt1Better = 0;
	int pt2Better = 0;
	double violPt1, violPt2;

	for (i=0; i<constrVec1->dimension; i++){
		if (constrVec1->vec[i] >= negPrecision){
			numFeas1++;
		}
		if (constrVec2->vec[i] >= negPrecision){
			numFeas2++;
		}
		violPt1 = MIN (negPrecision, constrVec1->vec[i]);
		violPt2 = MIN (negPrecision, constrVec2->vec[i]);
		if (violPt1 >= violPt2){
			pt1Better++;
		}
		else{
			pt2Better++;
		}
	}

	if (numFeas1 == constrVec1->dimension){
		*feasStatusPt1 = true;
	}
	else {
		*feasStatusPt1 = false;
	}
	if (numFeas2 == constrVec2->dimension){
		*feasStatusPt2 = true;
	}
	else{
		*feasStatusPt2 = false;
	}

	if (*feasStatusPt1 == false && *feasStatusPt2 == false && pt1Better >= pt2Better && pt1Better == constrVec1->dimension){
		betterPtStatus = true;
	}
	return betterPtStatus;
}

int fcCompare (const void *fc1, const void *fc2)
{
	double negPrecision = -1E-7;
	double precision = 1E-7;
	int lIndex = (int)((fcCompareStruct *)fc1)->index; 
	int rIndex = (int)((fcCompareStruct *)fc2)->index; 
	double lf = (double)((fcCompareStruct *)fc1)->funcVal; 
	double rf = (double)((fcCompareStruct *)fc2)->funcVal; 
	matrixD *constrMat = (matrixD*)((fcCompareStruct *)fc1)->constrMat; 

	int numConstraints = constrMat->rows;
	int lNumFeas = 0;
	int rNumFeas = 0;
	int lPtBetter = 0;
	int rPtBetter = 0;
	bool lFeasStatusPt, rFeasStatusPt;
	double lViolPt, rViolPt;
	double lc, rc;

	int i;
	for (i=0; i<numConstraints; i++){
		lc = constrMat->mat[i + lIndex * numConstraints];
		rc = constrMat->mat[i + rIndex * numConstraints];
		if (lc >= negPrecision){
			lNumFeas++;
		}
		if (rc >= negPrecision){
			rNumFeas++;
		}
		lViolPt = MIN (negPrecision, lc);
		rViolPt = MIN (negPrecision, rc);
		if (lViolPt >= rViolPt){
			lPtBetter++;
		}
		else{
			rPtBetter++;
		}
	}

	if (lNumFeas == numConstraints){
		lFeasStatusPt = true;
	}
	else {
		lFeasStatusPt = false;
	}
	if (rNumFeas == numConstraints){
		rFeasStatusPt = true;
	}
	else{
		rFeasStatusPt = false;
	}

	int betterPtStatus = 0;
	if (lFeasStatusPt == false && rFeasStatusPt == false && lPtBetter >= rPtBetter && lPtBetter == numConstraints){
		betterPtStatus = -1;
	}
	if (lFeasStatusPt == false && rFeasStatusPt == false && lPtBetter <= rPtBetter && rPtBetter == numConstraints){
		betterPtStatus = 1;
	}

	if ((lf <= rf - precision && lFeasStatusPt == true) || (lf > rf + precision && lFeasStatusPt == true && rFeasStatusPt == false) || (lFeasStatusPt == false && betterPtStatus == -1)){
		return -1;
	}
	else if ((rf <= lf - precision && rFeasStatusPt == true) || (rf > lf + precision && rFeasStatusPt == true && lFeasStatusPt == false) || (rFeasStatusPt == false && betterPtStatus == 1)){
		return 1;
	}
	else{
		return 0;
	}
}

int fcMin (matrixD *constrMat, vectorD *funcValues)
{
	int numPts = funcValues->dimension;
	int i;
	if (constrMat->cols != funcValues->dimension){
		printf ("ERROR: fcMin: Mismatch in size of constraint matrix and function vector\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numPts <= 0){
		printf ("ERROR: fcMin: Cannot find minimum from an empty array\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numPts == 1){
		return 0;
	}
	fcCompareStruct *fc = malloc (sizeof(fcCompareStruct)*numPts);
	for (i=0; i<numPts; i++){
		fc[i].funcVal = funcValues->vec[i];
		fc[i].constrMat = constrMat;
		fc[i].index = i;
	}

	int optIndex = 0;
	for (i=0; i<numPts; i++){
		int status = fcCompare ((void *)&fc[optIndex], (void *)&fc[i]);
		if (status == 1){
			optIndex = i;
		}
	}
	free (fc);
	return optIndex;
}

void fcSort (matrixD *constrMat, vectorD *funcValues, int *indices)
{
	int numPts = funcValues->dimension;
	int i;
	if (constrMat->cols != funcValues->dimension){
		printf ("ERROR: deSort: Mismatch in size of constraint matrix and function vector\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numPts <= 0){
		printf ("ERROR: deSort: Cannot sort an empty array\n");
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (numPts == 1){
		indices[0] = 0;
		return;
	}
	fcCompareStruct *fc = malloc (sizeof(fcCompareStruct)*numPts);
	for (i=0; i<numPts; i++){
		fc[i].funcVal = funcValues->vec[i];
		fc[i].constrMat = constrMat;
		fc[i].index = i;
	}
	qsort (fc, numPts, sizeof(fc[0]), fcCompare);
	for (i=0; i<numPts; i++){
		indices[i] = fc[i].index;
	}
	free (fc);
}

void differentialEvolution (diffEvolStruct *diffEvol, matrixD *outputMat, vectorD *outputVec, char *ptFeasibility, void funcConstrEval (double *, double *, double *, int, int, void *), userDataPtr udPtr)
{
	double negPrecision = -1e-7;
	if (outputMat->cols < diffEvol->numOutputs || outputVec->dimension < diffEvol->numOutputs){
		printf ("ERROR: differentialEvolution: Cannot generate %d outputs. Incorrect dimensions for matrix or vector\n",diffEvol->numOutputs);
		printf ("PROCESS TERMINATED\n");
		exit (EXIT_FAILURE);
	}
	if (diffEvol->populationSize < diffEvol->numOutputs){
		printf ("WARNING: differentialEvolution: Population size is less than numOutputs. Increasing it to numoutputPts\n"); 
		diffEvol->populationSize = diffEvol->numOutputs;
	}

	int i,j,k;
	matrixD *popMat = malloc (sizeof(matrixD));
	matrixInit (popMat, diffEvol->dimension, diffEvol->populationSize);
	vectorD *popFuncValues = malloc (sizeof(vectorD));
	vectorInit (popFuncValues, diffEvol->populationSize);
	matrixD *popConstrMatrix = malloc (sizeof(matrixD));
	matrixInit (popConstrMatrix, diffEvol->numConstraints, diffEvol->populationSize);

	vectorD *tmpPt = malloc (sizeof(vectorD));
	vectorInit (tmpPt, diffEvol->dimension);
	vectorD *tmpConstr = malloc (sizeof(vectorD));
	vectorInit (tmpConstr, diffEvol->numConstraints);
	vectorD *constrVec = malloc (sizeof(vectorD));
	vectorInit (constrVec, diffEvol->numConstraints);
	char *feasibilityIndex = malloc (sizeof(char)*diffEvol->populationSize);

	int offset = 0;
	if (diffEvol->useInputPts == true){
		memcpy (popMat->mat, diffEvol->inputMat->mat, sizeof(double)*(diffEvol->inputMat->size));
		memcpy (popConstrMatrix->mat, diffEvol->inputConstrMat->mat, sizeof(double)*(diffEvol->inputConstrMat->size));
		memcpy (popFuncValues->vec, diffEvol->inputFuncVal->vec, sizeof(double)*(diffEvol->inputFuncVal->dimension));
		offset += diffEvol->inputMat->cols;
	}

	int randomSeed = 1;
	srand (randomSeed);
	for (i=offset; i<popMat->cols; i++){
		for (j=0; j<popMat->rows; j++){
			popMat->mat[j + i * popMat->rows] = ((double)rand()/(double)RAND_MAX)*(diffEvol->ub->vec[j]-diffEvol->lb->vec[j]) + diffEvol->lb->vec[j];
		}
	}
	for (i=offset; i<popMat->cols; i++){
		vecExtractMat (tmpPt, popMat, i);
		funcConstrEval (tmpPt->vec, &popFuncValues->vec[i], tmpConstr->vec, diffEvol->dimension, diffEvol->numConstraints, udPtr);
		j = 0;
		feasibilityIndex[i] = 'F';
		while (j < popConstrMatrix->rows){
			if (tmpConstr->vec[j] < negPrecision){
				feasibilityIndex[i] = 'I';
				break;
			}
			j++;
		}
		memcpy (popConstrMatrix->mat + i*diffEvol->numConstraints, tmpConstr->vec, sizeof(double)*diffEvol->numConstraints);
	}

	matrixD *nextGenMat = malloc (sizeof(matrixD));
	matrixInit (nextGenMat, diffEvol->dimension, diffEvol->populationSize);
	vectorD *nextGenFuncValues = malloc (sizeof(vectorD));
	vectorInit (nextGenFuncValues, diffEvol->populationSize);
	matrixD *nextGenConstrMat = malloc (sizeof(matrixD));
	matrixInit (nextGenConstrMat, diffEvol->numConstraints, diffEvol->populationSize);
	memcpy (nextGenMat->mat, popMat->mat, sizeof(double)*popMat->size);
	memcpy (nextGenConstrMat->mat, popConstrMatrix->mat, sizeof(double)*popConstrMatrix->size);
	memcpy (nextGenFuncValues->vec, popFuncValues->vec, sizeof(double)*popFuncValues->dimension);

	//Main algorithm
	for (k=0; k<diffEvol->maxIterations; k++){
		for (i=0; i<diffEvol->populationSize; i++){
			int randomNum = (int)(((double)rand()/(double)RAND_MAX)*diffEvol->dimension);
			int r1, r2, r3;
			do {
				r1 = (int)(((double)rand()/(double)RAND_MAX)*diffEvol->populationSize);
			} while (r1 == i);
			do {
				r2 = (int)(((double)rand()/(double)RAND_MAX)*diffEvol->populationSize);
			} while (r2 == i || r2 == r1);
			do {
				r3 = (int)(((double)rand()/(double)RAND_MAX)*diffEvol->populationSize);
			} while (r3 == i || r3 == r1 || r3 == r2);

			for (j=0; j<diffEvol->dimension; j++){
				double randomValue = (double)rand()/(double)RAND_MAX;
				if (randomValue < diffEvol->CR || randomNum == j){
					tmpPt->vec[j] = popMat->mat[r3*popMat->rows + j] + diffEvol->F*(popMat->mat[r1*popMat->rows + j] - popMat->mat[r2*popMat->rows + j]);
					if (tmpPt->vec[j] < diffEvol->lb->vec[j] || tmpPt->vec[j] > diffEvol->ub->vec[j]){
						tmpPt->vec[j] = ((double)rand()/(double)RAND_MAX)*(diffEvol->ub->vec[j]-diffEvol->lb->vec[j]) + diffEvol->lb->vec[j];
					}
				}
				else{
					tmpPt->vec[j] = popMat->mat[i*popMat->rows + j];
				}
			}
			double tmpFuncValue = DBL_MAX;
			funcConstrEval (tmpPt->vec, &tmpFuncValue, tmpConstr->vec, diffEvol->dimension, diffEvol->numConstraints, udPtr);
			vecExtractMat (constrVec, popConstrMatrix, i);
			bool feasStatusPt1 = false;
			bool feasStatusPt2 = false;
			bool betterPt = compareConstraints (tmpConstr, constrVec, &feasStatusPt1, &feasStatusPt2);
			if ((tmpFuncValue <= popFuncValues->vec[i] && feasStatusPt1 == true) || (tmpFuncValue > popFuncValues->vec[i] && feasStatusPt1 == true && feasStatusPt2 == false) || (feasStatusPt1 == false && betterPt == true)){
				memcpy (nextGenMat->mat+i*nextGenMat->rows, tmpPt->vec, sizeof(double)*nextGenMat->rows);
				nextGenFuncValues->vec[i] = tmpFuncValue;
				memcpy (nextGenConstrMat->mat+i*nextGenConstrMat->rows, tmpConstr->vec, sizeof(double)*nextGenConstrMat->rows);

				j = 0;
				feasibilityIndex[i] = 'F';
				while (j < popConstrMatrix->rows){
					if (tmpConstr->vec[j] < negPrecision){
						feasibilityIndex[i] = 'I';
						break;
					}
					j++;
				}
			}
		}
		memcpy (popMat->mat, nextGenMat->mat, sizeof(double)*nextGenMat->size);
		memcpy (popFuncValues->vec, nextGenFuncValues->vec, sizeof(double)*nextGenFuncValues->dimension);
		memcpy (popConstrMatrix->mat, nextGenConstrMat->mat, sizeof(double)*nextGenConstrMat->size);
	}

	freeMatrixD (nextGenMat);
	freeMatrixD (nextGenConstrMat);
	freeVectorD (nextGenFuncValues);
	freeVectorD (tmpPt);
	freeVectorD (constrVec);
	freeVectorD (tmpConstr);

	//Sorting and extracting pts from popMat. 
	if (diffEvol->numOutputs == 1){
		int index = fcMin (popConstrMatrix, popFuncValues);
		memcpy (outputMat->mat, popMat->mat + index * popMat->rows, sizeof(double)*popMat->rows);
		outputVec->vec[0] = popFuncValues->vec[index];
		ptFeasibility[0] = feasibilityIndex[index];
	}
	else {
		double precision = 1E-4;
		int *indices = malloc (sizeof(int)*popMat->cols);
		int numElem;
		uniqueMatrix (popMat, indices, &numElem, precision);
		matrixD *popMatTrim = malloc (sizeof(matrixD));
		matrixInit (popMatTrim, popMat->rows, numElem);
		matrixD *popConstrMatrixTrim = malloc (sizeof(matrixD));
		matrixInit (popConstrMatrixTrim, popConstrMatrix->rows, numElem);
		vectorD *funcValueTrim = malloc (sizeof(vectorD));
		vectorInit (funcValueTrim, numElem);
		char *feasibilityTrim = malloc (sizeof(char)*numElem);

		for (i=0; i<numElem; i++){
			int index = indices[i];
			memcpy (popMatTrim->mat + i*popMatTrim->rows, popMat->mat + index*popMat->rows, sizeof(double)*popMat->rows);
			memcpy (popConstrMatrixTrim->mat + i*popConstrMatrixTrim->rows, popConstrMatrix->mat + index*popConstrMatrix->rows, sizeof(double)*popConstrMatrix->rows);
			funcValueTrim->vec[i] = popFuncValues->vec[index];
			feasibilityTrim[i] = feasibilityIndex[index];
		}
		free (indices);

		int *indicesTrim = malloc (sizeof(int)*numElem);
		fcSort (popConstrMatrixTrim, funcValueTrim, indicesTrim);
		int maxElem = MIN (numElem, outputMat->cols);
		diffEvol->numSortedUniquePts = maxElem;
		for (i=0; i<maxElem; i++){
			int index = indicesTrim[i];
			memcpy (outputMat->mat + i * outputMat->rows, popMatTrim->mat + index * popMatTrim->rows, sizeof(double)*popMatTrim->rows);
			outputVec->vec[i] = funcValueTrim->vec[index];
			ptFeasibility[i] = feasibilityTrim[index];
		}
		free (indicesTrim);
		free (feasibilityTrim);
		freeVectorD (funcValueTrim);
		freeMatrixD (popMatTrim);
		freeMatrixD (popConstrMatrixTrim);
	}
	free (feasibilityIndex);
	freeMatrixD (popMat);
	freeMatrixD (popConstrMatrix);
	freeVectorD (popFuncValues);
}

