EXE = gsdo
SOURCEPATH = $(shell pwd)
THIRDPARTYPATH = $(SOURCEPATH)/ThirdParty
SRC = $(SOURCEPATH)/SRC
OBJDIR = $(SOURCEPATH)/Objects
OBJS_tmp = gsdo.o customFunctions.o localSearch.o rbfCompute.o knn.o rbfOptUsingDE.o differentialEvolution.o computeObjConstrValue.o feasiblePtGenerate.o generatePts.o globalApproach.o inputParameters.o latinHypercube.o multipleFeasiblePoints.o solution.o vecBlas.o vecLapack.o vecMatDeclare.o main.o

OBJS = $(patsubst %.o,$(OBJDIR)/%.o, $(OBJS_tmp))

LIBPATH = $(THIRDPARTYPATH)/lib
INCLUDEDIR = 
LIBRARYPATH += -L $(LIBPATH)

# C Compiler command
CC = gcc 
FC = gfortran

# C Compiler options
CFLAGS = -O3 -fPIC -Wall
#CFLAGS = -pg -fPIC -Wall
#CFLAGS = -g -fPIC -Wall
FORTRANFLAG = -lgfortran

# additional C Compiler options for linking
# Make changes with lapack and blas path if custom path is used for them.
LFLAGS = -Wl,-rpath=$(LIBPATH) -llapack_LINUX -lblas_LINUX -lm 
gsdo : $(OBJS) 
	$(CC) $^ $(INCLUDEDIR) $(LIBRARYPATH) $(CFLAGS) $(LFLAGS) $(FORTRANFLAG) -o $@ 

$(OBJDIR)/%.o : $(SRC)/%.c 
	$(CC) $^ -c $(INCLUDEDIR) $(CFLAGS) -o $@

$(OBJDIR)/%.o : %.f
	$(CC) -c $^ $(INCLUDEDIR) $(LIBRARYPATH) $(CFLAGS) $(LFLAGS) $(FORTRANFLAG) -o $@

$(OBJDIR)/%.o : %.for
	$(CC) -c $^ $(INCLUDEDIR) $(FORTRANFLAG) -o $@

clean:
	rm -f  $(OBJS) gsdo INPUT/input.txt INPUT/output.txt input.txt output.txt 2>/dev/null
