# Global optimization using Surrogates for Derivative free Optimization (GSDO)

An optimization library to solve constrained derivative free optimization problems in global sense.
The optimization problem should of the following format:
```math
 \text{min}\quad f(x)\\
 g_j(x) >= 0 \quad \text{for}\quad j = 1,\ldots, m\\
 l_i <= x_i <= u_i \quad \text{for} \quad i = 1,\ldots,n.\\
```
## Installation requirements:

1. Linux operating system with shell (preferably bash).
2. GCC compiler for C language.
3. Python 3 with numpy installed for running examples or test problems.
4. AMD64 architecture.

All the required third-party softwares are already provided in the ThirdParty directory.

## Guidelines to run the program:
* Open a terminal or console.
* Clone/download the project into destination directory.

	```
	git clone https://gitlab.com/gcmouli1/gsdo
	```

* Move to the destination directory (gsdo) and create 3 directories: Objects, INPUT and OUTPUT

	```
	cd gsdo
	mkdir -p {Objects,INPUT,OUTPUT}
	```

* Run the Makefile.

	```
	make
	```

* If "make" is successful, we obtain an executable binary called "gsdo" in the current directory.
* Run the executable with input parameters from ***INPUT*** directory.

	```
	 ./gsdo INPUT/parameters.txt
	```

* The output files are stored in ***OUTPUT*** directory.

## Steps to run an example file, say TEST_Examples/G1.
* Copy all the files from TEST_Examples/G1 to ***INPUT*** directory.
* Run the following commands in the terminal:

	```
	make
	```

	```
	./gsdo INPUT/parameters.txt
	```

* Check ***OUTPUT*** directory for various outputs. 

## Input for the example file
 All input information must be placed inside ***INPUT*** directory. It should contain the following files:

***Simulation Executables*** The executable binary or file which reads input point using file "input.txt" and returns its output (objective and constraint values) to "output.txt". Here both input.txt and output.txt are line separated. For e.g. each attached test cases contain two python files:  function_constraint_eval.py and funcWrapper.py. Here funcWrapper.py reads input point details from "input.txt" file, uses function_constraint_eval.py to computes objective and constraints, and finally returns output via "output.txt" file.

***parameters.txt***: This contains the information regarding various parameters supplied by the user.

## Parameters
The parameters are supplied via ***parameters.txt*** file in the ***INPUT*** folder. See any test case folder. It contains the following information: 

* Executable: The simulation code binary.
* ExecutableInputPtPath: The path from where simulation binary reads the input.txt file.
* ExecutableOutputPtPath: The path to where simulation binary writes the output.txt file.
* OutputFolder: The path where GSDO outputs are written.
* ObjectiveIndex: Position (line number) of objective value in output.txt file. (Default=1).
* Dimension: The dimension of the problem.
* NumConstraints: The number of constraints in the problem.
* MaxEvals: Maximum number of simulations allowed by the budget. One simulation corresponds to the evaluation of objective function and all the constraints at a given point.
* GlobalPrecision: The tolerance value used as termination criterion by solver during global exploration step.
* RandomSeed: Start seed for random generation.
* LB: Lower bound values on the variable. They need to be separated by spaces.
* UB: Upper bound values on the variable. They need to be separated by spaces.
* ConstraintType: Constraint type in terms of numbers. Use 5 for QRSK, 6 for NRSK, 7 for QUSK, 8 for NUSK. They need to be separated by spaces.

## Output
All output information is stored inside ***OUTPUT*** directory. It contains the following files:

* ***obj_values.csv*** : Objective values of all the points (feasible as well as infeasible) are stored here.
* ***points.csv***     : Contains all the points at which expensive functions have been evaluated. (Stored row-wise).
* ***constraints.csv***: Contains constraint values of all the evaluated points. (Stored row-wise).
* ***feasibility.csv***: Contains feasibility info about the points. 0 implies infeasible and 1 implies feasible.

## Output printed on the console.
* Apart from OUTPUT directory, the solver also prints some information to the stdout.
* A brief summary about the three stages involved in the gsdo algorithm. 
* Information about the  number of points generated, objective value and maximum constraint violation at current point, best feasible objective value attained till this iteration, feasibility details for the current point, and the step type used by the algorithm.

## Working of the algorithm
The gsdo algorithm/solver works in the following way:
* The solver uses *system* command to communicate with the simulation code. 
* ***input.txt***: The solver generates points in each iteration. To evaluate objective function and constraints over these points they need to be sent to simulation code. This is done by printing the point to ***input.txt*** file in a single column (not row-wise).
* ***output.txt***: The simulation code reads data from *input.txt*, computes the objective function and constraints, and then it resends the output to solver using *output.txt*. The output is stored in a single column where first value is the objective function and rest are constraint values.
* The simulation code must be written in a way such that it reads data from *input.txt* and paste the output to *output.txt*.
* Data inside ***input.txt*** and ***output.txt*** keeps changing with each new point. They are deleted at the end, by the solver.

## Citation
Please cite the following paper if you are planning to use this software:

Gannavarapu Chandramouli and Vishnu Narayanan, "An open-source solver for finding global solutions to constrained derivative-free optimization problems" [arXiv:2404.18080].
