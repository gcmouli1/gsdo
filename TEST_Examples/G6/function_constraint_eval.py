import numpy as np
import sys

def get_dimension_num_constraints():
    return 2,2

def func_eval(x):
    return (x[0]-10)**3 + (x[1]-20)**3

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(-1*(x[0]-5)**2 - (x[1]-5)**2 + 100)/100
    elif index == 1:
        return -1.0*((x[0]-6)**2 + (x[1]-5)**2 - 82.81)/82.81
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[13,100],[0,100]])
