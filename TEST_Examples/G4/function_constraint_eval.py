import numpy as np
import sys

def get_dimension_num_constraints():
    return 5,6

def func_eval (x):
    return 5.3578547*x[2]**2 + 0.8356891*x[0]*x[4] + 37.293239*x[0] - 40792.141

def constraint_eval (x, index):
    u = 85.334407 + 0.0056858*x[1]*x[4] + 0.0006262*x[0]*x[3] - 0.0022053*x[2]*x[4]
    v = 80.51249 + 0.0071317*x[1]*x[4] + 0.0029955*x[0]*x[1] + 0.0021813*x[2]**2
    w = 9.300961 + 0.0047026*x[2]*x[4] + 0.0012547*x[0]*x[2] + 0.0019085*x[2]*x[3]
    if index == 0:
        return u
    elif index == 1:
        return -1.0*(u-92)
    elif index == 2:
        return -1.0*(-v+90)
    elif index == 3:
        return -1.0*(v-110)
    elif index == 4:
        return -1.0*(-w+20)
    elif index == 5:
        return -1.0*(w-25)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[78,102],[33,45],[27,45],[27,45],[27,45]])



