import numpy as np
import sys

def get_dimension_num_constraints():
    return 4,6

def func_eval(x):
    return 1.10471*(x[0]**2)*x[1] + 0.04811*x[2]*x[3]*(14.0+x[1])

def constraint_eval(x, constraint_index):
    P = 6000
    L = 14
    E = 30E6
    G = 12E6
    t_max = 13600
    s_max = 30000
    x_max = 10
    d_max = 0.25
    M = P*(L+x[1]/2)
    R = np.sqrt(0.25*(x[1]**2+(x[0]+x[2])**2))
    J = np.sqrt(2)*x[0]*x[1]*((x[1]**2)/12 + 0.25*(x[0]+x[2])**2)
    P_c = ((4.013*E*x[2]*x[3]**3)/(6*L**2))*(1-0.25*x[2]*np.sqrt(E/G)/L)
    t_1 = P/(np.sqrt(2)*x[0]*x[1])
    t_2 = M*R/J
    t = np.sqrt(t_1**2 + t_1*t_2*x[1]/R + t_2**2)
    s = 6*P*L/(x[3]*x[2]**2)
    d = 4*P*L**3/(E*x[3]*x[2]**3)

    if constraint_index == 0:
        return  -1*(t-t_max)/t_max
    elif constraint_index == 1:
        return -1*(s-s_max)/s_max
    elif constraint_index == 2:
        return -1*(x[0]-x[3])/x_max
    elif constraint_index == 3:
        return -1*(0.10471*x[0]**2+0.04811*x[2]*x[3]*(14.0+x[1])-5.0)/5.0
    elif constraint_index == 4:
        return -1.0*(d-d_max)/d_max
    elif constraint_index == 5:
        return -1.0*(P-P_c)/P
    else:
        sys.exit("constraint index out of bound")

def get_bounds():
    return np.array([[0.125,10],[0.1,10],[0.1,10],[0.1,10]])
