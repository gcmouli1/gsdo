Executable python3 INPUT/funcWrapper.py
ExecutableInputPtPath INPUT/input.txt
ExecutableOutputPtPath INPUT/output.txt
outputFolder OUTPUT
ObjectiveIndex 1
dimension 4
numConstraints 6
maxEvals 75
GlobalPrecision 1E-5
RandomSeed 1
ConstraintType 5 5 5 5 5 5
LB 0.125 0.1 0.1 0.1 
UB 10 10 10 10
