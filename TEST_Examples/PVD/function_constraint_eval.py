import numpy as np
import sys

def get_dimension_num_constraints():
    return 4,3

def func_eval(x):
    return 0.6224*x[0]*x[2]*x[3]+1.7781*x[1]*x[2]**2 + 3.1661*x[0]**2*x[3] + 19.84*x[0]**2*x[2]

def plog(x):
    if x >= 0:
        return np.log(1+x)
    else:
        return -1.0*np.log(1-x)

def constraint_eval(x,index):
    if index == 0:
        return -1.0*(-x[0]+0.0193*x[2])
    elif index == 1:
        return -1.0*(-x[1]+0.00954*x[2])
    elif index == 2:
        return -1.0*(plog(-np.pi*x[2]**2*x[3] - (4/3)*np.pi*x[2]**3 + 1296000))
    else:
        sys.exit("constraint index out of bound")

def get_bounds():
    return np.array([[0,1],[0,1],[0,50],[0,240]])
        
