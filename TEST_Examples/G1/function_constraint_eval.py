import numpy as np
import sys

def get_dimension_num_constraints():
    return 13,9

def func_eval(x):
    return 5*np.sum(x[0:4]) - 5* np.sum(x[0:4]*x[0:4]) - np.sum(x[4:13])


def constraint_eval (x,index):
    if index == 0:
        return -1.0*(2*x[0] + 2*x[1] + x[9] + x[10] - 10)
    if index == 1:
        return -1.0*(2*x[0] + 2*x[2] + x[9] + x[11] - 10)
    if index == 2:
        return -1.0*(2*x[1] + 2*x[2] + x[10] + x[11] - 10)
    if index == 3:
        return -1.0*(-8*x[0] + x[9])
    if index == 4:
        return -1.0*(-8*x[1] + x[10])
    if index == 5:
        return -1.0*(-8*x[2] + x[11])
    if index == 6:
        return -1.0*(-2*x[3] - x[4] + x[9])
    if index == 7:
        return -1.0*(-2*x[5] - x[6] + x[10])
    if index == 8:
        return -1.0*(-2*x[7] - x[8] + x[11])
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,100],[0,100],[0,100],[0,1]])
