import numpy as np
import sys

def get_dimension_num_constraints():
    return 4,1

def func_eval(x):
    return (8.61E5)*np.sqrt(x[0])*x[1]*x[2]**(-2/3)*x[3]**(-1/2) + (3.69E4)*x[2] + (7.72E8)*(1/x[0])*(x[1]**0.219) - (765.43E6)/x[0]

def constraint_eval(x,index):
    if index == 0:
        return -1.0*(x[3]/(x[1]**2) + 1/(x[1]**2) -1)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[20,50],[1,10],[20,50],[0.1,60]])
