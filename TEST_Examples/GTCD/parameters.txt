Executable python3 INPUT/funcWrapper.py
ExecutableInputPtPath INPUT/input.txt
ExecutableOutputPtPath INPUT/output.txt
outputFolder OUTPUT
ObjectiveIndex 1
dimension 4
numConstraints 1
maxEvals 75
GlobalPrecision 1E-5
RandomSeed 1
ConstraintType 5
LB 20 1 20 0.1
UB 50 10 50 60
