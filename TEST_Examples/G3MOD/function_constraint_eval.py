import numpy as np
import sys

def get_dimension_num_constraints():
    return 20,1

def plog(x):
    if x >= 0:
        return np.log(1+x)
    else:
        return -1.0*np.log(1-x)

def func_eval(x):
    d = x.shape[0]
    return -plog( d**(d/2)*np.prod(x))

def constraint_eval (x, index):
    if index == 0:
        return -1.0*(np.sum(x**2) - 1)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    #d = x.shape[0]
    d = 20
    return np.tile([0,1],(d,1))
    
