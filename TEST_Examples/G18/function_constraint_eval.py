import numpy as np
import sys

def get_dimension_num_constraints():
    return 9,13

def func_eval(x):
    return -0.5*(x[0]*x[3] - x[1]*x[2] + x[2]*x[8] - x[4]*x[8] + x[4]*x[7] - x[5]*x[6])

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(x[2]**2 + x[3]**2 - 1)

    if index == 1:
        return -1.0*(x[8]**2 -1)

    if index == 2:
        return -1.0*(x[4]**2 + x[5]**2 -1)

    if index == 3:
        return -1.0*(x[0]**2 + (x[1]-x[8])**2 - 1)

    if index == 4:
        return -1.0*((x[0]-x[4])**2 + (x[1]-x[5])**2 -1 )

    if index == 5:
        return -1.0*((x[0]-x[6])**2 + (x[1]-x[7])**2 - 1)

    if index == 6:
        return -1.0*((x[2]-x[4])**2 + (x[3]-x[5])**2 - 1)

    if index == 7:
        return -1.0*((x[2]-x[6])**2 + (x[3]-x[7])**2 - 1)

    if index == 8:
        return -1.0*(x[6]**2 + (x[7]-x[8])**2 -1)

    if index == 9:
        return -1.0*(x[1]*x[2] - x[0]*x[3])

    if index == 10:
        return -1.0*(-x[2]*x[8])

    if index == 11:
        return -1.0*(x[4]*x[8])

    if index == 12:
        return -1.0*(x[5]*x[6] - x[4]*x[7])

    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[-10,10],[-10,10],[-10,10],[-10,10],[-10,10],[-10,10],[-10,10],[-10,10],[0,20]])
