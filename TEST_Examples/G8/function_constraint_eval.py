import numpy as np
import sys

def get_dimension_num_constraints():
    return 2,2

def func_eval(x):
    return (-np.sin(2*np.pi*x[0])**3)*np.sin(2*np.pi*x[1])/((x[0]**3)*(x[0]+x[1]))

def constraint_eval(x, index):
    if index == 0:
        return -1.0*(x[0]**2 - x[1] + 1)
    if index == 1:
        return -1.0*(1 - x[0] + (x[1]-4)**2)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[0,10],[0,10]])
