import numpy as np
import sys

def get_dimension_num_constraints():
    return 6,6

def func_eval(x):
    return -25*(x[0]-2)**2 - (x[1]-2)**2 - (x[2]-1)**2 - (x[3]-4)**2 - (x[4]-1)**2 - (x[5]-4)**2

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(2 - x[0] - x[1])/2

    if index == 1:
        return -1.0*(x[0] + x[1] - 6)/6

    if index == 2:
        return -1.0*(-x[0] + x[1] -2)/2

    if index == 3:
        return -1.0*(x[0] - 3*x[1] -2)/2

    if index == 4:
        return -1.0*(4 - (x[2]-3)**2 - x[3])/4

    if index == 5:
        return -1.0*(4 - (x[4]-3)**2 - x[5])/4

    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[0,5],[0,4],[1,5],[0,6],[1,5],[0,10]])
