import numpy as np
import sys
import function_constraint_eval as fce
dimension, num_constraints = fce.get_dimension_num_constraints()
filename = 'INPUT/input.txt'
x = np.loadtxt (filename)
value = np.zeros(num_constraints+1)
value[0] = fce.func_eval(x)
for index in range(num_constraints):
    value[index+1] = fce.constraint_eval (x,index)
#np.savetxt ('output.txt',value,fmt='%.9f')
#np.savetxt ('output.txt',value,fmt='%1.9e')
np.savetxt ('INPUT/output.txt',value)
