import numpy as np
import sys

def get_dimension_num_constraints():
    return 4,5

def func_eval (x):
    return 3*x[0] + 1E-6*x[0]**3 + 2*x[1] + (2E-6/3)*x[1]**3

def constraint_eval(x,index):
    if index == 0:
        return -1 * (x[2] - x[3] - 0.55)
    elif index == 1:
        return -1 * (x[3] - x[2] - 0.55)
    elif index == 2:
        return -1*(1000*np.sin(-x[2]-0.25) + 1000*np.sin(-x[3] - 0.25) + 894.8 - x[0])
    elif index == 3:
        return -1*(1000*np.sin(x[2]-0.25) + 1000*np.sin(x[2]-x[3]-0.25) + 894.8 - x[1])
    elif index == 4:
        return -1*(1000*np.sin(x[3]-0.25) + 1000*np.sin(x[3]-x[2]-0.25) + 1294.8)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[0,1200],[0,1200],[-0.55,0.55],[-0.55,0.55]])

