import numpy as np
import sys

def get_dimension_num_constraints():
    return 7,4

def func_eval(x):
    return (x[0]-10)**2 + 5*(x[1]-12)**2 + x[2]**4 + 3*(x[3]-11)**2 + 10*x[4]**6 + 7*x[5]**2 + x[6]**4 -4*x[5]*x[6] - 10*x[5] - 8*x[6]

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(2*x[0]**2 + 3*x[1]**4 + x[2] + 4*x[3]**2 + 5 *x[4] - 127)/127
    elif index == 1:
        return -1.0*(7*x[0] + 3*x[1] + 10*x[2]**2 + x[3] - x[4] - 282)/282
    elif index == 2:
        return -1.0*(23*x[0] + x[1]**2 + 6*x[5]**2 - 8*x[6] - 196)/196
    elif index == 3:
        return -1.0*(4*x[0]**2 + x[1]**2 - 3*x[0]*x[1] + 2*x[2]**2 + 5*x[5] - 11*x[6])
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.tile([-10,10],(7,1))
