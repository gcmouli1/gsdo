import numpy as np
import sys

def get_dimension_num_constraints():
    return 7,11

def func_eval(x):
    A = 3.3333*x[2]**2 + 14.9334*x[2] - 43.0934
    B = x[5]**2 + x[6]**2
    C = x[5]**3 + x[6]**3
    D = x[3]*(x[5]**2) + x[4]*(x[6]**2)
    return 0.7854*x[0]*(x[1]**2)*A - 1.508*x[0]*B + 7.477*C + 0.7854*D

def constraint_eval(x,index):
    if index == 0:
        return -1.0*(27-x[0]*(x[1]**2)*x[2])/27
    elif index == 1:
        return -1.0*(397.5-x[0]*(x[1]**2)*(x[2]**2))/397.5
    elif index == 2:
        return -1.0*(1.93-(x[1]*(x[5]**4)*x[2])/(x[3]**3))/1.93
    elif index == 3:
        return -1.0*(1.93-(x[1]*(x[6]**4)*x[2])/(x[4]**3))/1.93
    elif index == 4:
        A1 = np.sqrt((745*x[3]/(x[1]*x[2]))**2 + 16.91E6)
        B1 = 0.1*x[5]**3
        return -1.0*((A1/B1)-1100)/1100
    elif index == 5:
        A2 = np.sqrt((745*x[4]/(x[1]*x[2]))**2 + 157.5E6)
        B2 = 0.1*x[6]**3
        return -1.0*((A2/B2)-850)/850
    elif index == 6:
        return -1.0*(x[1]*x[2]-40)/40
    elif index == 7:
        return -1.0*(5-(x[0]/x[1]))/5
    elif index == 8:
        return -1.0*((x[0]/x[1])-12)/12
    elif index == 9:
        return -1.0*(1.9+1.5*x[5]-x[3])/1.9
    elif index == 10:
        return -1.0*(1.9+1.1*x[6]-x[4])/1.9
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[2.6,3.6],[0.7,0.8],[17,28],[7.3,8.3],[7.3,8.3],[2.9,3.9],[5.0,5.5]])
