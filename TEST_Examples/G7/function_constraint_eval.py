import numpy as np
import sys

def get_dimension_num_constraints():
    return 10,8

def func_eval(x):
    return x[0]**2 + x[1]**2 + x[0]*x[2] - 14*x[0] - 16*x[1] + (x[2]-10)**2 + 4*(x[3]-5)**2 + (x[4]-3)**2 + 2*(x[5]-1)**2 + 5 * x[6]**2 + 7*(x[7]-11)**2 + 2*(x[8]-10)**2 + (x[9]-7)**2 + 45

def constraint_eval(x, index):
    if index == 0:
        return -1.0*(4*x[0] + 5*x[1] - 3*x[6] + 9*x[7] - 105)/105
    elif index == 1:
        return -1.0*(10*x[0] - 8*x[1] - 17*x[6] + 2*x[7])/370
    elif index == 2:
        return -1.0*(-8*x[0] + 2*x[1] + 5*x[8] - 2*x[9] - 12)/158
    elif index == 3:
        return -1.0*(3*(x[0]-2)**2 + 4*(x[1]-3)**2 + 2*x[2]**2 - 7*x[3] - 120)/1258
    elif index == 4:
        return -1.0*(5*x[0]**2 + 8*x[1] + (x[2]-6)**2 - 2*x[3] - 40)/816
    elif index == 5:
        return -1.0*(0.5*(x[0]-8)**2 + 2*(x[1]-4)**2 + 3*x[4]**2 - x[5] - 30)/834
    elif index == 6:
        return -1.0*(x[0]**2 + 2*(x[1]-2)**2 - 2*x[0]*x[1] + 14*x[4] - 6*x[5])/788
    elif index == 7:
        return -1.0*(-3*x[0] + 6*x[1] + 12*(x[8]-8)**2 - 7*x[9])/4048
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.tile([-10,10],(10,1))


