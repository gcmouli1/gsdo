import numpy as np
import sys

def get_dimension_num_constraints():
    return 2,2

def func_eval(x):
    return -x[0] - x[1]

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(-2*x[0]**4 + 8*x[0]**3 - 8*x[0]**2 + x[1] -2)

    if index == 1:
        return -1.0*(-4*x[0]**4 + 32*x[0]**3 - 88*x[0]**2 + 96*x[0] + x[1] -36)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[0,3],[0,4]])
