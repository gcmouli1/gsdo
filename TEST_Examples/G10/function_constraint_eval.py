import numpy as np
import sys

def get_dimension_num_constraints():
    return 8,6

def func_eval(x):
    return x[0] + x[1]+ x[2]

def plog(x):
    if x >= 0:
        return np.log(1+x)
    else:
        return -1.0*np.log(1-x)

def constraint_eval (x,index):
    if index == 0:
        return -1.0*(-1 + 0.0025*(x[3]+x[5]))
    elif index == 1:
        return -1.0*(-1 + 0.0025*(-x[3]+x[4]+x[6]))
    elif index == 2:
        return -1.0*(-1 + 0.01*(-x[4]+x[7]))
    elif index == 3:
        return -1.0*(100*x[0] - x[0]*x[5] + 833.33252*x[3] - 83333.333)
    elif index == 4:
        return -1.0*(x[1]*x[3] - x[1]*x[6] - 1250*x[3] + 1250*x[4])
    elif index == 5:
        return -1.0*(x[2]*x[4] - x[2]*x[7] - 2500*x[4] + 1250000)
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.array([[10**2,10**4],[10**3,10**4],[10**3,10**4],[10,10**3],[10,10**3],[10,10**3],[10,10**3],[10,10**3]])
