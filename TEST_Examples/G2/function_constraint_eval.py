import numpy as np
import sys

def get_dimension_num_constraints():
    return 10,2

def func_eval(x):
    cos_sum = np.sum(np.cos(x)**4)
    cos_prod = np.prod(np.cos(x)**2)
    vec_num = np.arange(1,x.shape[0]+1)
    sum_value = np.sum(vec_num*(x**2))
    return -1.0*abs((cos_sum - 2*cos_prod)/sum_value)

def plog(x):
    if x >= 0:
        return np.log(1+x)
    else:
        return -1.0*np.log(1-x)

def constraint_eval(x,index):
    if index == 0:
        return -1.0*plog(-np.prod(x)+0.75)/plog(10**(x.shape[0]))
    elif index == 1:
        return -1.0*(np.sum(x)-7.5*x.shape[0])/(2.5*x.shape[0])
    else:
        sys.exit("index out of bounds")

def get_bounds():
    return np.tile([0,10],(10,1))

